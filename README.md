# Multi Peer Review

## INTRODUCTION

The Multi Peer Review module adds functions that facilitate 
peer review of your publications by various user-defined panels.

## REQUIREMENTS

The module does not have any specific requirements.

## INSTALLATION

Install as you would normally install a Drupal module.


## CONFIGURATION

The Multi Peer Review module does not need any specific
configuration and can run on default values. However, 
after enabling the module you can access the configuration 
options under the Drupal Configuration menu.
