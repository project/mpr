<?php

namespace Drupal\multi_peer_review;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\FilteredEntityListBuilder;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Defines a class to build a listing of Search Result Item entities.
 *
 * @see \Drupal\multi_peer_review\Entity\SearchResultItem
 */
class SearchResultItemListBuilder extends FilteredEntityListBuilder {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RendererInterface $renderer) {
    parent::__construct($entity_type, $storage);

    $this->renderer = $renderer;        
    
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('renderer')
    );
  }
  
   
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQueryConditions(&$query) {
    // Filter by selected Paper.
    $paper_id = \Drupal::request()->request->get('paper');        
    if ((empty($paper_id) == FALSE) && ($paper_id != '_none')) {

        $paper = Paper::load($paper_id);
        if (empty($paper) == FALSE) {

            $subjects = MPRCommon::getArrayFromMultiLineValues($paper->getSubjects());                
            if (count($subjects) != 0) {
                // Create an orConditionGroup.
                $orGroup = $query->orConditionGroup();                    
                foreach ($subjects as $subject) {                        
                    $orGroup->condition('subjects__value', '%' . \Drupal::database()->escapeLike($subject) . '%', 'LIKE');
                }
                // Add the group to the query.
                $query->condition($orGroup);                      
            }

        }            
    }


    // Filter by data source.
    $data_source = \Drupal::request()->request->get('data_source');        
    if ((empty($data_source) == FALSE) && ($data_source != '_none')) {
        $query->condition('data_source', $data_source, '=');      
    }
    
    
    // Set results limit.
    $query->range(0, intval(MPRCommon::getConfigValue('maximum_search_results')));


    // Filter by keywords.
    $this->applyKeywordFilter($query, [
        'full_name', 
        'external_papers__value', 
        'subjects__value', 
        'id_orcid', 
        'id_isni', 
        'id_researcherid',
        ]);     
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQuerySort(&$query) {
    $query->sort('full_name');
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function getFilterFormFields() {
    
    $form = [];
    
    // Prepare Paper options. Do not include reviewed Papers.
    $options = [];
    $options['_none'] = $this->t('- None -');
    foreach (Paper::getPapers(FALSE, NULL, Paper::STATUS_IDLE) as $paper) {
        $options[$paper->id()] = $paper->label();
    }    
    foreach (Paper::getPapers(FALSE, NULL, Paper::STATUS_SEEKING_REVIEWERS) as $paper) {
        $options[$paper->id()] = $paper->label();
    }      
    // Paper's that are being reviewed as included in case additional Reviewers are required.
    foreach (Paper::getPapers(FALSE, NULL, Paper::STATUS_REVIEW_PENDING) as $paper) {
        $options[$paper->id()] = $paper->label();
    }       
    $form['paper'] = MPRCommon::getDefaultDropDownFormField('Suitable for Paper', '', FALSE, '', $options);
    
    
    // Prepare data source options.
    $options = [];
    $options['_none'] = $this->t('- All -');
    $options['Manual'] = 'Manual';
    $options['apo'] = 'APO';
    $form['data_source'] = MPRCommon::getDefaultDropDownFormField('Data Source', '', FALSE, '', $options);    
        
    
    
    $form['keywords'] = MPRCommon::getDefaultSingleLineTextFormField('Keywords', '', FALSE, '');
 
    return $form;      
  }
      
  

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    //$res = parent::getDefaultOperations($entity);
    $res = [];
    
    // Shortcut to add Reviewer.
    $res['reviewer_add'] = [
        'title' => t('Add'),
        'weight' => 11,
        'url' => $this->ensureDestination(Url::fromRoute('entity.reviewer.add_form', [], ['query' => ['copy_item' => $entity->id()]])),               
    ];
    
    return $res;
  }  
  
  

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {    
    //$header['label'] = $this->t('Unique ID');
    //$header['id'] = $this->t('ID');
    $header['full_name'] = $this->t('Name');
    $header['subjects'] = $this->t('Subjects');
    $header['papers'] = $this->t('Papers');


    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\multi_peer_review\SearchResultItemInterface $entity */
    //$row['label'] = $entity->label();
    //$row['id'] = $entity->id();
    
    
    // Prepare Name cell output.
    $cell_value = '<a href="' . $entity->getSourceUrl() . '" target="_blank">' . htmlentities($entity->getFullName()) . '</a>';
    $row['full_name'] = [
            'data' => [
                '#type' => 'markup',
                '#markup' => $cell_value,
            ],
    ];     
    
    
    $row['subjects'] = $entity->getSubjectsCsv();
    
    
    
    // Prepare Paper cell output.
    $paper_links = [];
    foreach (unserialize($entity->getExternalPapers()) as $key => $value) {
        array_push($paper_links, '<li><a href="' . $value['url'] . '" target="_blank">' . htmlentities($value['title']) . '</a></li>');
    }
    
    if (count($paper_links) == 0) {
        $cell_value = '';
    }
    else
    { 
        $cell_value = '<ul class="list-builder-search-result-list">' . implode('', $paper_links) . '</ul>';
    }
                
    $row['papers'] = [
            'data' => [
                '#type' => 'markup',
                '#markup' => $cell_value,
            ],
    ];     
    


    return $row + parent::buildRow($entity);
  }


}
