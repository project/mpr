<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormInterface;

/**
 * Defines interface for Review forms so they can be easily distinguished.
 *
 * @internal
 */
interface ReviewFormInterface extends FormInterface {}
