<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Form controller for the Invitation send form.
 */
class InvitationSendForm extends ContentEntityConfirmFormBase implements InvitationFormInterface {

  /**
   * The invitation entity.
   *
   * @var \Drupal\multi_peer_review\InvitationInterface
   */
  protected $entity;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new InvitationForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $placeholders = [
        '%paper' => $this->entity->getPaperTitle(),
    ];
    
    return ($this->t('Send Invitation to review %paper?', $placeholders));           
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $placeholders = [
        '%paper' => $this->entity->getPaperTitle(),
        '%reviewer' => $this->entity->getReviewerName(),
    ];   
        
    return ($this->t('Send Invitation to review %paper to %reviewer?', $placeholders));    
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {   
    // Although overriding this method is required, it appears not to be used while viewed from the backend.
    // The returned Url is being properly handled here in case it is used somewhere or someday.
      
    // Example Url object toString() value: /admin/multi-peer-review/invitations
    return $this->entity->toUrl('collection');
  }    
  
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Send Invitation to Reviewer');
  }  
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }
  

  
  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // 'Confirm' button clicked.
    $status = NULL; // Set default status.  
      
    // Load Invitation, Paper and Reviewer entities.  
    $invitation = $this->entity;    
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();
      
    
    // Does not include the actual Paper.
    $email = \Drupal\multi_peer_review\MPREmail::createFromEmailCapableEntity(
        'invitation_new', 
        [$invitation, $paper, $reviewer], 
        $reviewer->getEmail(), 
        $invitation, 
        [$invitation]
    );    
    

    
    if ($email->send() == TRUE) {
        drupal_set_message(t('The Invitation has been sent.'));             
        $invitation->logAction($this->logger('multi_peer_review'), 'sent');
        
        // Update Invitation status.
        $invitation->set('status', Invitation::STATUS_PENDING);
        $invitation->set('pending_timestamp', time());
        $invitation->setNewRevision(TRUE);
        $status = $invitation->save();
        
        if ($status == SAVED_UPDATED) {
            // Update Reviewer statistics.
            $pending_invitations = $reviewer->get('cached_pending_invitations')->getValue();
            if (empty($pending_invitations) == TRUE) {
                $pending_invitations = 0;
            }
            else {
                $pending_invitations = intval($pending_invitations);
            }
            $pending_invitations++;
            $reviewer->get('cached_pending_invitations')->setValue($pending_invitations);
            $reviewer->save();
            
            // Update Paper status.
            Paper::rebuildCachedFigures($paper);                        
        }
        
    } 
    else {
        drupal_set_message(t('There was a problem sending the Invitation. It has not been sent.'), 'error'); 
    }    
    
    
    
    if ($status == NULL) {
        // Form will be rebuilt.
        $form_state->setRebuild();
    }
    else {         
        // Redirect the user to the home page. $status is typically SAVED_UPDATED.      
        $form_state->setRedirectUrl($this->getCancelUrl());          
    }    
    
  }
    
  
}

