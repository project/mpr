<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class UserPapersForm.
 *
 * @package Drupal\multi_peer_review\Form
 */
class UserPapersForm extends CommonFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_papers_form';
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
                
    $user_id = $this->getUserId();
    $show_reset_button = FALSE;
    $papers = [];    
    
    
    // Filter Papers.
    $status = \Drupal::request()->request->get('paper_status');
    if ($status == '_none') {
        $status = NULL;
    }
    
    if ($status != NULL) {
        $show_reset_button = TRUE;
    }
    
    $papers = Paper::getPapers(FALSE, NULL, $status, NULL, $user_id);    
        
    // Filter by keywords.
    $keywords = \Drupal::request()->request->get('paper_keywords');
    if (empty($keywords) == FALSE) {
        
        // Isolate each keyword.
        $keyword_list = explode(' ', strtolower(trim($keywords)));
        
        $filtered_papers = [];
        foreach ($papers as $paper) {
            
            $meta_items = [
                $paper->label(),
                $paper->getPaperType(),
                $paper->getAbstract(),
                $paper->getSubjectsCsv(),
                $paper->getIndividualContributors(),
                $paper->getNonIndividualContributors(),
                $paper->getAlertRecipients(),
            ];
            
            $meta = '';
            foreach ($meta_items as $meta_item) {
                if (empty($meta_item) == FALSE) {
                    $meta = $meta . $meta_item . ' ';
                }
            }
            $meta = strtolower(trim($meta));
                        
            foreach ($keyword_list as $keyword) {
                if (strpos($meta, $keyword) !== FALSE) {
                    
                    // Add to filtered array.
                    array_push($filtered_papers, $paper);
                    
                    break;
                }
            }
        }
         
        $papers = $filtered_papers;
        
        $show_reset_button = TRUE;
    }    
        
    Paper::sortPapersByStatusAndTitle($papers);
    
    
    
    // Configure form layout.   
    $form['paper_add'] = [
        '#type' => 'link',
        '#url' => Url::fromRoute('multi_peer_review.account.papers.add', ['user' => $user_id]),
        '#title' => $this->t('Upload Paper'),     
        '#attributes' => [
          'class' => ['button', 'button-action', 'button--primary', 'button--small'],
        ],           
    ];    
    
    
    $form['filters_container'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['form--inline', 'clearfix'],
        ],        
    ];
    
    
    // Configure form actions.
    $options = Paper::getTranslatedStatusListOptions();        
    $form['filters_container']['paper_status'] = MPRCommon::getDefaultDropDownFormField('Status', '', FALSE, '', $options);
    $form['filters_container']['paper_keywords'] = MPRCommon::getDefaultSingleLineTextFormField('Keywords', '', FALSE, '');
    
    
    $form['filters_container']['actions'] = [
        '#type' => 'actions',
        '#attributes' => [
            'class' => ['form-actions', 'js-form-wrapper', 'form-wrapper'],
        ],
    ];
    $form['filters_container']['actions']['filter_button'] = [
        '#type' => 'submit',           
        '#value' => $this->t('Filter'),        
    ];
    if ($show_reset_button == TRUE) {
        $form['filters_container']['actions']['reset_button'] = [
            '#type' => 'submit',        
            '#value' => $this->t('Reset'),        
        ];    
    }    
    
      
    
    
    // Output table of Papers.
    $form['papers'] = [
        '#type' => 'table',
        '#header' => [
            'title' => $this->t('Title'),
            'type' => $this->t('Type'),
            'status' => $this->t('Status'),
            'checklist' => $this->t('Checklist'),
            'created' => $this->t('Created'),            
            'modified' => $this->t('Modified'),  
            'operations' => $this->t('Operations'),      
        ],
        '#empty' => $this->t('No papers found.'),
        '#weight' => 100,
    ];
    
    $rows = [];
    $row_count = 0;
    $row_max = intval(MPRCommon::getConfigValue('default_front_end_display_row_limit'));
    foreach ($papers as $paper) {
        
        
        $paper_view_link = Url::fromRoute('multi_peer_review.account.papers.paper.view', [
            'user' => $user_id,
            'paper' => $paper->id(),
        ]);            
        
        $row = [
            'title' => $paper->label(),
            'type' => $this->t($paper->getPaperType()),
            'status' => $paper->getTranslatedVerboseStatusText(),
            'checklist' => $paper->getChecklistCompletionPercentText(),
            'created' => MPRCommon::getFormattedDateText($paper->getCreatedTime()),
            'modified' => MPRCommon::getFormattedDateText($paper->getChangedTime()),                    
        ];
        
        $row['title'] = new FormattableMarkup(
                '<a href="' . $paper_view_link->toString() . '">@paper-title</a>',
                [
                    '@paper-title' => $paper->label(),
                ]
        );        
        
                
        switch ($paper->getStatus()) {            
            case Paper::STATUS_IDLE:
                
                $edit_paper_link = Url::fromRoute('multi_peer_review.account.papers.paper.edit', [
                    'user' => $user_id,
                    'paper' => $paper->id(),
                ]);         
                
                $delete_paper_link = Url::fromRoute('multi_peer_review.account.papers.paper.delete', [
                    'user' => $user_id,
                    'paper' => $paper->id(),
                ]);                   
                
                $row['operations'] = new FormattableMarkup('<ul>
                    <li><a href="' . $edit_paper_link->toString() . '">@edit-paper-label</a></li>
                    <li><a href="' . $delete_paper_link->toString() . '">@delete-paper-label</a></li></ul>', 
                        [
                            '@edit-paper-label' => $this->t('Edit Paper'),                 
                            '@delete-paper-label' => $this->t('Delete Paper'),  
                        ]
                    );
                
                break;        
            default:
                $row['operations'] = '';
                break;
        }
        
        
        array_push($rows, $row);        
        
        $row_count++;
        if ($row_count >= $row_max) {
            break;
        }
    }
    $form['papers']['#rows'] = $rows;
     

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {  
      
    $operation = $form_state->getValue('op')->getUntranslatedString();
    switch ($operation) {
        case 'Filter':

            // Form will be rebuilt.
            $form_state->setRebuild();   
            
            break;
        case 'Reset':

            // Do nothing as this will reset the form.
            
            break;
    }
        
  }

}