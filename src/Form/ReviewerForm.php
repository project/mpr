<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\CommonContentEntityInterface;

/**
 * Form controller for the Reviewer edit forms.
 */
class ReviewerForm extends CommonContentEntityForm implements ReviewerFormInterface {

  
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

      
    $reviewer = $this->entity;
    $flexible_defaults['first_name'] = $reviewer->getFirstName();
    $flexible_defaults['last_name']  = $reviewer->getLastName();         
    $flexible_defaults['data_source']  = 'Manual';
    $flexible_defaults['subjects']  = $reviewer->getSubjects();
    $flexible_defaults['email']  = $reviewer->getEmail();
    $flexible_defaults['id_orcid'] = $reviewer->getORCID();
    $flexible_defaults['id_isni'] = $reviewer->getISNI();
    $flexible_defaults['id_researcherid'] = $reviewer->getResearcherID();    

    
    if ($reviewer->isNew() == TRUE) {
        $default_id = MPRCommon::getNewCustomTextId();
        
        $copy_item = \Drupal::request()->query->get('copy_item');
        if (empty($copy_item) == FALSE) {
            $copy =  \Drupal\multi_peer_review\Entity\SearchResultItem::load($copy_item);

            $flexible_defaults['first_name'] = $copy->getFirstName();
            $flexible_defaults['last_name']  = $copy->getLastName();
            $flexible_defaults['data_source'] = $copy->getDataSource();
            $flexible_defaults['subjects']  = $copy->getSubjects();
            $flexible_defaults['email'] = $copy->getEmail();
            $flexible_defaults['id_orcid'] = $copy->getORCID();
            $flexible_defaults['id_isni'] = $copy->getISNI();
            $flexible_defaults['id_researcherid'] = $copy->getResearcherID();
        }        
    }
    else {
        $default_id = $reviewer->label();
    }    
    
    if ($this->operation == 'edit') {
      $form['#title'] = $reviewer->getTranslatedMessage(CommonContentEntityInterface::MESSAGE_ADMIN_EDIT_TITLE);      
    }
    

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique ID'),
      '#maxlength' => 255,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#disabled' => !$reviewer->isNew(),           
    ];

    
    
    $form['title'] = MPRCommon::getDefaultSingleLineTextFormField('Title', 
            'Reviewer’s title such as Professor, Mrs, Sir.', FALSE, $reviewer->getTitle());  
    
    $form['first_name'] = MPRCommon::getDefaultSingleLineTextFormField('First Name', 
            'Reviewer’s first name or given name.', TRUE, $flexible_defaults['first_name']);    
    
    $form['last_name'] = MPRCommon::getDefaultSingleLineTextFormField('Last Name', 
            'Reviewer’s last name or family name.', TRUE, $flexible_defaults['last_name']);                  

    if (($reviewer->isNew() == TRUE) || ($reviewer->getOwnerId() == $reviewer->getOriginalOwner())) {
        $form['email'] = MPRCommon::getDefaultSingleEmailAddressFormField('Email', 
                'Reviewer’s email.', TRUE, $flexible_defaults['email']);       
    }
    else {
        $form['email_display'] = MPRCommon::getDefaultSingleEmailAddressFormField('Email', 
                'Reviewer’s email. To update this email address, update the user’s account.', TRUE, $flexible_defaults['email']);    
        $form['email_display']['#disabled'] = TRUE;
        $form['email_display']['#value'] = $flexible_defaults['email'];
    }
       
    $form['phone'] = MPRCommon::getDefaultSingleLineTextFormField('Phone', 
            'Reviewer’s contact number.', FALSE, $reviewer->getPhone());    
    
    if (empty(MPRCommon::getConfigValue('panels')) == FALSE) {
        $form['panel'] = MPRCommon::getDefaultDropDownFormField('Panel', 
                'The Panel that the Reviewer has been assigned to by staff. If no Panel is selected, the Reviewer is considered to be available for all Panels (if any).', FALSE, $reviewer->getPanel(), MPRCommon::getTranslatedListOptionsFromConfig('panels'));        
    }
    
    $form['internal_comments'] = MPRCommon::getDefaultMultiLineTextFormField('Internal Comments', 
            'Comments about the Reviewer by staff. Only other staff can view these comments.', FALSE, $reviewer->getInternalComments());     
        
    $form['biography'] = MPRCommon::getDefaultMultiLineTextFormField('Biography', 
            'A brief biography of the Reviewer. If provided, the information may help staff when deciding whether or not to invite a Reviewer to review a Paper.', FALSE, $reviewer->getBiography());       
    
    $form['subjects'] = MPRCommon::getDefaultMultiLineTextFormField('Subjects', 
            'The subjects that the Reviewer specialises in. Each subject must be entered on a single line.', FALSE, $flexible_defaults['subjects']);      
    
    $form['affiliations'] = MPRCommon::getDefaultMultiLineTextFormField('Affiliations', 
            'The Reviewer’s affiliations. Each affiliation must be entered on a single line.', FALSE, $reviewer->getAffiliations());          
    
    $form['id_orcid'] = MPRCommon::getDefaultSingleLineTextFormField('ORCID', 
            'The Reviewer’s unique identifier on the ORCID registry (https://orcid.org/).', FALSE, $flexible_defaults['id_orcid']); 

    $form['id_isni'] = MPRCommon::getDefaultSingleLineTextFormField('ISNI', 
            'The Reviewer’s unique identifier on the ISNI platform (http://www.isni.org/).', FALSE, $flexible_defaults['id_isni']); 
    
    $form['id_researcherid'] = MPRCommon::getDefaultSingleLineTextFormField('ResearcherID', 
            'The Reviewer’s unique identifier on the Publons platform (https://publons.com).', FALSE, $flexible_defaults['id_researcherid']);     
    
    

    $form['data_source'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Data Source'),
      '#maxlength' => 255,
      '#default_value' => $flexible_defaults['data_source'],
      '#required' => FALSE,
    ];    


    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Reviewer ID'),
      '#maxlength' => 255,
      '#default_value' => $reviewer->id(),
      '#disabled' => !$reviewer->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\Reviewer::load',
      ],
      '#element_validate' => [],
    ];
    
    
    return parent::form($form, $form_state);
  }
  

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge([
      'label',
      'id',
    ], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    $field_names = [
      'label',
      'id',
    ];
    foreach ($violations->getByFields($field_names) as $violation) {
      list($field_name) = explode('.', $violation->getPropertyPath(), 2);
      $form_state->setErrorByName($field_name, $violation->getMessage());
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $reviewer = $this->entity;
    
    $copied_entity = NULL;
    if ($reviewer->isNew() == TRUE) {
        $copy_item = \Drupal::request()->query->get('copy_item');
        if (empty($copy_item) == FALSE) {
            $copied_entity = \Drupal\multi_peer_review\Entity\SearchResultItem::load($copy_item);
            
            $reviewer->set('source_search_result_item', $copy_item);
        } 
    }
    
    $reviewer->setNewRevision(TRUE);
    $status = $reviewer->save();
    
    
    // Archive the copied entity as it should no longer appear in lists.
    if (($status == SAVED_NEW) && ($copied_entity != NULL)) {
        $copied_entity->set('archived', TRUE);                
        $copied_entity->save();
    }
    
    $reviewer->processAdminFormSaveMessages($this->logger('multi_peer_review'), $this->messenger, $status);


    if ($reviewer->id()) {
      $form_state->setValue('id', $reviewer->id());
      $form_state->set('id', $reviewer->id());

      $collection_url = $reviewer->toUrl('collection');
      $redirect = $collection_url->access() ? $collection_url : Url::fromRoute('<front>');
      $form_state->setRedirectUrl($redirect);
    }
    else {
      $this->messenger->addError($this->t('The Reviewer could not be saved.'));
      $form_state->setRebuild();
    }
  }

}

