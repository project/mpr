<?php

namespace Drupal\multi_peer_review\Form;
 
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPREmail;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Review;
 
/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {
/**
 * {@inheritdoc}
 */
  public function getFormId() {
    return 'multi_peer_review_config_settings';
  }
 
/**
 * {@inheritdoc}
 */
  protected function getEditableConfigNames() {
    return [
      'multi_peer_review.config_settings',
    ];
  }
 
/**
 * {@inheritdoc}
 */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('multi_peer_review.config_settings');
    
    $site_config = \Drupal::config('system.site');
    
    
    $form['general_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
    ];    

    // Emails that are sent by the module will use the given sender name in the email From field.
    $default_value = $config->get('system_sender_name');
    if (empty($default_value) == TRUE) {
        $default_value = $site_config->get('name');
    }
    $form['general_settings']['system_sender_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('System Sender Name'),
      '#description' => $this->t('Emails sent by the Multi Peer Review module will use this sender name.'),
      '#default_value' => $default_value,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];    
    
    // Emails that are sent by the module will use the given sender email in the email From field.
    $default_value = $config->get('system_sender_email');
    if (empty($default_value) == TRUE) {
        $default_value = $site_config->get('mail');
    }    
    $form['general_settings']['system_sender_email'] = [
      '#type' => 'email',
      '#title' => $this->t('System Sender Email'),
      '#description' => $this->t('Emails sent by the Multi Peer Review module will use this sender email address.'),
      '#default_value' => $default_value,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];    
    
    
    // The minimum number of submitted Reviews a Paper must have before it can be considered reviewed.
    $form['general_settings']['minimum_reviews_per_paper'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum Reviews per Paper'),
      '#description' => $this->t('The minimum number of submitted Reviews a Paper must have before it can be considered reviewed.'),
      '#default_value' => $config->get('minimum_reviews_per_paper'),
      '#min' => 1,
      '#max' => 1000,
      '#step' => 1,
      '#required' => TRUE,
    ];      
    
    // All emails that are sent out via the module are Bcc’d to the specified email address.
    $form['general_settings']['audit_email_address'] = [
      '#type' => 'email',
      '#title' => $this->t('Audit Email Address'),
      '#description' => $this->t('All emails that are sent out via the Multi Peer Review module are Bcc\'d to the specified email address.'),
      '#default_value' => $config->get('audit_email_address'),
      '#maxlength' => 255,
      '#required' => FALSE,
    ];        
    
    
    // Decline Reasons.
    // When a Reviewer declines an Invitation, they are asked to provide a reason. They can select a reason from this list. Each item must be entered on a separate line.
    $form['general_settings']['decline_reasons'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Decline Reasons'),
      '#description' => $this->t('When a Reviewer declines an Invitation, they are asked to provide a reason. They can select a reason from this list. Each item must be entered on a separate line.'),
      '#default_value' => $config->get('decline_reasons'),
      '#required' => TRUE,
    ];    
    
    // The number of days between each follow-up email to Reviewers who have been invited to Review a Paper.
    $form['general_settings']['invitation_follow_up_frequency_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Invitation Follow-up Frequency (Days)'),
      '#description' => $this->t('The number of days between each follow-up email to Reviewers who have been invited to Review a Paper.'),
      '#default_value' => $config->get('invitation_follow_up_frequency_days'),
      '#min' => 1,
      '#max' => 366,
      '#step' => 1,
      '#required' => TRUE,
    ];         
    
    // The number of follow-ups that must go unanswered before the module automatically retracts Invitations.
    // e.g. If the follow-up frequency is 3 days and this setting is 2, 9 days would need to pass before an Invitation is automatically retracted.
    $form['general_settings']['maximum_invitation_follow_ups'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Invitation Follow-ups'),
      '#description' => $this->t('The number of follow-ups that must go unanswered before the Multi Peer Review module automatically retracts Invitations.'),
      '#default_value' => $config->get('maximum_invitation_follow_ups'),
      '#min' => 1,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];       
    
    
    // The list items configured will appear as a list of check boxes within each Paper record form. The values have no impact on the module’s automated functions.
    $form['general_settings']['paper_checklist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paper Checklist'),
      '#description' => $this->t('The list items configured will appear as a list of check boxes within each Paper record form. Each item must be entered on its own line.'),
      '#default_value' => $config->get('paper_checklist'),
      '#required' => FALSE,
    ];       
    
    // A list containing the types of Papers that are relevant to the organisation and that will be peer reviewed by Reviewers. Each Paper Type must be entered on its own line.
    // i.e. One Paper Type per line.
    $form['general_settings']['paper_types'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paper Types'),
      '#description' => $this->t('A list containing the types of Papers that are relevant to the organisation and that will be peer reviewed by Reviewers. Each item must be entered on its own line.'),
      '#default_value' => $config->get('paper_types'),
      '#required' => TRUE,
    ];        
    
    // Determines when to remind Reviewers about upcoming Review deadlines. It is based on the number of days until the deadline is reached. 
    // e.g. If a Review’s deadline is 30/04/2021 and the setting is 10 days, the module will send a reminder on the 20/04/2021.
    $form['general_settings']['review_upcoming_deadline_reminder_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Review Upcoming Deadline Reminder (Days)'),
      '#description' => $this->t('Determines when to remind Reviewers about upcoming Review deadlines. The reminder is sent X days before the deadline, where X is the number provided.'),
      '#default_value' => $config->get('review_upcoming_deadline_reminder_days'),
      '#min' => 1,
      '#max' => 366,
      '#step' => 1,
      '#required' => TRUE,
    ];         
    
    // Determines when to remind Reviewers about overdue Reviews. It is based on the number of days elapsed after the deadline has passed.
    $form['general_settings']['review_overdue_reminder_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Review Overdue Reminder (Days)'),
      '#description' => $this->t('Determines when to remind Reviewers about overdue Reviews. The reminder is sent X days after the deadline, where X is the number provided.'),
      '#default_value' => $config->get('review_overdue_reminder_days'),
      '#min' => 1,
      '#max' => 366,
      '#step' => 1,
      '#required' => TRUE,
    ];  
    
    // Number of weeks that are considered “Recent” for the purpose of collecting statistics on Invitations.
    $form['general_settings']['recent_invitation_range_weeks'] = [
      '#type' => 'number',
      '#title' => $this->t('Recent Invitation Range (Weeks)'),
      '#description' => $this->t('Number of weeks that are considered "Recent" for the purpose of collecting statistics on Invitations.'),
      '#default_value' => $config->get('recent_invitation_range_weeks'),
      '#min' => 1,
      '#max' => 52,
      '#step' => 1,
      '#required' => TRUE,
    ];      
    
    // Reviewers will be assigned to a Panel from this list.
    $form['general_settings']['panels'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Panels'),
      '#description' => $this->t('Reviewers will be assigned to a Panel from this list.'),
      '#default_value' => $config->get('panels'),
      '#required' => TRUE,
    ];       
    
    // Technical incidents such as search script failures are sent to this email address.
    $form['general_settings']['technical_contact_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Technical Contact'),
      '#description' => $this->t('Technical incidents such as search script failures are sent to this email address.'),
      '#default_value' => $config->get('technical_contact_email'),
      '#maxlength' => 255,
      '#required' => FALSE,
    ];        
    
    
    // The maximum number of table rows that should normally be displayed on the front-end.
    $form['general_settings']['default_front_end_display_row_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Front-End Display Row Limit'),
      '#description' => $this->t('The maximum number of table rows that should normally be displayed on the front-end.'),
      '#default_value' => $config->get('default_front_end_display_row_limit'),
      '#min' => 5,
      '#max' => 10000,
      '#step' => 1,
      '#required' => TRUE,
    ];       
      
    
    // Email footer to appended to outgoing emails.
    $form['general_settings']['email_footer'] = MPRCommon::getDefaultHtmlTextFormField('Email Footer', 
            'Email footer appended to all outgoing emails.', FALSE, $config->get('email_footer'));
    
    
    // If enabled, the Drupal user registration form will be adjusted so that it allows users to
    // select the Multi Peer Review Guest role.
    $form['general_settings']['allow_guest_registration'] = MPRCommon::getDefaultCheckBoxFormField('Allow users to register as a Multi Peer Review Guest.', 
            '', $config->get('allow_guest_registration'));      
    
    
    // HTML tags allowed in Multi Peer Review rich text fields such as the Paper Abstract field. Each tag must be entered on a separate line.    
    $form['general_settings']['allowed_html_tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed HTML Tags'),
      '#description' => $this->t('HTML tags allowed in Multi Peer Review rich text fields such as the Paper Abstract field. Each tag must be entered on a separate line.'),
      '#default_value' => $config->get('allowed_html_tags'),
      '#required' => FALSE,
    ];        
    
    
    // Contains search engine settings
    $form['search_engine_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Search Engine'),
      '#open' => TRUE,
    ];       
    
    // Limits the number of results the search engine will display to Users.
    $form['search_engine_settings']['maximum_search_results'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Search Results'),
      '#description' => $this->t('Limits the number of results the search engine will display to Users.'),
      '#default_value' => $config->get('maximum_search_results'),
      '#min' => 1,
      '#max' => 300,
      '#step' => 1,
      '#required' => TRUE,
    ];  
    
    // Number of minutes between automatic and incremental building of the search cache.
    $form['search_engine_settings']['search_script_cache_build_frequency_minutes'] = [
      '#type' => 'number',
      '#title' => $this->t('Search Script Cache Build Frequency (Minutes)'),
      '#description' => $this->t('Number of minutes between automatic and incremental building of the search cache.'),
      '#default_value' => $config->get('search_script_cache_build_frequency_minutes'),
      '#min' => 1,
      '#max' => 527040,
      '#step' => 1,
      '#required' => TRUE,
    ];      
    
    // Contains developer settings
    $form['web_developer_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Web Developer Settings'),
      '#open' => MPRCommon::getDebugModeEnabled(),
      '#description' => 'Options for web developers',
    ];     
    
    $form['web_developer_settings']['debug_mode'] = MPRCommon::getDefaultCheckBoxFormField('Debug Mode', 
            'Add test controls to entity forms.', $config->get('debug_mode'));
    
    $form['web_developer_settings']['delete_orphaned_files'] = MPRCommon::getDefaultCheckBoxFormField('Delete Orphaned Files', 
            'Upon submitting this form, orphaned files in the Multi Peer Review directory will be deleted.', FALSE);    
    
    $form['web_developer_settings']['send_test_email'] = MPRCommon::getDefaultCheckBoxFormField('Send Test Email', 
            'Upon submitting this form, a test email will be sent to the current user and technical contact (if any). If at least one Paper exists, an attachment will be included.', FALSE);       
    
    
    return parent::buildForm($form, $form_state);
  }
 
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('multi_peer_review.config_settings')
        ->set('system_sender_name', $form_state->getValue('system_sender_name'))
        ->set('system_sender_email', $form_state->getValue('system_sender_email'))
        ->set('minimum_reviews_per_paper', $form_state->getValue('minimum_reviews_per_paper'))
        ->set('audit_email_address', $form_state->getValue('audit_email_address'))
        ->set('decline_reasons', $form_state->getValue('decline_reasons'))
        ->set('invitation_follow_up_frequency_days', $form_state->getValue('invitation_follow_up_frequency_days'))
        ->set('maximum_invitation_follow_ups', $form_state->getValue('maximum_invitation_follow_ups'))
        ->set('paper_checklist', $form_state->getValue('paper_checklist'))
        ->set('paper_types', $form_state->getValue('paper_types'))
        ->set('review_upcoming_deadline_reminder_days', $form_state->getValue('review_upcoming_deadline_reminder_days'))
        ->set('review_overdue_reminder_days', $form_state->getValue('review_overdue_reminder_days'))
        ->set('recent_invitation_range_weeks', $form_state->getValue('recent_invitation_range_weeks'))
        ->set('panels', $form_state->getValue('panels'))
        ->set('technical_contact_email', $form_state->getValue('technical_contact_email'))
        ->set('default_front_end_display_row_limit', $form_state->getValue('default_front_end_display_row_limit'))
        ->set('allowed_html_tags', $form_state->getValue('allowed_html_tags'))
        ->set('email_footer', MPRCommon::filterHtml($form_state->getValue('email_footer')['value']))
        ->set('allow_guest_registration', $form_state->getValue('allow_guest_registration'))
        ->set('maximum_search_results', $form_state->getValue('maximum_search_results'))
        ->set('search_script_cache_build_frequency_minutes', $form_state->getValue('search_script_cache_build_frequency_minutes'))
        ->set('debug_mode', $form_state->getValue('debug_mode'))
        ->save();
 
    
    // Reset the cron time related to search_script_cache_build_frequency_minutes. 
    \Drupal::state()->set(MPRCommon::EXECUTION_INTERVAL_SES, time() + (intval($form_state->getValue('search_script_cache_build_frequency_minutes')) * 60));
    
    
    // Update cached figures.
    Reviewer::rebuildCachedFigures();    
    Invitation::rebuildCachedFigures();
    Review::rebuildCachedFigures();    
    Paper::rebuildCachedFigures();
        
    
    // Developer commands.
    if ($form_state->getValue('delete_orphaned_files') == TRUE) {
        MPRCommon::deleteModuleFiles(TRUE);
    }
    
    if ($form_state->getValue('send_test_email') == TRUE) {
        
        // If there is a Paper, use it as a test attachment.
        $attachment_sources = [];
        foreach (Paper::getPapers(FALSE) as $paper) {
            array_push($attachment_sources, $paper);
            break;
        }
        
        MPRCommon::$last_error = 'Please ignore. This is a test email. It will contain an attachment if at least one Paper record exists.';
        $email = MPREmail::createFromEmailTemplate(
                'technical_incident', 
                [], 
                $this->currentUser()->getEmail(), 
                $form_state->getValue('technical_contact_email'), 
                '', 
                $attachment_sources
        );
        if ($email->send() == TRUE) {
            $this->messenger()->addMessage($this->t('Test email sent successfully.'));
        }
        else {
            $this->messenger()->addError($this->t('Failed to send test email.'));
        }
    }
    
    
    parent::submitForm($form, $form_state);
  }
}
