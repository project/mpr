<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPREmail;

/**
 * Provides a form for deleting Paper entities.
 *
 * @ingroup multi_peer_review
 */
class PaperDeleteForm extends ContentEntityDeleteForm implements PaperFormInterface {

    
    
  public function changeUrlOnGuestUser(&$destination_url) {    
    if (MPRCommon::isAdminUser() == FALSE) {
        $destination_url = Url::fromRoute('multi_peer_review.account.papers', ['user' => $this->currentUser()->id()]);
    }      
  }
    
    
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $res = parent::getCancelUrl();  
    
    $this->changeUrlOnGuestUser($res);
    
    return $res;
  }      
  
  
  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    $res = parent::getRedirectUrl();
    
    $this->changeUrlOnGuestUser($res);
    
    return $res;
  }  
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $paper = $this->entity;
      
    parent::submitForm($form, $form_state);
            
    // Send email to Paper owner and recipients.
    if (MPRCommon::isAdminUser() == TRUE) {
        $email = MPREmail::createFromEmailTemplate(
                'paper_deleted', 
                [$paper], 
                $paper->getOwner()->getEmail(), 
                $paper->getAlertRecipients(), 
                '', 
                []
        );
        $email->send();
    }
  }


}
