<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRFormHelper;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Form controller for the Common Confirm form.
 */
abstract class CommonConfirmForm extends ContentEntityConfirmFormBase {

  /**
   * The related entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityBase
   */
  protected $entity;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  
  /**
   * The form helper.
   *
   * @var \Drupal\multi_peer_review\MPRFormHelper
   */
  private $helper;

  
  
  /**
   * Returns the Multi Peer Review form helper instance for this object.
   * The form helper will be initialised the first time this method is called.
   * 
   * @return \Drupal\multi_peer_review\MPRFormHelper
   *   The instance of this object's helper.
   */     
  public function getHelper() {
    if (empty($this->helper) == TRUE) {
        $this->helper = new MPRFormHelper();
        $this->helper->initialise($this);
    }
    
    return $this->helper;
  }    
  
  
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // No description required for this form.  
    return '';
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {   
    // Although overriding this method is required, it appears not to be used while viewed from the backend.
    // The returned Url is being properly handled here in case it is used somewhere or someday.
      
    // Example Url object toString() value: /admin/multi-peer-review/invitations
         
    if ($this->isAdmin() == TRUE) {
        $res = $this->entity->toUrl('collection');
    }
    else {
        $user_id = $this->getUserId();
        if (empty($user_id) == TRUE) {
            $res = Url::fromRoute('<front>');
        }
        else {            
            $res = $this->getFrontEndUserCancelUrl($user_id);
        }
    }      
      
    return $res;
  }    
  
  abstract function getFrontEndUserCancelUrl($user_id);
  
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Confirm');
  }  
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }
  
  
  public function isAdmin() {    
    return $this->getHelper()->isAdmin();
  }  
  
  public function getUserId() {    
    return $this->getHelper()->getUserId();
  }  
  
  
  public function insertPaperDetails(&$form, $paper, $details_open = FALSE, $include_download = TRUE) {
    return $this->getHelper()->insertPaperDetails($form, $paper, $details_open, $include_download);     
  }
  
  public function insertReviewerDetails(&$form, $reviewer, $details_open = FALSE) {
    return $this->getHelper()->insertReviewerDetails($form, $reviewer, $details_open);     
  }


  
    
  
}

