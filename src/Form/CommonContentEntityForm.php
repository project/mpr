<?php
namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRFormHelper;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Form controller for the Common Content Entity Edit forms.
 */
abstract class CommonContentEntityForm extends ContentEntityForm {
    
  /**
   * The entity.
   *
   * @var \Drupal\multi_peer_review\ReviewerInterface
   */
  protected $entity;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;    
  
  
  /**
   * The form helper.
   *
   * @var \Drupal\multi_peer_review\MPRFormHelper
   */
  private $helper;
  
  
  /**
   * Constructs a new form.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }  
  
  
  /**
   * Returns the Multi Peer Review form helper instance for this object.
   * The form helper will be initialised the first time this method is called.
   * 
   * @return \Drupal\multi_peer_review\MPRFormHelper
   *   The instance of this object's helper.
   */     
  public function getHelper() {
    if (empty($this->helper) == TRUE) {
        $this->helper = new MPRFormHelper();
        $this->helper->initialise($this);
    }
    
    return $this->helper;
  }      
  
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }


  public function isAdmin() {    
    return $this->getHelper()->isAdmin();
  }  
  
  public function getUserId() {    
    return $this->getHelper()->getUserId();
  }    
  
  
  public function insertWebDeveloperControls(&$form, &$form_state) {
    if (MPRCommon::getDebugModeEnabled() == TRUE) {
        
        $web_dev_form = [];
        $this->buildWebDeveloperControls($web_dev_form);
        if (empty($web_dev_form) == FALSE) {
            
            // Add container.
            $form['web_developer_controls'] = [
              '#type' => 'details',
              '#title' => $this->t('Web Developer Controls'),
              '#open' => TRUE,
            ];                 
            
            // Merge controls with form.
            foreach ($web_dev_form as $key => $value) {
                $form['web_developer_controls'][$key] = $value;
            }
            
        }
        
    }
  }

  public function buildWebDeveloperControls(&$form) {
    // Override if web developer controls are required.
  }

  public function isCheckBoxChecked($form, $name) {    
    if (empty($form[$name]) == FALSE) {
        $res = boolval($form[$name]['#checked']);                
    }
    else {
        if (empty($form['web_developer_controls'][$name]) == FALSE) {
            $res = boolval($form['web_developer_controls'][$name]['#checked']);                
        }
        else {
            $res = FALSE;
        }
    }
    
    return $res;
  }



}