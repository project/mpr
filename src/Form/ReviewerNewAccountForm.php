<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Review;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\user\UserInterface;


/**
 * Form controller for the Reviewer new account form.
 */
class ReviewerNewAccountForm extends ContentEntityConfirmFormBase implements ReviewerFormInterface {

  /**
   * The reviewer entity.
   *
   * @var \Drupal\multi_peer_review\ReviewerInterface
   */
  protected $entity;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new ReviewerNewAccountForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $reviewer = $this->entity;
    
    $placeholder_replacements = [];
    MPRCommon::addWebsitePlaceholderReplacements($placeholder_replacements);    
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);  
    
    if ($this->isAdmin() == TRUE) {
        $res = $this->t('Create user account for %reviewer-full-name?', $placeholder_replacements);
    }
    else {
        $res = $this->t('Create a user account?', $placeholder_replacements);
    }
        
    return $res;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $reviewer = $this->entity;
    
    $placeholder_replacements = [];
    MPRCommon::addWebsitePlaceholderReplacements($placeholder_replacements);    
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);  
    
    return ($this->t('Complete and submit this form to open a reviewer account on %site-name. A confirmation email will be sent to %reviewer-email.', $placeholder_replacements)); 
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {   
    // Although overriding this method is required, it appears not to be used while viewed from the backend.
    // The returned Url is being properly handled here in case it is used somewhere or someday.
      
    // Example Url object toString() value: /admin/multi-peer-review/invitations
         
    if ($this->isAdmin() == TRUE) {
        $res = $this->entity->toUrl('collection');
    }
    else {
        $res = Url::fromRoute('<front>');
    }      
      
    return $res;
  }    
  
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Create user account');
  }  
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }
  

  
  public function isAdmin() {
    return MPRCommon::isAdminUser();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {


    $reviewer = $this->entity;

    $placeholder_replacements = [];
    MPRCommon::addWebsitePlaceholderReplacements($placeholder_replacements);    
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                

    // Suggest a username to ease the process.
    $username_suggestion = $reviewer->getFullName();
    $username_suggestion = strtolower(str_replace(' ', '_', trim($username_suggestion)));
    $username_suggestion = MPRCommon::stripNonAlphanumericCharacters($username_suggestion, '_');
    $username_suggestion = $username_suggestion . '_reviewer';


    $form['username'] = MPRCommon::getDefaultSingleLineTextFormField('Username', 
            'Several special characters are allowed, including space, period (.), hyphen (-), apostrophe (\'), underscore (_), and the @ sign.', TRUE, $username_suggestion, 
            UserInterface::USERNAME_MAX_LENGTH, $placeholder_replacements);
    $form['username']['#attributes'] = [
        'class' => ['username'],
        'autocorrect' => 'off',
        'autocapitalize' => 'off',        
        'spellcheck' => 'false',
    ];
    $form['username']['#size'] = 60;
    $form['username']['#weight'] = 10;


    $form['password'] = [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
        '#weight' => 11,
    ];    

    return parent::form($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
        
    // Access check.
    if (\Drupal\Core\Access\AccessResult::allowedIfHasPermission($this->currentUser(), 'administer multi_peer_review_users_add')->isAllowed() == TRUE) {
        $res = parent::buildForm($form, $form_state);
    }
    else {
        $res['#title'] = $this->t('Access denied.');
        $res['message'] = [
            '#type' => 'markup',
            '#markup' => $this->t('You do not have access to this page.'),
        ];        
    }
    
    return $res;
  }
  
  /**
   * {@inheritdoc}
   */    
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $reviewer = parent::validateForm($form, $form_state);
    
    $username = $form['username']['#value'];
    if (empty($username) == TRUE) {
        $form_state->setErrorByName('username', $this->t('Username is required.'));
    }
    else {
        $error = user_validate_name($username);
        if (empty($error) == FALSE) {
            $form_state->setErrorByName('username', $error);
        }
        else {
            // Check uniqueness.
            foreach (\Drupal\user\Entity\User::loadMultiple() as $user_id => $user) {
                if ($user->getUsername() == $username) {
                    $placeholder_replacements = ['%value' => $username];
                    $form_state->setErrorByName('username', $this->t('The username %value is already taken.', $placeholder_replacements));                    
                    break;
                }
            }            
        }
    }    
    
    if (empty($form['password']['#value']) == TRUE) {
        $form_state->setErrorByName('password', $this->t('Password is required.'));
    }    
        
    return $reviewer;
  }


  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {
      
    // 'Confirm' button clicked.
    $status = NULL; // Set default status.  
      
    // Load Reviewer entity.
    $reviewer = $this->entity;    
    
    $placeholder_replacements = [];
    MPRCommon::addWebsitePlaceholderReplacements($placeholder_replacements);    
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);           

    
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $user = \Drupal\user\Entity\User::create();
    
    $user->setUsername($form['username']['#value']);
    $user->setPassword($form['password']['#value']);
    $user->setEmail($reviewer->getEmail());
    $user->enforceIsNew();    
    $user->set('init', 'email');
    $user->set('langcode', $language);
    $user->set('preferred_langcode', $language);
    $user->set('preferred_admin_langcode', $language);    
    $user->addRole(MPRCommon::ROLE_REVIEWER);
    $user->activate();   
    
    $status = $user->save();    
    
    if ($status == SAVED_NEW) {
        // Change the owner of the Reviewer record.
    
        $new_user_id = intval($user->get('uid')->getValue()[0]['value']);
                
        $reviewer->set('uid', $new_user_id);
        $reviewer->setNewRevision();
        $status = $reviewer->save();
            
        if ($status == SAVED_UPDATED) {
            
            // Reload Reviewer.
            $reviewer = Reviewer::load($reviewer->id());
            
            
            // Change the owner of any Reviews linked to the Reviewer.
            foreach (Review::getReviews(FALSE) as $review) {
                $review->set('uid', $new_user_id);
                $review->setNewRevision();
                $review->save();
            }            
                    
                        
            $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
                    'reviewer_registered', 
                    [$reviewer], 
                    $reviewer->getEmail(), 
                    '', 
                    '', 
                    [] // No attachments.
                    );
            
            $email->send();
            
            $reviewer->logAction($this->logger('multi_peer_review'), 'user account generated');
            
            if ($this->isAdmin() == TRUE) {
                drupal_set_message(t('An account has been created successfully for %reviewer-full-name. An email has been sent to %reviewer-email.', $placeholder_replacements)); 
            }
            else {
                drupal_set_message(t('Thank you for registering an account with us. An email has been sent to %reviewer-email.', $placeholder_replacements)); 
            }
        }
        else {
            // A technical error has occurred.
            
            drupal_set_message(t('There was a problem creating the account. It has not been created.'), 'error'); 
            
            MPRCommon::sendEmailToTechnicalContact($this->t('A user account was generated but could not be attached to %reviewer-full-name. Please check for orphaned user accounts.', $placeholder_replacements));
        }
    }
    
        
    
    if ($status == NULL) {
        // Form will be rebuilt.
        $form_state->setRebuild();
    }
    else {         
        // Redirect the user to the home page. $status is typically SAVED_UPDATED.      
        $form_state->setRedirectUrl($this->getCancelUrl());          
    }    
    
  }
    
  
}

