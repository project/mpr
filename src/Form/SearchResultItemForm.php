<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;

/**
 * Form controller for the Search Result Item edit forms.
 */
class SearchResultItemForm extends ContentEntityForm implements SearchResultItemFormInterface {

  /**
   * The Search Result Item entity.
   *
   * @var \Drupal\multi_peer_review\SearchResultItemFormInterface
   */
  protected $entity;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new SearchResultItemForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }
  

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
      

      
    $dat = $this->entity;

    if ($dat->isNew() == TRUE) {
        $default_id = MPRCommon::getNewCustomTextId();
    }
    else {
        $default_id = $dat->label();
    }    
    
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit Search Result Item %label', ['%label' => $dat->label()]);
    }
        
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique ID'),
      '#maxlength' => 255,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#disabled' => !$dat->isNew(),           
    ];
    
    $form['title'] = MPRCommon::getDefaultSingleLineTextFormField('Title', 
            'Reviewer’s title such as Professor, Mrs, Sir.', FALSE, $dat->getTitle());  
    
    $form['first_name'] = MPRCommon::getDefaultSingleLineTextFormField('First Name', 
            'Reviewer’s first name or given name.', TRUE, $dat->getFirstName());    
    
    $form['last_name'] = MPRCommon::getDefaultSingleLineTextFormField('Last Name', 
            'Reviewer’s last name or family name.', TRUE, $dat->getLastName());                  

    $form['email'] = MPRCommon::getDefaultSingleEmailAddressFormField('Email', 
            'Reviewer’s email.', TRUE, $dat->getEmail());       
       

    
    
    $form['subjects'] = MPRCommon::getDefaultMultiLineTextFormField('Subjects',
            'The subjects that the Reviewer specialises in. Each subject must be entered on a single line.', FALSE, $dat->getSubjects());      

    
    
    $form['id_orcid'] = MPRCommon::getDefaultSingleLineTextFormField('ORCID', 
        'The Reviewer’s unique identifier on the ORCID registry (https://orcid.org/).', FALSE, $dat->getORCID()); 

    $form['id_isni'] = MPRCommon::getDefaultSingleLineTextFormField('ISNI', 
        'The Reviewer’s unique identifier on the ISNI platform (http://www.isni.org/).', FALSE, $dat->getISNI()); 
    
    $form['id_researcherid'] = MPRCommon::getDefaultSingleLineTextFormField('ResearcherID', 
        'The Reviewer’s unique identifier on the Publons platform (https://publons.com).', FALSE, $dat->getResearcherID());     
    
       

    

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Search Result Item ID'),
      '#maxlength' => 255,
      '#default_value' => $dat->id(),
      '#disabled' => !$dat->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\SearchResultItem::load',
      ],
      '#element_validate' => [],
    ];
    
    
    return parent::form($form, $form_state);
  }
  

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge([
      'label',
      'id',
    ], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    $field_names = [
      'label',
      'id',
    ];
    foreach ($violations->getByFields($field_names) as $violation) {
      list($field_name) = explode('.', $violation->getPropertyPath(), 2);
      $form_state->setErrorByName($field_name, $violation->getMessage());
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $dat = $this->entity;
    
    if ($dat->isNew() == TRUE) {
        // Indicate that the Search Result Item is being added manually.
        $dat->set('data_source', 'Manual');
    }
    
    //$dat->setNewRevision(TRUE);
    $status = $dat->save();

  }

}

