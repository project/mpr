<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\EmailTemplate;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Form controller for the Invitation retraction form.
 */
class InvitationRetractForm extends ContentEntityConfirmFormBase implements InvitationFormInterface {

  /**
   * The invitation entity.
   *
   * @var \Drupal\multi_peer_review\InvitationInterface
   */
  protected $entity;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new InvitationRetractForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $placeholders = MPRCommon::getEntityPlaceholderReplacements($this->entity->fabricateAndLoadPaper());
    
    return ($this->t('Retract Invitation to review %paper-title?', $placeholders));           
  }
  
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return ''; 
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {   
    // Although overriding this method is required, it appears not to be used while viewed from the backend.
    // The returned Url is being properly handled here in case it is used somewhere or someday.
      
    // Example Url object toString() value: /admin/multi-peer-review/invitations
    return $this->entity->toUrl('collection');
  }    
  
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Send Retraction to Reviewer');
  }  
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
      
    $invitation = $this->entity;
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();

    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                
    
    
    $form['paper_container'] = [
      '#type' => 'details',          
      '#title' => $this->t('Paper Details'),
      '#open' => FALSE,
    ];
    
   
    $form['paper_container']['header'] = [              
            '#type' => 'markup',
            '#markup' => '<dl>',
    ];
    $detail_fields = [
        'title' => $this->t('Title'), 
        'abstract' => $this->t('Abstract'), 
        'paper_type' => $this->t('Type'),
        'subjects' => $this->t('Subjects'),
    ];
    foreach ($detail_fields as $field_name => $field_label) {
        
        switch ($field_name) {
            case 'title':
                $row_value = $paper->label();
                break;
            case 'subjects':
                $row_value = $paper->getSubjectsCsv();
                break;
            default:
                $row_value = $paper->get($field_name)->getValue()[0]['value'];
                break;
        }        
                
    
        
        if (empty($row_value) == FALSE) {
            $form['paper_container'][$field_name]['title'] = [              
                '#type' => 'markup',
                '#markup' => '<dt>' . $field_label . '</dt>',            
            ];
            $form['paper_container'][$field_name]['content'] = [
                '#type' => 'markup',
                '#markup' => '<dd>' . $row_value . '</dd>',            
            ];        
        }
    }
    $form['paper_container']['footer'] = [              
            '#type' => 'markup',
            '#markup' => '</dl>',
    ];    
    
    
    
    $form['reviewer_container'] = [
      '#type' => 'details',          
      '#title' => $this->t('Reviewer Details'),
      '#open' => FALSE,
    ];
    
   
    $form['reviewer_container']['header'] = [              
            '#type' => 'markup',
            '#markup' => '<dl>',
    ];
    $detail_fields = [
        'title_and_full_name' => $this->t('Name'), 
        'email' => $this->t('Email'), 
        'phone' => $this->t('Phone'),
        'subjects' => $this->t('Subjects'),
    ];
    foreach ($detail_fields as $field_name => $field_label) {
        
        switch ($field_name) {
            case 'title_and_full_name':
                $row_value = $reviewer->getTitleAndFullName();
                break;
            case 'email':
                $row_value = $reviewer->getEmail();
                break;      
            case 'phone':
                $row_value = $reviewer->getPhone();
                break;               
            case 'subjects':
                $row_value = $reviewer->getSubjectsCsv();
                break;
            default:
                $row_value = $reviewer->get($field_name)->getValue()[0]['value'];
                break;
        }        
                
    
        
        if (empty($row_value) == FALSE) {
            $form['reviewer_container'][$field_name]['title'] = [              
                '#type' => 'markup',
                '#markup' => '<dt>' . $field_label . '</dt>',            
            ];
            $form['reviewer_container'][$field_name]['content'] = [              
                '#type' => 'markup',
                '#markup' => '<dd>' . $row_value . '</dd>',            
            ];        
        }
    }
    $form['reviewer_container']['footer'] = [              
            '#type' => 'markup',
            '#markup' => '</dl>',
    ];        
    
    
    
    $email_template = EmailTemplate::load('invitation_retract');
    
    $default_values = [];
    $default_values['email_subject'] = $email_template->getSubject();
    $default_values['email_body'] = $email_template->getBody();
    
    
    $form['email_attachments_control'] = MPRCommon::getDefaultMultiFileFormField('Attachments',
            'One or more files to include in the retraction email. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, FALSE, NULL, MPRCommon::DOCUMENT_FILE_EXTENSIONS);        
    
    $form['email_subject'] = MPRCommon::getDefaultSingleLineTextFormField('Email Subject', 
            'Subject line of the retraction email.', TRUE, $default_values['email_subject']);         
    
    $form['email_body'] = MPRCommon::getDefaultHtmlTextFormField('Email Body', 
            'Body of the retraction email.', TRUE, $default_values['email_body']);    
       
    $form['email_recipient_options_container'] = [
      '#type' => 'details',          
      '#title' => $this->t('Additional Email Recipients'),
      '#open' => FALSE,
    ];       
    
    $form['email_recipient_options_container']['email_cc'] = MPRCommon::getDefaultMultiLineTextFormField('Email CC', 
            'Email addresses of other recipients who should receive a copy of the retraction email. Separate each email address using commas.', FALSE, '');            
    $form['email_recipient_options_container']['email_cc']['#rows'] = 2;
       
    $form['email_recipient_options_container']['email_bcc'] = MPRCommon::getDefaultMultiLineTextFormField('Email BCC', 
            'Email addresses of other recipients who should receive a copy of the retraction email without the knowledge of other recipients. Separate each email address using commas.', FALSE, '');     
    $form['email_recipient_options_container']['email_bcc']['#rows'] = 2;    
    


    return parent::form($form, $form_state);
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    
    MPRCommon::filterHtmlTextFormField($form, $form_state, 'email_body');
  }
  
  
  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    // 'Confirm' button clicked.
    $status = NULL; // Set default status.  
      
    // Load Invitation, Paper and Reviewer entities.  
    $invitation = $this->entity;    
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();
      
    $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
                'invitation_retract',
                [$invitation, $paper, $reviewer],
                $reviewer->getEmail(),
                $form['email_recipient_options_container']['email_cc']['#value'],
                $form['email_recipient_options_container']['email_bcc']['#value'],
                [] // No attachment from entities.
    );
 
        
    $email->subject = $form['email_subject']['#value'];        
    $email->body = $form['email_body']['value']['#value'];
    
    
    $email->addAttachmentsFromForm($form['email_attachments_control']);
    

    if ($email->send() == TRUE) {
        drupal_set_message(t('The retraction has been sent.'));        
        $invitation->logAction($this->logger('multi_peer_review'), 'retracted');
        
        // Update Invitation status.
        $invitation->set('status', Invitation::STATUS_RETRACTED);
        $invitation->set('retracted_timestamp', time());
        $invitation->setNewRevision(TRUE);
        $status = $invitation->save();
        
        if ($status == SAVED_UPDATED) {
            // Update Reviewer statistics.
            $pending_invitations = $reviewer->get('cached_pending_invitations')->getValue();
            if (empty($pending_invitations) == FALSE) {                
                $pending_invitations = intval($pending_invitations);
                $pending_invitations--;
                
                $reviewer->get('cached_pending_invitations')->setValue($pending_invitations);
                $reviewer->save();                
            }        
            
            
            // Update Paper status.
            Paper::rebuildCachedFigures($paper);            
        }
        
    } 
    else {
        drupal_set_message(t('There was a problem sending the retraction. It has not been sent.'), 'error'); 
    }    
    
    
    
    if ($status == NULL) {
        // Form will be rebuilt.
        $form_state->setRebuild();
    }
    else {         
        // Redirect the user to the home page. $status is typically SAVED_UPDATED.      
        $form_state->setRedirectUrl($this->getCancelUrl());          
    }    
    
  }
    
  
}

