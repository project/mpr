<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPREmail;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class UserPaperForm.
 *
 * @package Drupal\multi_peer_review\Form
 */
class UserPaperForm extends CommonFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_paper_form';
  }

  
  public function getPaper() {
    $res = NULL;  
    $current_route = \Drupal::routeMatch();     
    $paper_id = $current_route->getParameters()->get('paper');
    if (empty($paper_id) == FALSE) {
        $res = Paper::load($paper_id);
    }
    return $res;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
                      
    $user_id = $this->getUserId();        
    $paper = $this->getPaper();
    $is_new = empty($paper);
    $allow_access = FALSE;
    
    $default_values['label'] = '';
    $default_values['abstract'] = '';
    $default_values['paper_type'] = NULL;
    $default_values['file'] = NULL;
    $default_values['alert_recipients'] = '';
    $default_values['subjects'] = '';
    
    if ($is_new == FALSE) {
        if (($paper->getOwnerId() == $user_id) && ($paper->getStatus() == Paper::STATUS_IDLE)) {
            $default_values['label'] = $paper->label();
            $default_values['abstract'] = $paper->getAbstract();
            $default_values['paper_type'] = $paper->getPaperType();
            $default_values['file'] = $paper->getFile();
            $default_values['alert_recipients'] = $paper->getAlertRecipients();
            $default_values['subjects'] = $paper->getSubjects();
            
            $allow_access = TRUE;
        }
    }
    else {
        $allow_access = TRUE;
    }
        
    if ($allow_access == TRUE) {
    
        $form['label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Title'),
          '#maxlength' => 255,
          '#default_value' => $default_values['label'],
          '#required' => TRUE,
        ];    


        $form['abstract'] = MPRCommon::getDefaultHtmlTextFormField('Abstract', 
                'The Paper’s abstract.', TRUE, $default_values['abstract']);        

        $form['paper_type'] = MPRCommon::getDefaultDropDownFormField('Type', 
                'The type of Paper.', TRUE, $default_values['paper_type'], MPRCommon::getTranslatedListOptionsFromConfig('paper_types'));    

        $form['file'] = MPRCommon::getDefaultFileFormField('File', 
                'The actual Paper file. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, TRUE, $default_values['file'], MPRCommon::DOCUMENT_FILE_EXTENSIONS);

        $form['alert_recipients'] = MPRCommon::getDefaultMultiLineTextFormField('Alert Recipients',
                'Email addresses of people who should receive updates about the Paper. Each email address should be separated by a comma.', FALSE, $default_values['alert_recipients']);
        // Shorten the height of the alert recipients field
        $form['alert_recipients']['#rows'] = 2; 

        $form['subjects'] = MPRCommon::getDefaultMultiLineTextFormField('Subjects',
                'The subjects that the Paper relates to. Each subject must be entered on a single line.', FALSE, $default_values['subjects']);      





        // Custom flag to check if we should override form values
        $force_value_override = $form_state->getValue('force_value_override');
        if (empty($force_value_override) == TRUE) {
            $force_value_override = FALSE;
        } else {
            if ($force_value_override == '1') {
                $force_value_override = TRUE;
            }
            else {
                $force_value_override = FALSE;
            }
        }

        $contributor_types = $this->getContributorTypes();


        foreach ($contributor_types as $key => $value) {

            $container_name = $key . '_container';

            $form[$container_name] = [
              '#type' => 'details',          
              '#title' => $this->t($value['title']),
              '#description' => $this->t($value['description']),
              '#open' => TRUE,
            ];    




            // Read contributor XML data
            $xml_data = $form_state->getValue($key);
            if (empty($xml_data) == TRUE) {
                $xml_data = $value['default_xml_data'];
            }

            if (($xml_data != '') && strpos($xml_data,'<contributor>') !== FALSE) {

                $contributors_dom = simplexml_load_string($xml_data);
                if ($contributors_dom != NULL) {

                    $index = 0;
                    foreach ($contributors_dom->children() as $contributor) {
                        $form_field_index = '_inxid' . strval($index);
                        $form_field_id = $key . '_container_item' . $form_field_index;

                        $container_title = trim(trim(strval($contributor->first_name) . ' ' . strval($contributor->last_name)) . ' ' . strval($contributor->entity_name));                    
                        if ($container_title == '') {
                            $container_title = $this->t('Contributor') . ' ' . strval($index + 1);
                        }                

                        $form[$container_name][$form_field_id] = [
                          '#type' => 'details',
                          '#title' => $container_title, // Names do not require translation        
                          '#open' => FALSE,                      
                        ];


                        $element = MPRCommon::getDefaultSingleLineTextFormField('First Name', 'First name or given name of this contributor.', FALSE, strval($contributor->first_name), 100); 
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->first_name);
                        }
                        $element['#access'] = $value['personal_details_fields'];
                        $form[$container_name][$form_field_id][$key . 'first_name' . $form_field_index] = $element;

                        $element = MPRCommon::getDefaultSingleLineTextFormField('Last Name', 'Last name or family name of this contributor.', FALSE, strval($contributor->last_name), 100);
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->last_name);
                        }                
                        $element['#access'] = $value['personal_details_fields'];
                        $form[$container_name][$form_field_id][$key . 'last_name' . $form_field_index] = $element;


                        $element = MPRCommon::getDefaultSingleLineTextFormField('Name', 'Organisation/entity name of this contributor.', FALSE, strval($contributor->entity_name), 100);
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->entity_name);
                        }                
                        $element['#access'] = !$value['personal_details_fields'];
                        $form[$container_name][$form_field_id][$key . 'entity_name' . $form_field_index] = $element;                        


                        $element = MPRCommon::getDefaultCheckBoxFormField('Author', 'If this contributor is an author of this Paper, this field should be ticked.', strval($contributor->is_author));
                        if ($force_value_override == TRUE) {
                            if (strval($contributor->is_author) == '1') {
                                $element['#value'] = TRUE;
                            } 
                            else {
                                $element['#value'] = FALSE;
                            }
                        }                  
                        $form[$container_name][$form_field_id][$key . 'is_author' . $form_field_index] = $element;


                        $element = MPRCommon::getDefaultMultiLineTextFormField('Affilations', 'Affiliations of the author/contributor. Each affiliation must be entered on a single line.', FALSE, strval($contributor->affiliations));
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->affiliations);
                        }   
                        $form[$container_name][$form_field_id][$key . 'affiliations' . $form_field_index] = $element;


                        $element = MPRCommon::getDefaultSingleLineTextFormField('ORCID', 'The unique identifier of the author/contributor on the ORCID registry (https://orcid.org/).', FALSE, strval($contributor->id_orcid), 25);
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->id_orcid);
                        }                
                        $form[$container_name][$form_field_id][$key . 'id_orcid' . $form_field_index] = $element;


                        $element = MPRCommon::getDefaultSingleLineTextFormField('ISNI', 'The unique identifier of the author/contributor on the ISNI platform (http://www.isni.org/).', FALSE, strval($contributor->id_isni), 25);
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->id_isni);
                        }                
                        $form[$container_name][$form_field_id][$key . 'id_isni' . $form_field_index] = $element;


                        $element = MPRCommon::getDefaultSingleLineTextFormField('ResearcherID', 'The unique identifier of the author/contributor on the Publons platform (https://publons.com).', FALSE, strval($contributor->id_researcherid), 25);
                        if ($force_value_override == TRUE) {
                            $element['#value'] = strval($contributor->id_researcherid);
                        }                
                        $form[$container_name][$form_field_id][$key . 'id_researcherid' . $form_field_index] = $element;                


                        $form[$container_name][$form_field_id][$key . 'contributor_delete' . $form_field_index] = [                  
                            '#type' => 'button',
                            '#name' => 'contributor_delete' . $form_field_index,
                            '#value' => 'Delete this Contributor',
                            '#button_type' => 'danger',
                            '#attributes' => ['class' => ['delete-contributor-' . str_replace('_', '-', $form_field_id)]],
                            '#executes_submit_callback' => TRUE,
                            '#submit' => ['::removeContributorCallback'],                    
                        ];  

                        $index++;
                    }            

                }
            }


            $form[$container_name][$key . 'contributor_add'] = [
                '#type' => 'button',
                '#name' => $container_name . '_contributor_add',
                '#value' => 'Add Contributor',
                '#attributes' => ['class' => ['add-contributor-' . str_replace('_', '-', $container_name)]], 
                '#executes_submit_callback' => TRUE,
                '#submit' => ['::addContributorCallback'],
            ];    
        }     

        // Reset custom flags
        $form_state->setValue('force_value_override', '0');    


    }


   
    
    // Action form elements.
    $form['actions'] = [
        '#type' => 'actions',
        '#attributes' => [
            'class' => ['form-actions', 'js-form-wrapper', 'form-wrapper'],
        ],
    ];
    if ($allow_access == TRUE) {
        $form['actions']['update_button'] = [
            '#type' => 'submit',           
            '#value' => $this->t('Save'),        
            '#button_type' => 'primary',
        ];    
    }
    $form['actions']['cancel_link'] = [
        '#type' => 'link',
        '#url' => Url::fromRoute('multi_peer_review.account.papers', ['user' => $user_id]),
        '#title' => 'Cancel',
    ];        

    


    return $form;
  }


  /**
   * Returns an array containing contributor types.
   */  
  public function getContributorTypes() {
    
    $default_values['individual_contributors'] = '';
    $default_values['non_individual_contributors'] = '';
    
    $paper = $this->getPaper();
    if (empty($paper) == FALSE) {
        $default_values['individual_contributors'] = $paper->getIndividualContributors();
        $default_values['non_individual_contributors'] = $paper->getNonIndividualContributors();
    }
    
    $contributor_types = [
        'individual_contributors' => [
            'title' => 'Individual Contributors', 
            'description' => 'People who authored or contributed to this paper.',
            'default_xml_data' => $default_values['individual_contributors'], 
            'personal_details_fields' => TRUE,
            ],
        'non_individual_contributors' => [
            'title' => 'Non Individual Contributors', 
            'description' => 'Organisations, teams and other non-individual entities who authored or contributed to this paper.',
            'default_xml_data' => $default_values['non_individual_contributors'], 
            'personal_details_fields' => FALSE,
            ],
    ];
    return $contributor_types;
  }
  
  /**
   * Submit button callback that deletes a given contributor.
   */  
  public function removeContributorCallback($form, FormStateInterface $form_state) {
      
    // Verify that the caller is the delete contributor button
    $remove_index = -1;
    $data_source = '';
    $caller = $form_state->getTriggeringElement();
    
    if (($caller != NULL) && (array_key_exists('#attributes', $caller) == TRUE)) {
        if (array_key_exists('class', $caller['#attributes']) == TRUE) {
        
            foreach($caller['#attributes']['class'] as $class_item) {                
                if (strpos($class_item, 'delete-contributor-') !== FALSE) {
                    
                    // Caller class contains the target index 
                    $remove_index = intval(substr($class_item, strpos($class_item, '-inxid') + 6));
                    
                    // Determine the data source that this caller relates to
                    if (strpos(str_replace('-', '_', $class_item), 'non_individual_contributors') === FALSE) {
                        $data_source = 'individual_contributors';
                    }
                    else {
                        $data_source = 'non_individual_contributors';
                    }                    
                    
                    break;
                }
            }
            
        }
    }
    
    if ($remove_index != -1) {        
        $xml_data = $form_state->getValue($data_source);
        if (empty($xml_data) == FALSE) {
            
            // If there are no contributors, there would be no XML data and this function would not be called.
            // Therefore, it is safe to assume at least one contributor exists.
            $xml_parts = explode('<contributor>', $xml_data);
            if (count($xml_parts) <= 2) {
                // This is the last contributor in the dataset
                $xml_data = '<?xml version="1.0" encoding="UTF-8"?><contributors></contributors>';
            }
            else {
                $new_xml_data = $xml_parts[0]; // XML header

                $index = 0;            
                foreach($xml_parts as $xml_part) {
                    if ($index != 0) {
                        if ($remove_index != ($index - 1)) {

                            $new_xml_data .= '<contributor>';
                            $new_xml_data .= substr($xml_part, 0, strpos($xml_part, '</contributor>'));
                            $new_xml_data .= '</contributor>';
                        }
                    }

                    $index++;
                }
                
                $new_xml_data .= '</contributors>'; // XML footer
                
                $xml_data = $new_xml_data;                
            }
            
            $form_state->setValue($data_source, $xml_data);
            $form_state->setValue('force_value_override', '1');
        }                  
    }
    
    $form_state->setRebuild(TRUE);
      
  }
  
  /**
   * Submit button callback that adds a blank contributor.
   */  
  public function addContributorCallback($form, FormStateInterface $form_state) {
    
    $eof = '</contributors>';
    $blank_contributor_data = '<contributor>
        <first_name></first_name>
        <last_name></last_name>
        <entity_name></entity_name>
        <is_author></is_author>
        <affiliations></affiliations>
        <id_orcid></id_orcid>
        <id_isni></id_isni>
        <id_researcherid></id_researcherid>        
        </contributor>';
    
    
    $data_source = '';
    $caller = $form_state->getTriggeringElement();
    
    if (($caller != NULL) && (array_key_exists('#attributes', $caller) == TRUE)) {
        if (array_key_exists('class', $caller['#attributes']) == TRUE) {
        
            foreach($caller['#attributes']['class'] as $class_item) {                
                if (strpos($class_item, 'add-contributor-') !== FALSE) {
                    
                    // Determine the data source that this caller relates to
                    if (strpos(str_replace('-', '_', $class_item), 'non_individual_contributors') === FALSE) {
                        $data_source = 'individual_contributors';
                    }
                    else {
                        $data_source = 'non_individual_contributors';
                    }
                                        
                    break;
                }
            }
            
        }
    }    
    
    if ($data_source != '') {
      
        $xml_data = $form_state->getValue($data_source);
        if (empty($xml_data) == FALSE) {

            if (strpos($xml_data, '<contributor>') === FALSE) {
                $xml_data = '<?xml version="1.0" encoding="UTF-8"?><contributors>' . $blank_contributor_data . $eof;            
            }
            else {
                $xml_data = substr($xml_data, 0, strpos($xml_data, $eof));
                $xml_data .= $blank_contributor_data . $eof;            
            }

            $form_state->setValue($data_source, $xml_data);
        }   

    }
    
    
    $form_state->setRebuild(TRUE);
  }
  
  

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    
    
    MPRCommon::filterHtmlTextFormField($form, $form_state, 'abstract');
    
                
    // Read contributor interface and store as XML
    $contributor_types = $this->getContributorTypes();
    foreach ($contributor_types as $key => $value) {
        $container_name = $key . '_container';
        
        // Create XML representation of contributor data
        $xw = xmlwriter_open_memory();
        xmlwriter_set_indent($xw, 1);
        xmlwriter_set_indent_string($xw, ' ');  

        xmlwriter_start_document($xw, '1.0', 'UTF-8');

        // Root element
        xmlwriter_start_element($xw, 'contributors');

        // Iterate through each contributor details form field
        $contributor_item_keys = array_keys($form[$container_name]); 
        
        //var_dump($contributor_item_keys);exit();
        $index = 0;
        foreach ($contributor_item_keys as $key_name) {
            $pos = strpos($key_name, $container_name . '_item_');
            if (($pos !== FALSE) && ($pos == 0)) {                
                $contributor_fields = $form[$container_name][$key_name];

                // Add contributor element
                xmlwriter_start_element($xw, 'contributor');

                // Add fields
                foreach (['first_name', 'last_name', 'entity_name', 'is_author', 'affiliations', 'id_orcid', 'id_isni', 'id_researcherid'] as $field_name)
                {
                    $indexed_field_name = $key . $field_name . '_inxid' . strval($index);

                    xmlwriter_start_element($xw, $field_name);
                    xmlwriter_text($xw, $contributor_fields[$indexed_field_name]['#value']);
                    xmlwriter_end_element($xw);             
                }

                // End contributor element
                xmlwriter_end_element($xw); 
                $index++;
            }         
        }

        // End root element
        xmlwriter_end_element($xw);
        xmlwriter_end_document($xw);

        $xml_output = xmlwriter_output_memory($xw);
        $form_state->setValue($key, $xml_output);             
    }
        
  }  
  

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {  

    $status = NULL;
    $user_id = $this->getUserId();   
    $paper = $this->getPaper();
    $is_new = empty($paper);
    $allow_access = FALSE;
    
    if ($is_new == FALSE) {
        if (($paper->getOwnerId() == $user_id) && ($paper->getStatus() == Paper::STATUS_IDLE)) {
            $allow_access = TRUE;
        }
    }
    else {
        $allow_access = TRUE;
    }  
    
    
    if ($allow_access == TRUE) {
        if ($is_new == TRUE) {
            $paper = Paper::create([
                'id' => MPRCommon::getNewCustomTextId(),
                'uid' => $user_id,
            ]);
        }
        
        $paper->set('label', $form['label']['#value']);
        $paper->set('abstract', $form['abstract']['value']['#value']);
        $paper->set('paper_type', $form['paper_type']['#value']);
        $paper->set('file', $form['file']['#value']['fids'][0]);
        $paper->set('alert_recipients', $form['alert_recipients']['#value']);
        $paper->set('subjects', $form['subjects']['#value']);
        
        // Read contributors.
        $contributor_types = $this->getContributorTypes();
        foreach ($contributor_types as $key => $value) {
            $paper->set($key, $form_state->getValue($key));
        }        
        
        $paper->setNewRevision(TRUE);
        $status = $paper->save();
        
        if (($is_new == TRUE) && ($status == SAVED_NEW)) {
            
            // Send an email to all staff.            
            // Find all staff users first.
            $staff_user_ids = \Drupal::entityQuery('user')
            ->condition('status', 1)
            ->condition('roles', MPRCommon::ROLE_STAFF)
            ->execute();
            $staff_users = \Drupal\user\Entity\User::loadMultiple($staff_user_ids);
            
            // Compile a list of email addresses.
            $recipients = [];
            foreach ($staff_users as $staff_user) {
                array_push($recipients, $staff_user->getEmail());
            }
            
            // Prepare and send email.
            if (count($recipients) != 0) {
                
                $recipients = implode(', ', $recipients);
                
                // Reload paper.
                $paper = Paper::load($paper->id());
                
                $email = MPREmail::createFromEmailTemplate(
                        'paper_uploaded', 
                        [$paper], 
                        $recipients, 
                        '', 
                        '', 
                        [$paper]
                );
                
                $email->send();
            }
            
        }
    }
    
      
    
    if ($status != NULL){
               
        $info = ['%info' => $paper->label()];
        $context = ['@type' => $paper->bundle(), '%info' => $paper->label()];
        $logger = $this->logger('multi_peer_review');        
        
        $destination_url = Url::fromRoute('multi_peer_review.account.papers', ['user' => $user_id]);
        
        switch ($status) {
            case SAVED_UPDATED:
                $logger->notice('@type: updated %info.', $context);      
                $this->messenger->addMessage($this->t('Paper %info has been updated.', $info));
                                                                          
                $form_state->setRedirectUrl($destination_url);                
                break;
            case SAVED_NEW:
                $logger->notice('@type: added %info.', $context);
                $this->messenger->addMessage($this->t('Paper %info has been uploaded.', $info));

                   
                $form_state->setRedirectUrl($destination_url);  
                break;
            default:
                $this->messenger->addError($this->t('There was an error saving the Paper.'));     
                
                $form_state->setRebuild();       
                break;
        }
        
        
        
    }

  }

}