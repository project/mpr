<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class UserReviewsForm.
 *
 * @package Drupal\multi_peer_review\Form
 */
class UserReviewsForm extends CommonFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_reviews_form';
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
                
    $user_id = $this->getUserId();
    $show_reset_button = FALSE;
    $papers = [];    
    
    // Check for any pending Invitations.
    $invitations = [];
    foreach (Reviewer::getReviewers(FALSE, NULL, $user_id) as $reviewer) {
        foreach (Invitation::getInvitations(FALSE, NULL, Invitation::STATUS_PENDING, NULL, $reviewer->id()) as $invitation) {
            
            $paper = $invitation->fabricateAndLoadPaper();
            if (empty($paper) == FALSE) {
                $papers[$paper->id()] = $paper;
                array_push($invitations, $invitation);
            }
        }
    }
        
    if (count($invitations) != 0) {
        
        Invitation::sortInvitationsByDeadlineDate($invitations);
        
        
        $form['invitations_container'] = [
            '#type' => 'details',          
            '#title' => $this->t('Pending Invitations'),
            '#open' => TRUE,
        ];

        // Output table of Invitations.
        $form['invitations_container']['invitations'] = [
            '#type' => 'table',
            '#header' => [
                'paper' => $this->t('Paper'),
                'target_start_date' => $this->t('Target Start Date'),
                'deadline' => $this->t('Deadline'),            
                'operations' => $this->t('Operations'),      
            ],
            '#empty' => $this->t('No invitations found.'),
        ];    
        
        $rows = [];
        foreach ($invitations as $invitation) {
            
            $paper = $papers[$invitation->getPaper()];
            $paper_view_link = Url::fromRoute('multi_peer_review.account.papers.paper.view', [
                'user' => $user_id,
                'paper' => $paper->id(),
            ]);     
            
            $row = [
                'paper' => $paper->label(),
                'target_start_date' => MPRCommon::getFormattedDateText($invitation->getTargetStartDate(), MPRCommon::NAMED_DATE_FORMAT_SHORT_DATE),
                'deadline' => MPRCommon::getFormattedDateText($invitation->getDeadline(), MPRCommon::NAMED_DATE_FORMAT_SHORT_DATE),
                'operations' => $invitation->getDeadline(),            
            ];
                   
            $row['paper'] = new FormattableMarkup(
                    '<a href="' . $paper_view_link->toString() . '">@paper-title</a>',
                    [
                        '@paper-title' => $paper->label(),
                    ]
            );
            

            $submit_review_link = Url::fromRoute('multi_peer_review.account.invitations.invitation.accept', [
                'user' => $user_id,
                'invitation' => $invitation->id(),
            ]);
            
            $cancel_review_link = Url::fromRoute('multi_peer_review.account.invitations.invitation.decline', [
                'user' => $user_id,
                'invitation' => $invitation->id(),
            ]);                

            $row['operations'] = new FormattableMarkup('<ul>
                <li><a href="' . $submit_review_link->toString() . '">@accept-label</a></li>
                <li><a href="' . $cancel_review_link->toString() . '">@decline-label</a></li></ul>', 
                    [
                        '@accept-label' => $this->t('Accept Invitation'),  
                        '@decline-label' => $this->t('Decline Invitation'),                 
                    ]
                );


            array_push($rows, $row);        
        }
        $form['invitations_container']['invitations']['#rows'] = $rows;
    
    }
    
    
    
    // Filter Reviews.
    $status = \Drupal::request()->request->get('review_status');
    if ($status == '_none') {
        $status = NULL;
    }
    
    if ($status != NULL) {
        $show_reset_button = TRUE;
    }
    
    $reviews = Review::getReviews(FALSE, NULL, $status, NULL, $user_id);    
    
    // Remove Reviews that are not linked to Papers and build Paper/Review dictionary.
    $filtered_reviews = [];
    $papers = [];
    foreach ($reviews as $review) {
        $invitation = $review->fabricateAndLoadInvitation();
        if (empty($invitation) == FALSE) {
            $paper = $invitation->fabricateAndLoadPaper();
            if (empty($paper) == FALSE) {
                $papers[$review->id()] = $paper;
                
                array_push($filtered_reviews, $review);
            }
        }
    }
    $reviews = $filtered_reviews;
    
    // Filter by keywords.
    $keywords = \Drupal::request()->request->get('review_keywords');
    if (empty($keywords) == FALSE) {
        
        // Isolate each keyword.
        $keyword_list = explode(' ', strtolower(trim($keywords)));
        
        $filtered_reviews = [];
        foreach ($reviews as $review) {
                        
            $meta = strtolower($review->getCachedSearchMetaData());
                        
            foreach ($keyword_list as $keyword) {
                if (strpos($meta, $keyword) !== FALSE) {
                    
                    // Add to filtered array.
                    array_push($filtered_reviews, $review);
                    
                    break;
                }
            }
        }
         
        $reviews = $filtered_reviews;
        
        $show_reset_button = TRUE;
    }    
    
    Review::sortReviewsByPriority($reviews);
    
    
    
    
    // Configure form layout.    
    $form['filters_container'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['form--inline', 'clearfix'],
        ],        
    ];    
    
    // Configure form actions.
    $options = Review::getTranslatedStatusListOptions();        
    $form['filters_container']['review_status'] = MPRCommon::getDefaultDropDownFormField('Status', '', FALSE, '', $options);
    $form['filters_container']['review_keywords'] = MPRCommon::getDefaultSingleLineTextFormField('Keywords', '', FALSE, '');
    
    
    $form['filters_container']['actions'] = [
        '#type' => 'actions',
        '#attributes' => [
            'class' => ['form-actions', 'js-form-wrapper', 'form-wrapper'],
        ],
    ];
    $form['filters_container']['actions']['filter_button'] = [
        '#type' => 'submit',           
        '#value' => $this->t('Filter'),        
    ];
    if ($show_reset_button == TRUE) {
        $form['filters_container']['actions']['reset_button'] = [
            '#type' => 'submit',        
            '#value' => $this->t('Reset'),        
        ];    
    }    
    
    
    
    // Output table of Reviews.
    $form['reviews'] = [
        '#type' => 'table',
        '#header' => [
            'paper' => $this->t('Paper'),
            'status' => $this->t('Status'),
            'start' => $this->t('Start'),
            'deadline' => $this->t('Deadline'),            
            'operations' => $this->t('Operations'),      
        ],
        '#empty' => $this->t('No reviews found.'),
        '#weight' => 100,
    ];
    
    $rows = [];
    $row_count = 0;
    $row_max = intval(MPRCommon::getConfigValue('default_front_end_display_row_limit'));
    foreach ($reviews as $review) {
        
        $overdue = FALSE;
        $status_text = $review->getStatus();
        if (($status_text == Review::STATUS_PENDING) && ($review->getOverdue() == TRUE)) {
            $status_text = 'Pending (Overdue)';        
            $overdue = TRUE;
        }
        
        $paper = $papers[$review->id()];
        $paper_view_link = Url::fromRoute('multi_peer_review.account.papers.paper.view', [
            'user' => $user_id,
            'paper' => $paper->id(),
        ]);          
           
        
        $row = [
            'paper' => $paper->label(),
            'status' => $this->t($status_text),
            'start' => $review->getStartDateText(),
            'deadline' => $review->getDeadlineText(),            
        ];
        
        $row['paper'] = new FormattableMarkup(
                '<a href="' . $paper_view_link->toString() . '">@paper-title</a>',
                [
                    '@paper-title' => $paper->label(),
                ]
        );        
        
        if ($overdue == TRUE) {
            $row['status'] = new FormattableMarkup('<strong>@status</strong>', ['@status' => $this->t($status_text)]);                                  
        }
                
        switch ($review->getStatus()) {            
            case Review::STATUS_PENDING:
                
                $submit_review_link = Url::fromRoute('multi_peer_review.account.reviews.review.submit', [
                    'user' => $user_id,
                    'review' => $review->id(),
                ]);
                
                $cancel_review_link = Url::fromRoute('multi_peer_review.account.reviews.review.cancel', [
                    'user' => $user_id,
                    'review' => $review->id(),
                ]);                
                
                $row['operations'] = new FormattableMarkup('<ul>
                    <li><a href="' . $submit_review_link->toString() . '">@submit-review-label</a></li>
                    <li><a href="' . $cancel_review_link->toString() . '">@cancel-review-label</a></li></ul>', 
                        [
                            '@submit-review-label' => $this->t('Submit Review'),  
                            '@cancel-review-label' => $this->t('Cancel Review'),                 
                        ]
                    );
                
                break;        
            case Review::STATUS_SUBMITTED:
                
                $view_review_link = Url::fromRoute('multi_peer_review.account.reviews.review.view', [
                    'user' => $user_id,
                    'review' => $review->id(),
                ]);         
                
                $row['operations'] = new FormattableMarkup('<ul>
                    <li><a href="' . $view_review_link->toString() . '">@view-review-label</a></li>', 
                        [
                            '@view-review-label' => $this->t('View Review'),  
                        ]
                    );                
                
                break;
            default:
                $row['operations'] = '';
                break;
        }
        
        
        array_push($rows, $row);        
        
        $row_count++;
        if ($row_count >= $row_max) {
            break;
        }
    }
    $form['reviews']['#rows'] = $rows;
     

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {  
      
    $operation = $form_state->getValue('op')->getUntranslatedString();
    switch ($operation) {
        case 'Filter':

            // Form will be rebuilt.
            $form_state->setRebuild();   
            
            break;
        case 'Reset':

            // Do nothing as this will reset the form.
            
            break;
    }
        
  }

}