<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Review;
use Drupal\multi_peer_review\Entity\Invitation;


/**
 * Form controller for the Invitation acceptance form.
 */
class InvitationAcceptForm extends CommonConfirmForm implements InvitationFormInterface {


  /**
   * Constructs a new InvitationAcceptForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $this->entity);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $this->entity->fabricateAndLoadPaper());
    
    return ($this->t('Accept invitation to review %paper-title?', $placeholder_replacements));           
  }
    
  /**
   * {@inheritdoc}
   */  
  public function getFrontEndUserCancelUrl($user_id) {
    return Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]);
  }
  

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
      
    $invitation = $this->entity;
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();

    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                
    
    
    $this->insertPaperDetails($form, $paper, TRUE, FALSE);
    
    
    $form['preferred_start_date'] = MPRCommon::getDefaultDateFormField('Preferred start date', 
            'If you need some time before starting to review this paper, please enter your preferred start date, otherwise the start date will be %invitation-target-start-date.', 
            FALSE, NULL, $placeholder_replacements);
    

    $form['accept'] = MPRCommon::getDefaultCheckBoxFormField('I, %reviewer-title-and-full-name, agree to review %paper-title by %invitation-deadline.', 
            '', FALSE, $placeholder_replacements);
    $form['accept']['#required'] = TRUE;


    return parent::form($form, $form_state);
  }
  
  
  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {


    // 'Confirm' button clicked.
    // Mark the invitation as accepted and inform the invite sender (Owner of the record).
    $status = NULL; // Set default status.
    
                
    // Load Invitation, Paper and Reviewer entities.  
    $invitation = $this->entity;    
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();
    
    
    // Update Invitation status.        
    $invitation->set('status', Invitation::STATUS_ACCEPTED);
    $invitation->set('accepted_timestamp', time());    
    
    // Collect the reviewer's preferred date or default to the target start date set by the team.
    $preferred_start_date = $form['preferred_start_date']['#value'];
    if (empty($preferred_start_date) == TRUE) {
        $preferred_start_date = $invitation->getTargetStartDate();
    }    
    $invitation->set('preferred_start_date', $preferred_start_date);
    
    
    $invitation->setNewRevision(TRUE);
    $status = $invitation->save();

    if ($status == SAVED_UPDATED) {
        // Update Reviewer statistics.
        // Remove pending Invitation.
        $pending_invitations = $reviewer->get('cached_pending_invitations')->getValue();
        if (empty($pending_invitations) == FALSE) {            
            $pending_invitations = intval($pending_invitations);
            $pending_invitations--;
            $reviewer->get('cached_pending_invitations')->setValue($pending_invitations);
        }        

        // Add pending Review.
        $pending_reviews = $reviewer->get('cached_pending_reviews')->getValue();
        if (empty($pending_reviews) == FALSE) {     
            $pending_reviews = 0;
        }
        else {            
            $pending_reviews = intval($pending_reviews);
        }            
        $pending_reviews++;
        $reviewer->get('cached_pending_reviews')->setValue($pending_reviews);        


        $reviewer->save();        
        
     
        

        // Reload Invitation.
        $invitation = \Drupal\multi_peer_review\Entity\Invitation::load($invitation->id());


        // Create pending Review.
        // Note that email_hash is set by EmailCapableEntity save().
        $new_review_id = MPRCommon::getNewCustomTextId();
        $review = \Drupal\multi_peer_review\Entity\Review::create([
            'id' => $new_review_id, 
            'label' => $new_review_id, 
            'invitation' => $invitation->id(),
            'status' => Review::STATUS_PENDING,
            'start_date' => $preferred_start_date,
            'deadline' => $invitation->getDeadline(),
            'archived' => FALSE,            
            'uid' => $reviewer->getOwnerId(),   // The owner of the Reviewer record is the owner of the Review record.
        ]);
        // The Review Owner Id should match the Reviewer's Owner Id, at least at the beginning.
        $review->setOwnerId($reviewer->getOwnerId());
        
        if ($review->save() == SAVED_NEW) {            
            $review = Review::load($new_review_id);

            // Send information to Reviewer.
            $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
                'invitation_accepted_information', 
                [$invitation, $paper, $reviewer, $review], 
                $reviewer->getEmail(), 
                '',
                '',
                [$paper] // Send a copy of the Paper.
            );        
            $email->send();            
                                    
        }

        
        // Send email to staff.
        $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
            'invitation_accepted', 
            [$invitation, $paper, $reviewer], 
            $invitation->getOwner()->getEmail(), 
            '',
            '',
            [] // No attachments are necessary.
        );
        $email->send();  
        

        
        // Update Paper status.
        Paper::rebuildCachedFigures($paper);            
        
        $invitation->logAction($this->logger('multi_peer_review'), 'accepted');
        drupal_set_message(t('Thank you for accepting our invitation.'));  
         
        // Redirect the user to the home page. $status is typically SAVED_UPDATED.      
        $form_state->setRedirectUrl($this->getCancelUrl());           
    }
    else {
        drupal_set_message(t('There was a technical issue accepting the invitation. Please contact us or try again later.'), 'error'); 

        // Form will be rebuilt.
        $form_state->setRebuild();        
    }

    
  }
    
  
}

