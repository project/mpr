<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Reviewer entities.
 *
 * @ingroup multi_peer_review
 */
class ReviewerDeleteForm extends ContentEntityDeleteForm implements ReviewerFormInterface {


}
