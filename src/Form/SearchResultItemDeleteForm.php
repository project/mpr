<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Search Result Item entities.
 *
 * @ingroup multi_peer_review
 */
class SearchResultItemDeleteForm extends ContentEntityDeleteForm implements SearchResultItemFormInterface {


}
