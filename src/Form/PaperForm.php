<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPREmail;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Form controller for the Paper edit forms.
 */
class PaperForm extends CommonContentEntityForm implements PaperFormInterface {


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $paper = $this->entity;
    
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit Paper %label', ['%label' => $paper->label()]);
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $paper->label(),
      '#required' => TRUE,
    ];
    
    $form['status_display'] = [
      '#type' => 'textfield',      
      '#title' => $this->t('Status'),
      '#default_value' => $this->t($paper->getStatus()), // Translated status text
      '#description' => $this->t('Indicates the status of the Paper. Possible statuses: ') . implode(', ', Paper::getTranslatedStatuses()),
      '#disabled' => TRUE,      
    ];      
        
    if ($paper->getCachedReviewCompletionPercent() >= 100) {
        $minimum_reviews_per_paper = intval(MPRCommon::getConfigValue('minimum_reviews_per_paper'));
        $reviews_submitted = $paper->getCachedReviewsSubmitted();
        if ($reviews_submitted >= $minimum_reviews_per_paper) {        
            $form['peer_review_complete'] = MPRCommon::getDefaultCheckBoxFormField('Peer Review Complete', 
                    'Mark this Paper as being Peer Reviewed. Once marked, you will not be able to invite additional reviewers to review this Paper.', $paper->getPeerReviewComplete());
        }
        else {
            $remaining_reviews = $minimum_reviews_per_paper - $reviews_submitted;
            $form['peer_review_complete_message'] = [
                '#type' => 'markup',
                '#markup' => $this->t('This Paper needs @remaining-reviews more submitted Reviews before it can be marked as reviewed.', ['@remaining-reviews' => strval($remaining_reviews)]),
            ];
        }
    }
    
       
    $form['abstract'] = MPRCommon::getDefaultHtmlTextFormField('Abstract', 
            'The Paper’s abstract.', TRUE, $paper->getAbstract());    
    
    $form['paper_type'] = MPRCommon::getDefaultDropDownFormField('Type', 
            'The type of Paper.', TRUE, $paper->getPaperType(), MPRCommon::getTranslatedListOptionsFromConfig('paper_types'));    
    
    $form['file'] = MPRCommon::getDefaultFileFormField('File', 
            'The actual Paper file. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, TRUE, $paper->getFile(), MPRCommon::DOCUMENT_FILE_EXTENSIONS);
    
    $form['alert_recipients'] = MPRCommon::getDefaultMultiLineTextFormField('Alert Recipients',
            'Email addresses of people who should receive updates about the Paper. Each email address should be separated by a comma.', FALSE, $paper->getAlertRecipients());
    // Shorten the height of the alert recipients field
    $form['alert_recipients']['#rows'] = 2; 
    
    $form['subjects'] = MPRCommon::getDefaultMultiLineTextFormField('Subjects',
            'The subjects that the Paper relates to. Each subject must be entered on a single line.', FALSE, $paper->getSubjects());  
   
    
    // Load checklist options (if any)
    $checkbox_options = MPRCommon::getTranslatedListOptionsFromConfig('paper_checklist');    
    if (empty($checkbox_options) == FALSE) {                
        $form['checklist_control'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Checklist'),           
          '#description' => $this->t('Indicates administrative statuses that apply to the Paper.'),
          '#required' => FALSE,
          '#options' => $checkbox_options,          
          '#default_value' => $paper->getChecklist(),
        ];    
    }
    
    // Custom flag to check if we should override form values
    $force_value_override = $form_state->getValue('force_value_override');
    if (empty($force_value_override) == TRUE) {
        $force_value_override = FALSE;
    } else {
        if ($force_value_override == '1') {
            $force_value_override = TRUE;
        }
        else {
            $force_value_override = FALSE;
        }
    }

    $contributor_types = $this->getContributorTypes();
    
    
    foreach ($contributor_types as $key => $value) {
        
        $container_name = $key . '_container';
        
        $form[$container_name] = [
          '#type' => 'details',          
          '#title' => $this->t($value['title']),
          '#description' => $this->t($value['description']),
          '#open' => TRUE,
        ];    




        // Read contributor XML data
        $xml_data = $form_state->getValue($key);
        if (empty($xml_data) == TRUE) {
            $xml_data = $value['default_xml_data'];
        }

        if (($xml_data != '') && strpos($xml_data,'<contributor>') !== FALSE) {

            $contributors_dom = simplexml_load_string($xml_data);
            if ($contributors_dom != NULL) {

                $index = 0;
                foreach ($contributors_dom->children() as $contributor) {
                    $form_field_index = '_inxid' . strval($index);
                    $form_field_id = $key . '_container_item' . $form_field_index;

                    $container_title = trim(trim(strval($contributor->first_name) . ' ' . strval($contributor->last_name)) . ' ' . strval($contributor->entity_name));                    
                    if ($container_title == '') {
                        $container_title = $this->t('Contributor') . ' ' . strval($index + 1);
                    }                

                    $form[$container_name][$form_field_id] = [
                      '#type' => 'details',
                      '#title' => $container_title, // Names do not require translation        
                      '#open' => FALSE,                      
                    ];

                    
                    $element = MPRCommon::getDefaultSingleLineTextFormField('First Name', 'First name or given name of this contributor.', FALSE, strval($contributor->first_name), 100); 
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->first_name);
                    }
                    $element['#access'] = $value['personal_details_fields'];
                    $form[$container_name][$form_field_id][$key . 'first_name' . $form_field_index] = $element;

                    $element = MPRCommon::getDefaultSingleLineTextFormField('Last Name', 'Last name or family name of this contributor.', FALSE, strval($contributor->last_name), 100);
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->last_name);
                    }                
                    $element['#access'] = $value['personal_details_fields'];
                    $form[$container_name][$form_field_id][$key . 'last_name' . $form_field_index] = $element;


                    $element = MPRCommon::getDefaultSingleLineTextFormField('Name', 'Organisation/entity name of this contributor.', FALSE, strval($contributor->entity_name), 100);
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->entity_name);
                    }                
                    $element['#access'] = !$value['personal_details_fields'];
                    $form[$container_name][$form_field_id][$key . 'entity_name' . $form_field_index] = $element;                        


                    $element = MPRCommon::getDefaultCheckBoxFormField('Author', 'If this contributor is an author of this Paper, this field should be ticked.', strval($contributor->is_author));
                    if ($force_value_override == TRUE) {
                        if (strval($contributor->is_author) == '1') {
                            $element['#value'] = TRUE;
                        } 
                        else {
                            $element['#value'] = FALSE;
                        }
                    }                  
                    $form[$container_name][$form_field_id][$key . 'is_author' . $form_field_index] = $element;


                    $element = MPRCommon::getDefaultMultiLineTextFormField('Affilations', 'Affiliations of the author/contributor. Each affiliation must be entered on a single line.', FALSE, strval($contributor->affiliations));
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->affiliations);
                    }   
                    $form[$container_name][$form_field_id][$key . 'affiliations' . $form_field_index] = $element;


                    $element = MPRCommon::getDefaultSingleLineTextFormField('ORCID', 'The unique identifier of the author/contributor on the ORCID registry (https://orcid.org/).', FALSE, strval($contributor->id_orcid), 25);
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->id_orcid);
                    }                
                    $form[$container_name][$form_field_id][$key . 'id_orcid' . $form_field_index] = $element;


                    $element = MPRCommon::getDefaultSingleLineTextFormField('ISNI', 'The unique identifier of the author/contributor on the ISNI platform (http://www.isni.org/).', FALSE, strval($contributor->id_isni), 25);
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->id_isni);
                    }                
                    $form[$container_name][$form_field_id][$key . 'id_isni' . $form_field_index] = $element;


                    $element = MPRCommon::getDefaultSingleLineTextFormField('ResearcherID', 'The unique identifier of the author/contributor on the Publons platform (https://publons.com).', FALSE, strval($contributor->id_researcherid), 25);
                    if ($force_value_override == TRUE) {
                        $element['#value'] = strval($contributor->id_researcherid);
                    }                
                    $form[$container_name][$form_field_id][$key . 'id_researcherid' . $form_field_index] = $element;                


                    $form[$container_name][$form_field_id][$key . 'contributor_delete' . $form_field_index] = [                  
                        '#type' => 'button',
                        '#name' => 'contributor_delete' . $form_field_index,
                        '#value' => 'Delete this Contributor',
                        '#button_type' => 'danger',
                        '#attributes' => ['class' => ['delete-contributor-' . str_replace('_', '-', $form_field_id)]],
                        '#executes_submit_callback' => TRUE,
                        '#submit' => ['::removeContributorCallback'],                    
                    ];  

                    $index++;
                }            

            }
        }


        $form[$container_name][$key . 'contributor_add'] = [
            '#type' => 'button',
            '#name' => $container_name . '_contributor_add',
            '#value' => 'Add Contributor',
            '#attributes' => ['class' => ['add-contributor-' . str_replace('_', '-', $container_name)]], 
            '#executes_submit_callback' => TRUE,
            '#submit' => ['::addContributorCallback'],
        ];    
    }     
    
    // Reset custom flags
    $form_state->setValue('force_value_override', '0');
    
    

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Paper ID'),
      '#maxlength' => 255,
      '#default_value' => $paper->id(),
      '#disabled' => !$paper->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\Paper::load',
      ],
      '#element_validate' => [],
    ];
    
    $this->insertWebDeveloperControls($form, $form_state);
    
    return parent::form($form, $form_state);
  }

  public function buildWebDeveloperControls(&$form) {      
      $form['debug_option_reset_status'] = MPRCommon::getDefaultCheckBoxFormField('Reset Status', '', FALSE);
  }  

  /**
   * Returns an array containing contributor types.
   */  
  public function getContributorTypes() {
    $paper = $this->entity;
    $contributor_types = [
        'individual_contributors' => [
            'title' => 'Individual Contributors', 
            'description' => 'People who authored or contributed to this paper.',
            'default_xml_data' => $paper->getIndividualContributors(), 
            'personal_details_fields' => TRUE,
            ],
        'non_individual_contributors' => [
            'title' => 'Non Individual Contributors', 
            'description' => 'Organisations, teams and other non-individual entities who authored or contributed to this paper.',
            'default_xml_data' => $paper->getNonIndividualContributors(), 
            'personal_details_fields' => FALSE,
            ],
    ];
    return $contributor_types;
  }
  
  /**
   * Submit button callback that deletes a given contributor.
   */  
  public function removeContributorCallback($form, FormStateInterface $form_state) {
      
    // Verify that the caller is the delete contributor button
    $remove_index = -1;
    $data_source = '';
    $caller = $form_state->getTriggeringElement();
    
    if (($caller != NULL) && (array_key_exists('#attributes', $caller) == TRUE)) {
        if (array_key_exists('class', $caller['#attributes']) == TRUE) {
        
            foreach($caller['#attributes']['class'] as $class_item) {                
                if (strpos($class_item, 'delete-contributor-') !== FALSE) {
                    
                    // Caller class contains the target index 
                    $remove_index = intval(substr($class_item, strpos($class_item, '-inxid') + 6));
                    
                    // Determine the data source that this caller relates to
                    if (strpos(str_replace('-', '_', $class_item), 'non_individual_contributors') === FALSE) {
                        $data_source = 'individual_contributors';
                    }
                    else {
                        $data_source = 'non_individual_contributors';
                    }                    
                    
                    break;
                }
            }
            
        }
    }
    
    if ($remove_index != -1) {        
        $xml_data = $form_state->getValue($data_source);
        if (empty($xml_data) == FALSE) {
            
            // If there are no contributors, there would be no XML data and this function would not be called.
            // Therefore, it is safe to assume at least one contributor exists.
            $xml_parts = explode('<contributor>', $xml_data);
            if (count($xml_parts) <= 2) {
                // This is the last contributor in the dataset
                $xml_data = '<?xml version="1.0" encoding="UTF-8"?><contributors></contributors>';
            }
            else {
                $new_xml_data = $xml_parts[0]; // XML header

                $index = 0;            
                foreach($xml_parts as $xml_part) {
                    if ($index != 0) {
                        if ($remove_index != ($index - 1)) {

                            $new_xml_data .= '<contributor>';
                            $new_xml_data .= substr($xml_part, 0, strpos($xml_part, '</contributor>'));
                            $new_xml_data .= '</contributor>';
                        }
                    }

                    $index++;
                }
                
                $new_xml_data .= '</contributors>'; // XML footer
                
                $xml_data = $new_xml_data;                
            }
            
            $form_state->setValue($data_source, $xml_data);
            $form_state->setValue('force_value_override', '1');
        }                  
    }
    
    $form_state->setRebuild(TRUE);
      
  }
  
  /**
   * Submit button callback that adds a blank contributor.
   */  
  public function addContributorCallback($form, FormStateInterface $form_state) {
    
    $eof = '</contributors>';
    $blank_contributor_data = '<contributor>
        <first_name></first_name>
        <last_name></last_name>
        <entity_name></entity_name>
        <is_author></is_author>
        <affiliations></affiliations>
        <id_orcid></id_orcid>
        <id_isni></id_isni>
        <id_researcherid></id_researcherid>        
        </contributor>';
    
    
    $data_source = '';
    $caller = $form_state->getTriggeringElement();
    
    if (($caller != NULL) && (array_key_exists('#attributes', $caller) == TRUE)) {
        if (array_key_exists('class', $caller['#attributes']) == TRUE) {
        
            foreach($caller['#attributes']['class'] as $class_item) {                
                if (strpos($class_item, 'add-contributor-') !== FALSE) {
                    
                    // Determine the data source that this caller relates to
                    if (strpos(str_replace('-', '_', $class_item), 'non_individual_contributors') === FALSE) {
                        $data_source = 'individual_contributors';
                    }
                    else {
                        $data_source = 'non_individual_contributors';
                    }
                                        
                    break;
                }
            }
            
        }
    }    
    
    if ($data_source != '') {
      
        $xml_data = $form_state->getValue($data_source);
        if (empty($xml_data) == FALSE) {

            if (strpos($xml_data, '<contributor>') === FALSE) {
                $xml_data = '<?xml version="1.0" encoding="UTF-8"?><contributors>' . $blank_contributor_data . $eof;            
            }
            else {
                $xml_data = substr($xml_data, 0, strpos($xml_data, $eof));
                $xml_data .= $blank_contributor_data . $eof;            
            }

            $form_state->setValue($data_source, $xml_data);
        }   

    }
    
    
    $form_state->setRebuild(TRUE);
  }
  
  

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    
    MPRCommon::filterHtmlTextFormField($form, $form_state, 'abstract');
    
    
    // Store checklist selection
    if (empty($form['checklist_control']) == FALSE) {
        
        // $form['checklist_control']['#value'] example: array(1) { ["Ethics approved"]=> string(15) "Ethics approved" } 
        
        $checklist_value = [];
        foreach ($form['checklist_control']['#value'] as $key => $value) {
            array_push($checklist_value, $key);
        }
        
        $form_state->setValue('checklist', implode("\n", $checklist_value));
    }
    
    
                
    // Read contributor interface and store as XML
    $contributor_types = $this->getContributorTypes();
    foreach ($contributor_types as $key => $value) {
        $container_name = $key . '_container';
        
        // Create XML representation of contributor data
        $xw = xmlwriter_open_memory();
        xmlwriter_set_indent($xw, 1);
        xmlwriter_set_indent_string($xw, ' ');  

        xmlwriter_start_document($xw, '1.0', 'UTF-8');

        // Root element
        xmlwriter_start_element($xw, 'contributors');

        // Iterate through each contributor details form field
        $contributor_item_keys = array_keys($form[$container_name]); 
        
        //var_dump($contributor_item_keys);exit();
        $index = 0;
        foreach ($contributor_item_keys as $key_name) {
            $pos = strpos($key_name, $container_name . '_item_');
            if (($pos !== FALSE) && ($pos == 0)) {                
                $contributor_fields = $form[$container_name][$key_name];

                // Add contributor element
                xmlwriter_start_element($xw, 'contributor');

                // Add fields
                foreach (['first_name', 'last_name', 'entity_name', 'is_author', 'affiliations', 'id_orcid', 'id_isni', 'id_researcherid'] as $field_name)
                {
                    $indexed_field_name = $key . $field_name . '_inxid' . strval($index);

                    xmlwriter_start_element($xw, $field_name);
                    xmlwriter_text($xw, $contributor_fields[$indexed_field_name]['#value']);
                    xmlwriter_end_element($xw);             
                }

                // End contributor element
                xmlwriter_end_element($xw); 
                $index++;
            }         
        }

        // End root element
        xmlwriter_end_element($xw);
        xmlwriter_end_document($xw);

        $xml_output = xmlwriter_output_memory($xw);
        $form_state->setValue($key, $xml_output);             
    }
        
  }  
  

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge([
      'label',
      'id',
    ], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    $field_names = [
      'label',
      'id',
    ];
    foreach ($violations->getByFields($field_names) as $violation) {
      list($field_name) = explode('.', $violation->getPropertyPath(), 2);
      $form_state->setErrorByName($field_name, $violation->getMessage());
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
                      
    $paper = $this->entity;    
    
    // Check debug options.    
    if ($this->isCheckBoxChecked($form, 'debug_option_reset_status') == TRUE) {
        $paper->set('status', Paper::STATUS_IDLE);
    }    
    
    
    // Checks if the Paper is being marked as complete for the first time.
    if (($this->isCheckBoxChecked($form, 'peer_review_complete') == TRUE) && ($paper->getStatus() == Paper::STATUS_REVIEW_PENDING)) {
        $send_completion_email = TRUE;
        
        $paper->set('status', Paper::STATUS_REVIEWED);
    }
    else {
        $send_completion_email = FALSE;
    }    
    
    $paper->setNewRevision(TRUE);
    $status = $paper->save();
    
    
    if (($status == SAVED_UPDATED) && ($send_completion_email == TRUE)) {
        
        // Reload Paper.
        $paper = Paper::load($paper->id());
        
        
        // Prepare completion email.
        $email = MPREmail::createFromEmailTemplate(
                'paper_reviewed', 
                [$paper], 
                $paper->getOwner()->getEmail(), 
                $paper->getAlertRecipients(), 
                '', 
                []
        );
        
        $email->send();
    }
    
    
    

    $info = ['%info' => $paper->label()];
    $context = ['@type' => $paper->bundle(), '%info' => $paper->label()];
    $logger = $this->logger('multi_peer_review');

    if ($status == SAVED_UPDATED) {
      $logger->notice('@type: updated %info.', $context);
      $this->messenger->addMessage($this->t('Paper %info has been updated.', $info));
    }
    else {
      $logger->notice('@type: added %info.', $context);
      $this->messenger->addMessage($this->t('Paper %info has been created.', $info));
    }

    if ($paper->id()) {
      $form_state->setValue('id', $paper->id());
      $form_state->set('id', $paper->id());

      $collection_url = $paper->toUrl('collection');
      $redirect = $collection_url->access() ? $collection_url : Url::fromRoute('<front>');
      $form_state->setRedirectUrl($redirect);
    }
    else {
      $this->messenger->addError($this->t('The Paper could not be saved.'));
      $form_state->setRebuild();
    }
  }

}

