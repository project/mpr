<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Form controller for the Invitation declination form.
 */
class InvitationDeclineForm extends CommonConfirmForm implements InvitationFormInterface {

  /**
   * Constructs a new InvitationDeclineForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $this->entity);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $this->entity->fabricateAndLoadPaper());
    
    return ($this->t('Decline invitation to review %paper-title?', $placeholder_replacements));           
  }
  
  
  /**
   * {@inheritdoc}
   */ 
  public function getFrontEndUserCancelUrl($user_id) {
    return Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]); 
  }
  
 
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
      
    $invitation = $this->entity;
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();

    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                
    
    
    
    $this->insertPaperDetails($form, $paper, FALSE, FALSE);
    
    
    
    
    $form['decline_reason'] = MPRCommon::getDefaultDropDownFormField('Reason for declining', 
            '', FALSE, 
            NULL, MPRCommon::getTranslatedListOptionsFromConfig('decline_reasons'), $placeholder_replacements);          

    $form['decline_message'] = MPRCommon::getDefaultMultiLineTextFormField('Message', 
            '', FALSE, '', $placeholder_replacements);        

    $form['decline_suggestion'] = MPRCommon::getDefaultMultiLineTextFormField('Suggest another reviewer', 
            'If you can recommend another person to review this paper, please provide their contact details, thank you.', FALSE, '', $placeholder_replacements);        
    


    return parent::form($form, $form_state);
  }
  
  
  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {


    // 'Confirm' button clicked.
    // Mark the invitation as declined and inform the invite sender (Owner of the record).
    $status = NULL; // Set default status.
    
                
    // Load Invitation, Paper and Reviewer entities.  
    $invitation = $this->entity;    
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();
    
    
    // Update Invitation status.        
    $invitation->set('status', Invitation::STATUS_DECLINED);
    $invitation->set('declined_timestamp', time());    
    
    // Collect the reviewer's reasons for declining.    
    $invitation->set('decline_reason', $form['decline_reason']['#value']);
    $invitation->set('decline_message', $form['decline_message']['#value']);
    $invitation->set('decline_suggestion', $form['decline_suggestion']['#value']);
    
    
    
    $invitation->setNewRevision(TRUE);
    $status = $invitation->save();

    if ($status == SAVED_UPDATED) {
        
        // Update Reviewer statistics.
        Reviewer::rebuildCachedFigures($reviewer);
        
        // Update Paper status.
        Paper::rebuildCachedFigures($paper);         

        // Reload Invitation.
        $invitation = Invitation::load($invitation->id());
        

        $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
            'invitation_declined', 
            [$invitation, $paper, $reviewer], 
            $invitation->getOwner()->getEmail(), 
            '',
            '',
            [] // No attachments are necessary.
        );


        $email->send();  
        
        $invitation->logAction($this->logger('multi_peer_review'), 'declined');
        drupal_set_message(t('Thank you for responding to our invitation.'));  
         
        // Redirect the user to the home page. $status is typically SAVED_UPDATED.      
        $form_state->setRedirectUrl($this->getCancelUrl());           
    }
    else {
        drupal_set_message(t('There was a technical problem declining the invitation. Please contact us or try again later.'), 'error'); 

        // Form will be rebuilt.
        $form_state->setRebuild();        
    }

    
  }
    
  
}

