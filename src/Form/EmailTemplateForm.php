<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Form controller for the EmailTemplate edit forms.
 */
class EmailTemplateForm extends EntityForm implements EmailTemplateFormInterface {


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $ent = $this->entity;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit Email Template %label', ['%label' => $ent->label()]);
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $ent->label(),
      '#required' => TRUE,
    ];      
    
    $form['staff_notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Staff Notes'),      
      '#default_value' => $ent->getStaffNotes(),
      '#rows' => '2',
      '#required' => FALSE,    
    ];    
    
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#default_value' => $ent->getSubject(),
      '#required' => TRUE,
    ];    
       
    
//    $form['body'] = [
//      '#type' => 'text_format',
//      '#title' => $this->t('Body'),      
//      '#default_value' => $ent->getBody(),
//      '#description' => $this->t('Email body content.'),
//      '#rows' => '20',
//      '#format'=> 'full_html',        
//      '#required' => TRUE,    
//    ];
    
    $form['body'] = MPRCommon::getDefaultHtmlTextFormField('Body', 'Email body content.', TRUE, $ent->getBody());
    $form['body']['#rows'] = '20';
    

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Email Template ID'),
      '#maxlength' => 255,
      '#default_value' => $ent->id(),
      '#disabled' => !$ent->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\EmailTemplate::load',
      ],
      '#element_validate' => [],
    ];
    
    
    return parent::form($form, $form_state);
  }
  


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $ent = $this->entity;    
    $ent->set('staff_notes', $form_state->getValue('staff_notes'));
    $ent->set('subject', $form_state->getValue('subject'));
    $ent->set('body', $form_state->getValue('body'));
    $status = $ent->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Email Template.', [
          '%label' => $ent->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Email Template.', [
          '%label' => $ent->label(),
        ]));
    }
    $form_state->setRedirectUrl($ent->toUrl('collection'));
  }

}

