<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRFormHelper;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class CommonFormBase.
 *
 * @package Drupal\multi_peer_review\Form
 */
abstract class CommonFormBase extends FormBase {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  
  /**
   * The form helper.
   *
   * @var \Drupal\multi_peer_review\MPRFormHelper
   */
  private $helper;

  
  public function __construct() {
    
    // Setup helper.  
    $this->getHelper();
    
    $this->messenger = \Drupal::messenger();
    
  }  

  
  /**
   * Returns the Multi Peer Review form helper instance for this object.
   * The form helper will be initialised the first time this method is called.
   * 
   * @return \Drupal\multi_peer_review\MPRFormHelper
   *   The instance of this object's helper.
   */     
  public function getHelper() {
    if (empty($this->helper) == TRUE) {
        $this->helper = new MPRFormHelper();
        $this->helper->initialise($this);
    }
    
    return $this->helper;
  }     
  
  
  public function getUserId() {
    return \Drupal::request()->get('user');
  }  
  
    
}
