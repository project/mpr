<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Web Request Cache Item entities.
 *
 * @ingroup multi_peer_review
 */
class WebRequestCacheItemDeleteForm extends ContentEntityDeleteForm implements WebRequestCacheItemFormInterface {


}
