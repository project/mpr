<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Review;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Invitation;

/**
 * Class UserProfileForm.
 *
 * @package Drupal\multi_peer_review\Form
 */
class UserProfileForm extends CommonFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_profile_form';
  }
  

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
                
    $user_id = $this->getUserId();    
    $reviewer = Reviewer::getReviewerByOwner($user_id);
    if ((empty($reviewer) == FALSE) && ($reviewer->getOwnerId() == $user_id)){

        $form['title'] = MPRCommon::getDefaultSingleLineTextFormField('Title', 
                'Your title, such as Professor, Mrs, Sir, etc.', FALSE, $reviewer->getTitle());  

        $form['first_name'] = MPRCommon::getDefaultSingleLineTextFormField('First Name', 
                'First name or given name.', TRUE, $reviewer->getFirstName());    

        $form['last_name'] = MPRCommon::getDefaultSingleLineTextFormField('Last Name', 
                'Last name or family name.', TRUE, $reviewer->getLastName());                  

        $form['email_display'] = MPRCommon::getDefaultSingleEmailAddressFormField('Email', 
                'Email address for correspondence and accessing your account. If you need to change your email address, change it via the Edit tab.', TRUE, $reviewer->getEmail());       
        $form['email_display']['#value'] = $reviewer->getEmail();
        $form['email_display']['#disabled'] = TRUE;

        $form['phone'] = MPRCommon::getDefaultSingleLineTextFormField('Phone', 
                'Contact number.', FALSE, $reviewer->getPhone());    

        if (empty(MPRCommon::getConfigValue('panels')) == FALSE) {
            $panel_display = $reviewer->getPanel();
            if ((empty($panel_display) == TRUE) || ($panel_display == '_none')) {
                $panel_display = 'All Panels';
            }

            $form['panel_display'] = MPRCommon::getDefaultSingleLineTextFormField('Panel', 
                    'The Panel assigned to you by staff.', FALSE, $panel_display);    
            $form['panel_display']['#value'] = $this->t($panel_display);
            $form['panel_display']['#disabled'] = TRUE;
        }

        $form['biography'] = MPRCommon::getDefaultMultiLineTextFormField('Biography', 
                'A brief autobiography. The information you provide may help staff when they are seeking reviewers.', FALSE, $reviewer->getBiography());       

        $form['subjects'] = MPRCommon::getDefaultMultiLineTextFormField('Subjects', 
                'The subjects that you specialise in. Each subject must be entered on a single line.', FALSE, $reviewer->getSubjects());      

        $form['affiliations'] = MPRCommon::getDefaultMultiLineTextFormField('Affiliations', 
                'Your affiliations. Each affiliation must be entered on a single line.', FALSE, $reviewer->getAffiliations());          

        $form['id_orcid'] = MPRCommon::getDefaultSingleLineTextFormField('ORCID', 
                'Your unique identifier on the ORCID registry (https://orcid.org/).', FALSE, $reviewer->getORCID()); 

        $form['id_isni'] = MPRCommon::getDefaultSingleLineTextFormField('ISNI', 
                'Your unique identifier on the ISNI platform (http://www.isni.org/).', FALSE, $reviewer->getISNI()); 

        $form['id_researcherid'] = MPRCommon::getDefaultSingleLineTextFormField('ResearcherID', 
                'Your unique identifier on the Publons platform (https://publons.com).', FALSE, $reviewer->getResearcherID());     



        $form['actions'] = [
            '#type' => 'actions',
            '#attributes' => [
                'class' => ['form-actions', 'js-form-wrapper', 'form-wrapper'],
            ],
        ];
        $form['actions']['update_button'] = [
            '#type' => 'submit',           
            '#value' => $this->t('Update'),        
            '#button_type' => 'primary',
        ];
        $form['actions']['cancel_link'] = [
            '#type' => 'link',
            '#url' => Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]),
            '#title' => 'Cancel',
        ];        

    }
    else {
        $form['message'] = [
            '#type' => 'markup',
            '#markup' => $this->t('No reviewer profile found.'),
        ];
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {  

    $user_id = $this->getUserId();    
    $reviewer = Reviewer::getReviewerByOwner($user_id);
    if ($reviewer->getOwnerId() == $user_id){

        
        $reviewer->set('title', $form['title']['#value']);
        $reviewer->set('first_name', $form['first_name']['#value']);
        $reviewer->set('last_name', $form['last_name']['#value']);
        //$reviewer->set('email', $form['email']['#value']);
        $reviewer->set('phone', $form['phone']['#value']);
        $reviewer->set('biography', $form['biography']['#value']);
        $reviewer->set('subjects', $form['subjects']['#value']);
        $reviewer->set('affiliations', $form['affiliations']['#value']);
        $reviewer->set('id_orcid', $form['id_orcid']['#value']);
        $reviewer->set('id_isni', $form['id_isni']['#value']);
        $reviewer->set('id_researcherid', $form['id_researcherid']['#value']);
        

        
        $reviewer->setNewRevision(TRUE);
        $status = $reviewer->save();      

        $descriptive_label = $reviewer->getTranslatedDescriptiveLabel()->render();
        
        $info = ['%info' => $descriptive_label];
        $context = ['@type' => $reviewer->bundle(), '%info' => $descriptive_label];
        $logger = $this->logger('multi_peer_review');        
        
        if ($status == SAVED_UPDATED) {
            $logger->notice('@type: updated %info.', $context);
            $this->messenger->addMessage($this->t('Your profile has been updated.', $info));            
            
            $form_state->setRedirectUrl(Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]));
        }
        else {
            $this->messenger->addError($this->t('There was an error updating your reviewer profile.'));
            $form_state->setRebuild();
        }
        
    }
        
  }

}