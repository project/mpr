<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPREmail;
use Drupal\multi_peer_review\Entity\EmailTemplate;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Form controller for the Review submit form.
 */
class ReviewSubmitForm extends CommonConfirmForm implements ReviewFormInterface {


  /**
   * Constructs a new ReviewSubmitForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $placeholders = [
        '%paper' => $this->entity->getPaperTitle(),
    ];
    
    return ($this->t('Submit Review of %paper', $placeholders));           
  }
    
  
  /**
   * {@inheritdoc}
   */ 
  public function getFrontEndUserCancelUrl($user_id) {
    return Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]);
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Submit Review');
  }  
  
    
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
      
    $review = $this->entity;
    $invitation = $review->fabricateAndLoadInvitation();
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();

    $placeholder_replacements = [];
    MPRCommon::addWebsitePlaceholderReplacements($placeholder_replacements);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                
    
    
    $this->insertPaperDetails($form, $paper);       
    
    
    $form['email_attachments_control'] = MPRCommon::getDefaultMultiFileFormField('Attachments',
            'Please attach your review files. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, TRUE, NULL, MPRCommon::DOCUMENT_FILE_EXTENSIONS);
        
    
    $form['submit_message'] = MPRCommon::getDefaultMultiLineTextFormField('Message', 
            'Optional message to %site-name and paper uploader/contributors.', FALSE, '', $placeholder_replacements);     

    $form['email_recipient_options_container'] = [
      '#type' => 'details',          
      '#title' => $this->t('Additional Options'),
      '#open' => FALSE,
    ];       
    
    $form['email_recipient_options_container']['email_cc'] = MPRCommon::getDefaultMultiLineTextFormField('CC', 
            'Email addresses of other recipients besides %site-name and paper uploader/contributors who should receive a copy of your review and message. Separate each email address using commas.', FALSE, NULL, $placeholder_replacements);
    $form['email_recipient_options_container']['email_cc']['#rows'] = 2;
       
    $form['email_recipient_options_container']['email_bcc'] = MPRCommon::getDefaultMultiLineTextFormField('BCC', 
            'Email addresses of other recipients besides %site-name and paper uploader/contributors who should receive a copy of your review and message without the knowledge of other recipients. Separate each email address using commas.', FALSE, NULL, $placeholder_replacements);     
    $form['email_recipient_options_container']['email_bcc']['#rows'] = 2;



    return parent::form($form, $form_state);
  }
    
  

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);
    
    
    // Process email attachment control input
    $entity->validateFormProcess($form, $form_state);
    
    
    return $entity;
  }  
  
  
  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {
                                                                            
    // 'Confirm' button clicked.
    $status = NULL; // Set default status.  
    
    $email_template_key = 'review_submitted';
    
    // Load Review, Invitation, Paper and Reviewer entities.  
    $review = $this->entity;    
    $invitation = $review->fabricateAndLoadInvitation();
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();
    $email_template = EmailTemplate::load($email_template_key);
    
    
    // Set fields from form.
    $review->set('submit_message', $form['submit_message']['#value']);
    $review->set('email_cc', $form['email_recipient_options_container']['email_cc']['#value']);
    $review->set('email_bcc', $form['email_recipient_options_container']['email_bcc']['#value']);
    $review->set('email_attachments', $form_state->getValue('email_attachments'));
    
    // Set fields from Email Template.
    $review->set('email_subject', $email_template->getSubject());
    $review->set('email_body', $email_template->getBody());
    
    
    // Update Review status.
    $review->set('status', Review::STATUS_SUBMITTED);
    $review->set('submitted_timestamp', time());
        
    
    $review->setNewRevision();
    $status = $review->save();
    
    
    if ($status == SAVED_UPDATED) {
        if ($this->isAdmin() == TRUE) {
            drupal_set_message(t('The Review has been submitted.')); 
        }
        else {
            drupal_set_message(t('Thank you for submitting your review.')); 
        }
        
        $review->logAction($this->logger('multi_peer_review'), 'submitted');
        
        // Update statistics.
        Reviewer::rebuildCachedFigures($reviewer);   
        Paper::rebuildCachedFigures($paper);
        
        
        // Reload Review.
        $review = Review::load($review->id());
        
        // Send an email to the user who invited the Reviewer.
        // The Paper owner and alert recipients will also be included.
        $recipients = [
            $invitation->getOwner()->getEmail(),
            $paper->getOwner()->getEmail(),
            $paper->getAlertRecipients(),
        ];
        $recipients = MPRCommon::getDistinctValues($recipients);
        $recipients = implode(', ', $recipients);
        
        $email = MPREmail::createFromEmailCapableEntity(
            $email_template_key, 
            [$review, $invitation, $paper, $reviewer], 
            $recipients, 
            $review, 
            [$review]
        );            
        
        $email->send();     
        
        
        // Send an email to Reviewer.
        $email = MPREmail::createFromEmailTemplate(
            'review_received', 
            [$review, $invitation, $paper, $reviewer], 
            $reviewer->getEmail(), 
            '', 
            '', 
            []
        );
        
        $email->send();   
        
    }
    else {
        if ($this->isAdmin() == TRUE) {
            drupal_set_message(t('There was a problem submitting the Review. It has not been submitted.'), 'error'); 
        }
        else {
            drupal_set_message(t('There was a technical issue submitting your review. Please contact us or try again later.'), 'error'); 
        }
    }
                
    
    if ($status == NULL) {
        // Form will be rebuilt.
        $form_state->setRebuild();
    }
    else {         
        // Redirect the user to the home page. $status is typically SAVED_UPDATED.      
        $form_state->setRedirectUrl($this->getCancelUrl());          
    }    
    
  }
    
  
}
