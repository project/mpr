<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormInterface;

/**
 * Defines interface for Search Result Item forms so they can be easily distinguished.
 *
 * @internal
 */
interface SearchResultItemFormInterface extends FormInterface {}
