<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Review entities.
 *
 * @ingroup multi_peer_review
 */
class ReviewDeleteForm extends ContentEntityDeleteForm implements ReviewFormInterface {


}
