<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;
use Drupal\multi_peer_review\Entity\CommonContentEntityInterface;

/**
 * Form controller for the Invitation edit forms.
 */
class InvitationForm extends CommonContentEntityForm implements InvitationFormInterface {

   
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
      
    $invitation = $this->entity;       
    $flexible_defaults['reviewer'] = $invitation->getReviewer();
    $flexible_defaults['paper'] = $invitation->getPaper();
    
    if ($invitation->isNew() == TRUE) {
        $default_id = MPRCommon::getNewCustomTextId();
        
        $copy_item = \Drupal::request()->query->get('reviewer');
        if (empty($copy_item) == FALSE) {            
            $flexible_defaults['reviewer'] = $copy_item;            
        }       
        
        $copy_item = \Drupal::request()->query->get('paper');
        if (empty($copy_item) == FALSE) {            
            $flexible_defaults['paper'] = $copy_item;            
        }          
    }
    else {
        $default_id = $invitation->label();
    }

    if ($this->operation == 'edit') {
      $form['#title'] = $invitation->getTranslatedMessage(CommonContentEntityInterface::MESSAGE_ADMIN_EDIT_TITLE);
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique ID'),
      '#maxlength' => 255,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#disabled' => !$invitation->isNew(),     
    ];
    
    $form['status_display'] = [
      '#type' => 'textfield',      
      '#title' => $this->t('Status'),
      '#default_value' => $this->t($invitation->getStatus()), // Translated status text
      '#description' => $this->t('Indicates the status of the Invitation. Possible statuses: ') . implode(', ', Invitation::getTranslatedStatuses()),
      '#disabled' => TRUE,      
    ];    
    
    if ($invitation->isNew() == TRUE) {
        $email_template = \Drupal\multi_peer_review\Entity\EmailTemplate::load('invitation_new');
        $default_values = [
            'email_subject' => $email_template->getSubject(),
            'email_body' => $email_template->getBody(),            
        ];
    }
    else {
        $default_values = [
            'email_subject' => $invitation->getEmailSubject(),
            'email_body' => $invitation->getEmailBody(),     
        ];
    }


    // Invitations only allow the selection of Papers and Reviewers while in Draft status.
    if ($invitation->getStatus() == Invitation::STATUS_DRAFT) {
        
        // Load eligible Papers.
        $papers = Paper::getPapers(FALSE, NULL, NULL, FALSE);                         
                
        $options = MPRCommon::getTranslatedListOptionsFromEntities($papers);    
        $form['paper'] = MPRCommon::getDefaultDropDownFormField('Paper', 
                'The Paper that the Invitation relates to.', TRUE, $flexible_defaults['paper'], $options);

        // Load Reviewers.
        $options = MPRCommon::getTranslatedListOptionsFromEntities(Reviewer::getReviewers(FALSE));
        $form['reviewer'] = MPRCommon::getDefaultDropDownFormField('Reviewer', 
                'The Reviewer that is being invited to review the Paper.', TRUE, $flexible_defaults['reviewer'], $options);    

    }
    else {
        $form['paper_display'] = [
          '#type' => 'textfield',      
          '#title' => $this->t('Paper'),
          '#default_value' => $invitation->getPaperTitle(),
          '#description' => $this->t('The Paper that the Invitation relates to.'),
          '#disabled' => TRUE,      
        ];         
        
        $form['reviewer_display'] = [
          '#type' => 'textfield',      
          '#title' => $this->t('Reviewer'),
          '#default_value' => $invitation->getReviewerName(),
          '#description' => $this->t('The Reviewer that was invited to review the Paper.'),
          '#disabled' => TRUE,      
        ];            
    }
    
    
    
    $form['target_start_date'] = MPRCommon::getDefaultDateFormField('Target Start Date', 
            'The Staff’s suggested\target start date.', TRUE, $invitation->getTargetStartDate());
    
    $form['deadline'] = MPRCommon::getDefaultDateFormField('Deadline', 
            'The Review deadline.', TRUE, $invitation->getDeadline());    
    
    
    $status = $invitation->getStatus();
    if (($status == Invitation::STATUS_ACCEPTED) || ($status == Invitation::STATUS_DECLINED)) {
        $form['reviewer_response'] = [
          '#type' => 'details',          
          '#title' => $this->t('Reviewer’s Response'),
          '#open' => TRUE,
        ];     

        if ($status == Invitation::STATUS_ACCEPTED) {
            $form['reviewer_response']['preferred_start_date'] = MPRCommon::getDefaultDateFormField('Preferred Start Date', 
                    'The Reviewer’s preferred start date.', FALSE, $invitation->getPreferredStartDate());    
        }
        else {
            $form['reviewer_response']['decline_reason'] = MPRCommon::getDefaultDropDownFormField('Reason', 
                    'Reason that the Invitation was declined by the Reviewer. The drop-down options are configurable in Multi Peer Review Settings.', FALSE, 
                    $invitation->getDeclineReason(), MPRCommon::getTranslatedListOptionsFromConfig('decline_reasons'));          

            $form['reviewer_response']['decline_message'] = MPRCommon::getDefaultMultiLineTextFormField('Message', 
                    'Message provided by the Reviewer at the time that they declined the Invitation.', FALSE, $invitation->getDeclineMessage());        

            $form['reviewer_response']['decline_suggestion'] = MPRCommon::getDefaultMultiLineTextFormField('Suggested Reviewer', 
                    'An alternative Reviewer who was suggested by the declining Reviewer.', FALSE, $invitation->getDeclineSuggestion());    
        }
    }
    
    $form['email_attachments_control'] = MPRCommon::getDefaultMultiFileFormField('Attachments',
            'One or more files to include in the Invitation email. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, FALSE, $invitation->getEmailAttachments(), MPRCommon::DOCUMENT_FILE_EXTENSIONS);        
    
    $form['email_subject'] = MPRCommon::getDefaultSingleLineTextFormField('Email Subject', 
            'Subject line of the Invitation email.', TRUE, $default_values['email_subject']);         
    
    $form['email_body'] = MPRCommon::getDefaultHtmlTextFormField('Email Body', 
            'Body of the Invitation email.', TRUE, $default_values['email_body']);    
       
    $form['email_recipient_options_container'] = [
      '#type' => 'details',          
      '#title' => $this->t('Additional Email Recipients'),
      '#open' => (!empty($invitation->getEmailCC()) || !empty($invitation->getEmailBCC())), // These options will be displayed if they contain data.
    ];       
    
    $form['email_recipient_options_container']['email_cc'] = MPRCommon::getDefaultMultiLineTextFormField('Email CC', 
            'Email addresses of other recipients who should receive a copy of the Invitation email. Separate each email address using commas.', FALSE, $invitation->getEmailCC());            
    $form['email_recipient_options_container']['email_cc']['#rows'] = 2;
       
    $form['email_recipient_options_container']['email_bcc'] = MPRCommon::getDefaultMultiLineTextFormField('Email BCC', 
            'Email addresses of other recipients who should receive a copy of the Invitation email without the knowledge of other recipients. Separate each email address using commas.', FALSE, $invitation->getEmailBCC());     
    $form['email_recipient_options_container']['email_bcc']['#rows'] = 2;
    
    
    // Display controls based on current status
    switch ($invitation->getStatus()) {
        case Invitation::STATUS_DRAFT:
            $form['send_invite_now'] = MPRCommon::getDefaultCheckBoxFormField('Send Invite Now', 
                    'Tick to send this Invite to the selected Reviewer upon clicking on Save.', FALSE);                 
            break;
    }
 


    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Invitation ID'),
      '#maxlength' => 255,
      '#default_value' => $invitation->id(),
      '#disabled' => !$invitation->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\Invitation::load',
      ],
      '#element_validate' => [],
    ];
    
    
    $this->insertWebDeveloperControls($form, $form_state);
    
    
    return parent::form($form, $form_state);
  }
  
  public function buildWebDeveloperControls(&$form) {
      $form['debug_option_hasten_next_follow_up'] = MPRCommon::getDefaultCheckBoxFormField('Hasten next Follow-up', '', FALSE);
      $form['debug_option_increment_follow_up_count'] = MPRCommon::getDefaultCheckBoxFormField('Increment Follow-up Count', '', FALSE);
      $form['debug_option_decrement_follow_up_count'] = MPRCommon::getDefaultCheckBoxFormField('Decrement Follow-up Count', '', FALSE);
  }
  

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);
    
    
    MPRCommon::filterHtmlTextFormField($form, $form_state, 'email_body');
        
    // Process email attachment control input
    $entity->validateFormProcess($form, $form_state);
    
    
    return $entity;
  }
  
  
  

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge([
      'label',
      'id',
    ], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    $field_names = [
      'label',
      'id',
    ];
    foreach ($violations->getByFields($field_names) as $violation) {
      list($field_name) = explode('.', $violation->getPropertyPath(), 2);
      $form_state->setErrorByName($field_name, $violation->getMessage());
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    
    $invitation = $this->entity;     
    
    
    // Check whether or not we need to redirect the user to the send invitation form.
    $send_invite_now = $this->isCheckBoxChecked($form, 'send_invite_now');
    
    
    // Check debug options.    
    if ($this->isCheckBoxChecked($form, 'debug_option_hasten_next_follow_up') == TRUE) {
        // Set follow-up to X minute(s) from now.
        $frequency_days = intval(MPRCommon::getConfigValue('invitation_follow_up_frequency_days'));

        // Convert days to seconds.
        $frequency_seconds = MPRCommon::getSecondsFromDays($frequency_days);  
        
        $new_follow_up_time = (time() - $frequency_seconds) + 60;
        $invitation->set('cached_last_follow_up_timestamp', $new_follow_up_time);        
    }
    
    if ($this->isCheckBoxChecked($form, 'debug_option_increment_follow_up_count') == TRUE) {
        $follow_ups = $invitation->getCachedFollowUps();
        $follow_ups++;
        $invitation->set('cached_follow_ups', $follow_ups);
    }
    
    if ($this->isCheckBoxChecked($form, 'debug_option_decrement_follow_up_count') == TRUE) {
        $follow_ups = $invitation->getCachedFollowUps();
        $follow_ups--;
        $invitation->set('cached_follow_ups', $follow_ups);
    }    
    
       
    $invitation->setNewRevision(TRUE);
    $status = $invitation->save();
    
    
    $invitation->processAdminFormSaveMessages($this->logger('multi_peer_review'), $this->messenger, $status);
    

    if ($invitation->id()) {
      $form_state->setValue('id', $invitation->id());
      $form_state->set('id', $invitation->id());

      if ($send_invite_now == FALSE) {
        $collection_url = $invitation->toUrl('collection');
        $redirect = $collection_url->access() ? $collection_url : Url::fromRoute('<front>');        
      }
      else {
        $redirect_url = $invitation->toUrl('send-form');                
        $redirect = $redirect_url->access() ? $redirect_url : Url::fromRoute('<front>');                
      }
      
      // Clears the currently configured destination.
      \Drupal::request()->query->remove('destination');
      // Sets the new destination.
      $form_state->setRedirectUrl($redirect);      
    }
    else {
      $this->messenger->addError($this->t('The Invitation could not be saved.'));
      $form_state->setRebuild();
    }
    
    return $status;
  }

  
  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    $invitation = $this->entity;
    
    // Display actions based on current status
//    switch ($invitation->getStatus()) {
//        case 'Draft':
//            $element['send_now'] = [
//              '#type' => 'submit',
//              '#value' => t('Send Invitation to Reviewer'),
//              '#weight' => 20,
//              //'#submit' => ['::submitForm', '::preview'],
//            ];           
//            break;
//    }


    return $element;
  }  
  
  
}

