<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Invitation entities.
 *
 * @ingroup multi_peer_review
 */
class InvitationDeleteForm extends ContentEntityDeleteForm implements InvitationFormInterface {


}
