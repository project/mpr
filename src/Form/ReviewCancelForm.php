<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;
use Drupal\multi_peer_review\Entity\EmailTemplate;


/**
 * Form controller for the Review cancellation form.
 */
class ReviewCancelForm extends CommonConfirmForm implements ReviewFormInterface {


  /**
   * Constructs a new ReviewCancelForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {      
    $review = $this->entity;
    $invitation = $review->fabricateAndLoadInvitation();
    $paper = $invitation->fabricateAndLoadPaper();
      
    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    
    return ($this->t('Cancel the review of %paper-title?', $placeholder_replacements));           
  }
  
    
  /**
   * {@inheritdoc}
   */ 
  public function getFrontEndUserCancelUrl($user_id) {
    return Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]);
  }    
  
  
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    
    $review = $this->entity;
    $invitation = $review->fabricateAndLoadInvitation();
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();

    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);   
    
    
    $this->insertPaperDetails($form, $paper);       
            
    
    if ($this->isAdmin() == TRUE) {

        $this->insertReviewerDetails($form, $reviewer);

        
        

        $email_template = EmailTemplate::load('review_cancelled');

        $default_values = [];
        $default_values['email_subject'] = $email_template->getSubject();
        $default_values['email_body'] = $email_template->getBody();


        $form['email_attachments_control'] = MPRCommon::getDefaultMultiFileFormField('Attachments',
                'One or more files to include in the cancellation email. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, FALSE, NULL, MPRCommon::DOCUMENT_FILE_EXTENSIONS);        

        $form['email_subject'] = MPRCommon::getDefaultSingleLineTextFormField('Email Subject', 
                'Subject line of the cancellation email.', TRUE, $default_values['email_subject']);         

        $form['email_body'] = MPRCommon::getDefaultHtmlTextFormField('Email Body', 
                'Body of the cancellation email.', TRUE, $default_values['email_body']);    

        $form['email_recipient_options_container'] = [
          '#type' => 'details',          
          '#title' => $this->t('Additional Email Recipients'),
          '#open' => FALSE,
        ];       

        $form['email_recipient_options_container']['email_cc'] = MPRCommon::getDefaultMultiLineTextFormField('Email CC', 
                'Email addresses of other recipients who should receive a copy of the cancellation email. Separate each email address using commas.', FALSE, '');            
        $form['email_recipient_options_container']['email_cc']['#rows'] = 2;

        $form['email_recipient_options_container']['email_bcc'] = MPRCommon::getDefaultMultiLineTextFormField('Email BCC', 
                'Email addresses of other recipients who should receive a copy of the cancellation email without the knowledge of other recipients. Separate each email address using commas.', FALSE, '');     
        $form['email_recipient_options_container']['email_bcc']['#rows'] = 2;    

                
    }
    else {
    

        $form['cancel_reason'] = MPRCommon::getDefaultMultiLineTextFormField('Reason for cancelling', 
                '', FALSE, '', $placeholder_replacements);        

        $form['cancel_suggestion'] = MPRCommon::getDefaultMultiLineTextFormField('Suggest another reviewer', 
                'If you can recommend another person to review this paper, please provide their contact details, thank you.', FALSE, '', $placeholder_replacements);        

    }

    return parent::form($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    
    if ($this->isAdmin() == TRUE) {
        // The Email Body field is only present in the admin version of this form.
        MPRCommon::filterHtmlTextFormField($form, $form_state, 'email_body');    
    }
  }
  
  /**
   * {@inheritdoc}
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // 'Confirm' button clicked.
    $status = NULL; // Set default status.
    
                
    // Load Review, Invitation, Paper and Reviewer entities.  
    $review = $this->entity;
    $invitation = $review->fabricateAndLoadInvitation();
    $paper = $invitation->fabricateAndLoadPaper();
    $reviewer = $invitation->fabricateAndLoadReviewer();

    $placeholder_replacements = [];
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                
    MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);   
    
    
    // Update Review status.        
    $review->set('status', 'Cancelled');
    $review->set('cancelled_timestamp', time());    
    
    if ($this->isAdmin() == FALSE) {
        // Collect the Reviewer's reasons for cancelling.        
        $review->set('cancel_reason', $form['cancel_reason']['#value']);    
        $review->set('cancel_suggestion', $form['cancel_suggestion']['#value']);
    }
            
    $review->setNewRevision(TRUE);
    $status = $review->save();

    if ($status == SAVED_UPDATED) {
        
        // Also cancel the Invitation.
        $invitation->set('status', Invitation::STATUS_RETRACTED);
        $invitation->set('retracted_timestamp', time());
        $invitation->setNewRevision(TRUE);
        $invitation->save();        
        
        
        // Update statistics.        
        Reviewer::rebuildCachedFigures($reviewer);
        Invitation::rebuildCachedFigures($invitation);
        Paper::rebuildCachedFigures($paper);         
        Review::rebuildCachedFigures($review);

        // Reload Review.
        $review = Review::load($review->id());
        
        
        
        if ($this->isAdmin() == FALSE) {
            // Send an email to staff regarding the cancellation.
            $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
                'review_cancelled_notify', 
                [$invitation, $paper, $reviewer, $review],
                $invitation->getOwner()->getEmail(), 
                '',
                '',
                [] // No attachments are necessary.
            );
            $email->send();              
        }        
        else {
            // Send an email to the Reviewer about the cancellation.
            $email = \Drupal\multi_peer_review\MPREmail::createFromEmailTemplate(
                'review_cancelled',
                [$invitation, $paper, $reviewer, $review],
                $reviewer->getEmail(),
                $form['email_recipient_options_container']['email_cc']['#value'],
                $form['email_recipient_options_container']['email_bcc']['#value'],
                [] // No attachment from entities.
            );


            $email->subject = $form['email_subject']['#value'];        
            $email->body = $form['email_body']['value']['#value'];            
            
            $email->addAttachmentsFromForm($form['email_attachments_control']);
            
            $email->send();
        }
        
        $review->logAction($this->logger('multi_peer_review'), 'cancelled');

        if ($this->isAdmin() == FALSE) {
            drupal_set_message(t('Your review of %paper-title has been cancelled.', $placeholder_replacements));  
        }
        else {
            drupal_set_message(t('The review of %paper-title has been cancelled.', $placeholder_replacements));  
        }
         
        // Redirect the user.    
        $form_state->setRedirectUrl($this->getCancelUrl());           
    }
    else {
        drupal_set_message(t('There was a technical problem cancelling the review. Please contact us or try again later.'), 'error'); 

        // Form will be rebuilt.
        $form_state->setRebuild();        
    }    
    

    
  }
    
  
}

