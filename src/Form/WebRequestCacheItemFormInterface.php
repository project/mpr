<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Form\FormInterface;

/**
 * Defines interface for Web Request Cache Item forms so they can be easily distinguished.
 *
 * @internal
 */
interface WebRequestCacheItemFormInterface extends FormInterface {}
