<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multi_peer_review\MPRCommon;

/**
 * Form controller for the SES edit forms.
 */
class SESForm extends EntityForm implements SESFormInterface {


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $ent = $this->entity;

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit Search Engine Script %label', ['%label' => $ent->label()]);
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $ent->label(),
      '#required' => TRUE,
    ];
    
    $form['online'] = MPRCommon::getDefaultCheckBoxFormField('Online', 
            'When Online, the Search Engine Script will be used at regular intervals to scan for new Reviewers.', $ent->getOnline());
       
    
    $form['engine_script'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Script'),      
      '#default_value' => $ent->getEngineScript(),
      '#description' => $this->t('PHP 7+ code extending \Drupal\multi_peer_review\MPRSearchEngineScript.'),
      '#rows' => '20',
      '#required' => TRUE,    
    ];
    

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Search Engine Script ID'),
      '#maxlength' => 255,
      '#default_value' => $ent->id(),
      '#disabled' => !$ent->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\SES::load',
      ],
      '#element_validate' => [],
    ];
    
    
    return parent::form($form, $form_state);
  }
  


  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $ent = $this->entity;
    $ent->set('engine_script', $form_state->getValue('engine_script'));
    $status = $ent->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Search Engine Script.', [
          '%label' => $ent->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Search Engine Script.', [
          '%label' => $ent->label(),
        ]));
    }
    $form_state->setRedirectUrl($ent->toUrl('collection'));
  }

}

