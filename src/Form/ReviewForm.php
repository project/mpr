<?php

namespace Drupal\multi_peer_review\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;
use Drupal\multi_peer_review\Entity\CommonContentEntityInterface;

/**
 * Form controller for the Review edit forms.
 */
class ReviewForm extends CommonContentEntityForm implements ReviewFormInterface {


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $review = $this->entity;

    if ($review->isNew() == TRUE) {
        $default_id = MPRCommon::getNewCustomTextId();
    }
    else {
        $default_id = $review->label();
    }

    if ($this->operation == 'edit') {
      $form['#title'] = $review->getTranslatedMessage(CommonContentEntityInterface::MESSAGE_ADMIN_EDIT_TITLE);      
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique ID'),
      '#maxlength' => 255,
      '#default_value' => $default_id,
      '#required' => TRUE,
      '#disabled' => !$review->isNew(),           
    ];
    
    $form['status_display'] = [
      '#type' => 'textfield',      
      '#title' => $this->t('Status'),
      '#default_value' => $this->t($review->getStatus()), // Translated status text
      '#description' => $this->t('Indicates the status of the Review. Possible statuses: ') . implode(', ', Review::getTranslatedStatuses()),
      '#disabled' => TRUE,      
    ];      

    if ($review->isNew() == TRUE) {
        
        $invitations = Invitation::getInvitations(FALSE, NULL, Invitation::STATUS_ACCEPTED);
                
        $form['invitation'] = MPRCommon::getDefaultDropDownFormField('Invitation', 
                'The Invitation that this Review relates to.', TRUE, $review->getInvitation(), MPRCommon::getTranslatedListOptionsFromEntities($invitations));        
    }
    else {    
        $form['paper_display'] = MPRCommon::getDefaultSingleLineTextFormField('Paper', 
                'The Paper that was or is being reviewed.', FALSE, $review->getPaperTitle());
        $form['paper_display']['#disabled'] = TRUE;

        $form['reviewer_display'] = MPRCommon::getDefaultSingleLineTextFormField('Reviewer', 
                'The Reviewer who conducted or is conducting this Review.', FALSE, $review->getReviewerName());    
        $form['reviewer_display']['#disabled'] = TRUE;        
    }
    
    $form['start_date'] = MPRCommon::getDefaultDateFormField('Start Date', 
            'The date when reviewing is/was scheduled to commence.', TRUE, $review->getStartDate());
    
    
    $form['deadline'] = MPRCommon::getDefaultDateFormField('Deadline', 
            'The Review deadline.', TRUE, $review->getDeadline());        
    
    
    
    $status = $review->getStatus();
    if (($status == Review::STATUS_SUBMITTED) || ($status == Review::STATUS_CANCELLED)) {
        $form['reviewer_response'] = [
          '#type' => 'details',          
          '#title' => $this->t('Reviewer’s Response'),
          '#open' => TRUE,
        ];     

        
        if ($status == Review::STATUS_SUBMITTED) {
            $form['reviewer_response']['submit_message'] = MPRCommon::getDefaultMultiLineTextFormField('Message', 
                    'Message provided by the Reviewer at the time that they submitted the Review.', FALSE, $review->getSubmitMessage());     
            
            $form['reviewer_response']['email_attachments_control'] = MPRCommon::getDefaultMultiFileFormField('Attachments',
                    'One or more files that form part or all of the Review. Accepted file types: ' . MPRCommon::DOCUMENT_FILE_DESCRIPTIONS, FALSE, $review->getEmailAttachments(), MPRCommon::DOCUMENT_FILE_EXTENSIONS);


            $form['reviewer_response']['email_subject'] = MPRCommon::getDefaultSingleLineTextFormField('Email Subject', 
                    'Subject line of the submission email.', TRUE, $review->getEmailSubject());     

            $form['reviewer_response']['email_body'] = MPRCommon::getDefaultHtmlTextFormField('Email Body', 
                    'Body of the submission email.', TRUE, $review->getEmailBody());        

            $form['reviewer_response']['email_cc'] = MPRCommon::getDefaultMultiLineTextFormField('Email CC', 
                    'Email addresses of other recipients who received a copy of the submission email.', FALSE, $review->getEmailCC());        
            $form['reviewer_response']['email_cc']['#rows'] = 2;

            $form['reviewer_response']['email_bcc'] = MPRCommon::getDefaultMultiLineTextFormField('Email BCC', 
                    'Email addresses of other recipients who received a copy of the submission email without the knowledge of other recipients.', FALSE, $review->getEmailBCC());      
            $form['reviewer_response']['email_bcc']['#rows'] = 2;            
        }
        else {
            
            $form['reviewer_response']['cancel_reason'] = MPRCommon::getDefaultMultiLineTextFormField('Reason', 
                    'Reason that the Review was cancelled.', FALSE, $review->getCancelReason());        

            $form['reviewer_response']['cancel_suggestion'] = MPRCommon::getDefaultMultiLineTextFormField('Suggested Reviewer', 
                    'An alternative Reviewer who was suggested by the Reviewer upon cancelling the Review.', FALSE, $review->getCancelSuggestion());    
        }        
    }    
    
    


    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Review ID'),
      '#maxlength' => 255,
      '#default_value' => $review->id(),
      '#disabled' => !$review->isNew(),
      '#machine_name' => [
        'exists' => '\Drupal\multi_peer_review\Entity\Review::load',
      ],
      '#element_validate' => [],
    ];
    
    
    $this->insertWebDeveloperControls($form, $form_state);
    
    return parent::form($form, $form_state);
  }
  
  
  public function buildWebDeveloperControls(&$form) {
      $form['debug_option_reset_last_follow_up_timestamp'] = MPRCommon::getDefaultCheckBoxFormField('Reset Last Follow-up Timestamp', '', FALSE);
  }  
  
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);
    
    MPRCommon::filterHtmlTextFormField($form['reviewer_response'], $form_state, 'email_body');
    
    if ($entity->getStatus() != Review::STATUS_PENDING) {
        // Process email attachment control input
        $entity->validateFormProcess($form, $form_state);
    }   
   
    return $entity;
  }
  

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge([
      'label',
      'id',
    ], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    $field_names = [
      'label',
      'id',
    ];
    foreach ($violations->getByFields($field_names) as $violation) {
      list($field_name) = explode('.', $violation->getPropertyPath(), 2);
      $form_state->setErrorByName($field_name, $violation->getMessage());
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    
    $status = NULL;
    
    $review = $this->entity;   
    
    
    // Check debug options.    
    if ($this->isCheckBoxChecked($form, 'debug_option_reset_last_follow_up_timestamp') == TRUE) {        
        $review->set('cached_last_follow_up_timestamp', 0);        
    }    
    
    
    
    if ($review->isNew() == TRUE) {
        $invitation_id = $form['invitation']['#value'];
        $invitation = Invitation::load($invitation_id);
        $paper = $invitation->fabricateAndLoadPaper();
        $reviewer = $invitation->fabricateAndLoadReviewer();        
        
        // Only allow one Pending Review per Invitation        
        $allow_save = TRUE;
        foreach (Review::getReviews(NULL, NULL, 'Pending', $invitation_id) as $review) {
            $allow_save = FALSE;          
            
            $placeholder_replacements = [];
            MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
            MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
            MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);
            
            $this->messenger->addError($this->t('The Review could not be saved. A Review for %paper-title by %reviewer-title-and-full-name is already pending.', $placeholder_replacements));
            $form_state->setRebuild();            
            break;
        }        
    }
    else {
        $allow_save = TRUE;
        $reviewer = NULL;
    }
    
    if ($allow_save == TRUE) {
        $review->setNewRevision(TRUE);            
        $status = $review->save();

        
        if (($status == SAVED_NEW) && (empty($reviewer) == FALSE)) {            
            Reviewer::rebuildCachedFigures($reviewer);
        }
        
        $review->processAdminFormSaveMessages($this->logger('multi_peer_review'), $this->messenger, $status);

        if ($review->id()) {
          $form_state->setValue('id', $review->id());
          $form_state->set('id', $review->id());

          $collection_url = $review->toUrl('collection');
          $redirect = $collection_url->access() ? $collection_url : Url::fromRoute('<front>');
          $form_state->setRedirectUrl($redirect);
        }
        else {
          $this->messenger->addError($this->t('The Review could not be saved.'));
          $form_state->setRebuild();
        }
    }
    
    return $status;
  }
  
  

}

