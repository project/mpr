<?php
namespace Drupal\multi_peer_review;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\multi_peer_review\Entity\EmailTemplate;

/**
 * Defines a Multi Peer Review email.
 * 
 */
class MPREmail {
    
  var $key = '';
  var $token_and_placeholder_entities = [];
  var $to = '';
  var $cc = '';
  var $bcc = '';
  var $subject = '';
  var $body = '';
  var $attachment_sources = [];
  var $form_attachment_files = [];
    
  
  
  public function __construct($key, $token_and_placeholder_entities, $to, $cc, $bcc, $subject, $body, $attachment_sources) {
    $this->key = $key;
    $this->token_and_placeholder_entities = $token_and_placeholder_entities;
    $this->to = $to;
    $this->cc = $cc;
    $this->bcc = $bcc;
    $this->subject = $subject;
    $this->body = $body;
    $this->attachment_sources = $attachment_sources;
  }
  
  public static function createFromEmailCapableEntity($key, $token_and_placeholder_entities, $to, $email_capable_entity, $attachment_sources) {

    return new MPREmail(
        $key,
        $token_and_placeholder_entities,
        $to,
        MPRCommon::getValidEmailAddressesFromMultiEmailAddressData($email_capable_entity->getEmailCC()),
        MPRCommon::getValidEmailAddressesFromMultiEmailAddressData($email_capable_entity->getEmailBCC()),
        $email_capable_entity->getEmailSubject(),
        $email_capable_entity->getEmailBody(),
        $attachment_sources
    );    
  }
  
  public static function createFromEmailTemplate($key, $token_and_placeholder_entities, $to, $cc, $bcc, $attachment_sources) {
    
    $res = NULL;
    $email_template = EmailTemplate::load($key);
    if (empty($email_template) == FALSE) {  
        $res = new MPREmail(
            $key,
            $token_and_placeholder_entities,
            $to,
            $cc,
            $bcc,
            $email_template->getSubject(),
            $email_template->getBody(),
            $attachment_sources
        );    
    }
    
    return $res;
  }  
  
  public function addAttachmentsFromForm($managed_file_control) {
      
    $files = $managed_file_control['#files'];
    if (empty($files) == FALSE) {
        foreach ($files as $fid => $file_item) {
          
            $file = \Drupal\file\Entity\File::load($fid);

            $file_info = [
                'filecontent' => file_get_contents($file->getFileUri()),
                'filename' => $file->getFilename(),
                'filemime' => $file->getMimeType(),
                'filesize' => $file->getSize(),
            ];

            array_push($this->form_attachment_files, $file_info);     
                        
        }
    } 
    
  }
  
  public function send() {
      
    $res = FALSE; 
      
    // Prepare token data for mail merging.
    $token_service = \Drupal::token();
    $token_data = [];
    foreach ($this->token_and_placeholder_entities as $entity) {
        $token_data[$entity->getEntityTypeId()] = $entity;
    }
    
    // Add Audit email address to BCC if it is provided.
    $email_bcc = $this->bcc;
    $audit_email_address = MPRCommon::getConfigValue('audit_email_address');
    if (empty($audit_email_address) == FALSE) {
        $audit_email_address = trim($audit_email_address);
        if (empty($audit_email_address) == FALSE) {
            if (empty($email_bcc) == FALSE) {
                $email_bcc .= ', ';
            }        
            $email_bcc .= $audit_email_address;
        }
    }       
    
    
    // Generate attachments params.   
    $params = [];
    foreach ($this->attachment_sources as $attachment_source) {
        $attachment_source->addEmailAttachmentsToMailParams($params);
    }
    
    // Add attachments that were uploaded via a form.
    // These files are usually temporary.
    foreach ($this->form_attachment_files as $form_file) {
        if (array_key_exists('multi_peer_review_attachments', $params) == FALSE) {
          $params['multi_peer_review_attachments'] = $this->form_attachment_files;          
        }
        else {
            foreach ($this->form_attachment_files as $attachment) {
                array_push($params['multi_peer_review_attachments'], $attachment);
            }
        }        
    }
    
    
    // Prepare and send email.
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'multi_peer_review';
    $key = $this->key;
    $to = $this->to;
    $params['email_subject'] = $token_service->replace($this->subject, $token_data);
    $params['email_body'] = $token_service->replace($this->body, $token_data) . MPRCommon::getConfigValue('email_footer');
    
    if (empty($this->cc) == FALSE) {
        $params['email_cc'] = $this->cc;
    }
    if (empty($email_bcc) == FALSE) {
        $params['email_bcc'] = $email_bcc;
    }
    
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);    
    
    if ($result['result'] == TRUE) {
        $res = TRUE;
    }
      
    return $res;  
  }
    
}
