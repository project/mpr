<?php

namespace Drupal\multi_peer_review;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Defines a class to build a listing of Review entities.
 *
 * @see \Drupal\multi_peer_review\Entity\Review
 */
class ReviewListBuilder extends FilteredEntityListBuilder {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RendererInterface $renderer) {
    parent::__construct($entity_type, $storage);

    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('renderer')
    );
  }
  

  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQueryConditions(&$query) {
      
    // Filter by status.
    $status = \Drupal::request()->request->get('status');        
    if ((empty($status) == FALSE) && ($status != '_none')) {
        $query->condition('status', $status, '=');             
    }

    // Filter by keywords.  
    $this->applyKeywordFilter($query, ['cached_search_meta_data__value', 'cancel_suggestion__value', 'submit_message__value', 'cancel_reason__value']);
        
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQuerySort(&$query) {
    $query->sort('created', 'DESC');
  }  
  
  
  
  /**
   * {@inheritdoc}
   */   
  protected function getFilterFormFields() {
    
    $form = [];
    
    
    // Prepare Review status options.
    $options = Review::getTranslatedStatusListOptions();
    $options['_none'] = $this->t('- All -'); 
    $form['status'] = MPRCommon::getDefaultDropDownFormField('Status', '', FALSE, '', $options);
            
    $form['keywords'] = MPRCommon::getDefaultSingleLineTextFormField('Keywords', '', FALSE, '');    
 
    return $form;      
  }    
  
    
  
  
  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $res = parent::getDefaultOperations($entity);
        
    
    switch ($entity->getStatus()) {
        case Review::STATUS_PENDING:                        
            
            if (MPRCommon::isAdminUser() == FALSE) {
                $title = 'Submit Now';
            }
            else {
                $title = 'Proxy Submission';
            }
            
            $res['submit_now'] = [
                'title' => t($title),
                'weight' => 11,
                'url' => $this->ensureDestination($entity->toUrl('submit-form')),               
            ];
            
            $res['cancel'] = [
                'title' => t('Cancel'),
                'weight' => 11,
                'url' => $this->ensureDestination($entity->toUrl('cancel-form')),               
            ];            
            break;
    }
    
    return $res;
  }

  

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    
    $header['id'] = $this->t('ID');
    
    $header['paper'] = [
            'data' => $this->t('Paper'),
            'field' => 'cached_paper_title',
            'specifier' => 'cached_paper_title',
    ];    
    
    $header['reviewer'] = [
            'data' => $this->t('Reviewer'),
            'field' => 'cached_reviewer_name',
            'specifier' => 'cached_reviewer_name',
    ];  
    
    $header['status'] = [
            'data' => $this->t('Status'),
            'field' => 'status',
            'specifier' => 'status',
    ];   
    
    $header['start_date'] = [
            'data' => $this->t('Start Date'),
            'field' => 'start_date',
            'specifier' => 'start_date',
    ];      
    
    $header['deadline'] = [
            'data' => $this->t('Deadline'),
            'field' => 'deadline',
            'specifier' => 'deadline',
    ];      
        
    $header['overdue'] = $this->t('Overdue');
    
    
    $header['cached_last_follow_up_timestamp'] = [
            'data' => $this->t('Last Follow-up Date'),
            'field' => 'cached_last_follow_up_timestamp',
            'specifier' => 'cached_last_follow_up_timestamp',
    ];           
    
    $header['calculated_next_follow_up_date'] = $this->t('Next Follow-Up Date');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\multi_peer_review\ReviewInterface $entity */
    
    $row['id'] = $entity->id();
    $row['paper'] = $entity->getPaperTitle();
    $row['reviewer'] = $entity->getReviewerName();
    $row['status'] = $this->t($entity->getStatus());
    $row['start_date'] = $entity->getStartDateText();
    $row['deadline'] = $entity->getDeadlineText();
    $row['overdue'] = $this->t($entity->getOverdueText());
    $row['cached_last_follow_up_timestamp'] = $entity->getCachedLastFollowUpTimestampText();
    $row['calculated_next_follow_up_date'] = $entity->getCalculatedNextFollowUpDateText();
    
    return $row + parent::buildRow($entity);
  }


}
