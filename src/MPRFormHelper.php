<?php

namespace Drupal\multi_peer_review;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Multi Peer Review form helper.
 * 
 */
class MPRFormHelper {
    
    
  /**
   * Reference to the form using this helper instance.
   *
   * @var object
   */          
  private $owner_object;  
    
  
  /**
   * Initializes the helper instance.
   *
   * @param object $owner_object
   *   The parent of this helper instance. It should typically be a form.
   */    
  public function initialise($owner_object) {
      $this->owner_object = $owner_object;
  }
    

  /**
   * Returns a Boolean value indicating whether the current user is an administrator or not. 
   * 
   * @return bool
   *   True if the current user is an administrator, otherwise, false.
   */    
  public function isAdmin() {
    return MPRCommon::isAdminUser();
  }  
  
  
  /**
   * Returns the id of the current user. 
   *
   * @return int
   *   The current user's id.
   */    
  public function getUserId() {
    return \Drupal::request()->get('user');
  }  
  
  
  /**
   * Adds form fields populated with information from a given Paper record to a specified form.
   * The Paper information is contained in an expandable/collapsible field.
   *
   * @param array &$form
   *   Fields and information will be added to this form.
   * @param object $paper
   *   The Paper object to extract information from.
   * @param bool $details_open
   *   (optional) Determines whether or not the information container field should be expanded or collapsed.
   * @param bool $include_download
   *   (optional) Determines whether or not a link to the Paper file should be included in the output.
   */    
  public function insertPaperDetails(&$form, $paper, $details_open = FALSE, $include_download = TRUE) {
    $form['paper_container'] = [
      '#type' => 'details',          
      '#title' => new TranslatableMarkup('Paper Details'),
      '#open' => $details_open,
    ];    
    
    $form['paper_container']['header'] = [              
            '#type' => 'markup',
            '#markup' => '<dl>',
    ];
    $paper_fields = [
        'title' => new TranslatableMarkup('Title'), 
        'abstract' => new TranslatableMarkup('Abstract'), 
        'paper_type' => new TranslatableMarkup('Type'),
        'subjects' => new TranslatableMarkup('Subjects'),
        'contributors' => new TranslatableMarkup('Contributors'),
        // 'download' => new TranslatableMarkup('Download'),
    ];
    
    if ($include_download == TRUE) {
        $paper_fields['download'] = new TranslatableMarkup('Download');
    }
    
    foreach ($paper_fields as $field_name => $field_label) {
        
        switch ($field_name) {
            case 'title':
                $row_value = htmlentities($paper->label());
                break;
            case 'subjects':
                $row_value = htmlentities($paper->getSubjectsCsv());
                break;
            case 'download':
                $row_value = $paper->getFileDownloadMarkup();                
                break;         
            case 'contributors':                                
                $names = [];                
                $doc = simplexml_load_string($paper->getIndividualContributors());                             
                foreach ($doc->contributor as $contributor) {
                    $full_name = trim($contributor->first_name . ' ' . $contributor->last_name);
                    array_push($names, $full_name);
                }
                $doc = simplexml_load_string($paper->getNonIndividualContributors());                             
                foreach ($doc->contributor as $contributor) {
                    $full_name = trim($contributor->entity_name);                    
                    array_push($names, $full_name);
                }                
                
                $row_value = implode(', ', $names);
                $row_value = htmlentities($row_value);
                
                break;    
            default:
                $row_value = $paper->get($field_name)->getValue()[0]['value'];
                break;
        }                          
        
        if (empty($row_value) == FALSE) {
            $form['paper_container'][$field_name]['title'] = [              
                '#type' => 'markup',
                '#markup' => '<dt>' . $field_label . '</dt>',            
            ];
            $form['paper_container'][$field_name]['content'] = [              
                '#type' => 'markup',
                '#markup' => '<dd>' . $row_value . '</dd>',            
            ];        
        }
    }
    $form['paper_container']['footer'] = [              
            '#type' => 'markup',
            '#markup' => '</dl>',
    ];          
  }
  
  
  /**
   * Adds form fields populated with information from a given Reviewer record to a specified form.
   * The Reviewer information is contained in an expandable/collapsible field.
   *
   * @param array &$form
   *   Fields and information will be added to this form.
   * @param object $reviewer
   *   The Reviewer object to extract information from.
   * @param bool $details_open
   *   (optional) Determines whether or not the information container field should be expanded or collapsed.
   */    
  public function insertReviewerDetails(&$form, $reviewer, $details_open = FALSE) {
    $form['reviewer_container'] = [
      '#type' => 'details',          
      '#title' => new TranslatableMarkup('Reviewer Details'),
      '#open' => $details_open,
    ];


    $form['reviewer_container']['header'] = [              
            '#type' => 'markup',
            '#markup' => '<dl>',
    ];
    $detail_fields = [
        'title_and_full_name' => new TranslatableMarkup('Name'), 
        'email' => new TranslatableMarkup('Email'), 
        'phone' => new TranslatableMarkup('Phone'),
        'subjects' => new TranslatableMarkup('Subjects'),
    ];
    foreach ($detail_fields as $field_name => $field_label) {

        switch ($field_name) {
            case 'title_and_full_name':
                $row_value = $reviewer->getTitleAndFullName();
                break;
            case 'email':
                $row_value = $reviewer->getEmail();
                break;      
            case 'phone':
                $row_value = $reviewer->getPhone();
                break;               
            case 'subjects':
                $row_value = $reviewer->getSubjectsCsv();
                break;
            default:
                $row_value = $reviewer->get($field_name)->getValue()[0]['value'];
                break;
        }        



        if (empty($row_value) == FALSE) {
            $form['reviewer_container'][$field_name]['title'] = [              
                '#type' => 'markup',
                '#markup' => '<dt>' . $field_label . '</dt>',            
            ];
            $form['reviewer_container'][$field_name]['content'] = [             
                '#type' => 'markup',
                '#markup' => '<dd>' . $row_value . '</dd>',            
            ];        
        }
    }
    $form['reviewer_container']['footer'] = [             
            '#type' => 'markup',
            '#markup' => '</dl>',
    ];      
  }

  /**
   * Adds a message to a given form stating that the email link used to access the page is invalid.
   *
   * @param array &$form
   *   The message will be added to this form.
   */      
  public function insertInvalidEmailLinkMessage(&$form) {
    $form['content'] = [
        '#type' => 'markup',
        '#markup' => '<h2>' . new TranslatableMarkup('Invalid Email Link') . '</h2><p>' . new TranslatableMarkup('The link you used to access this page is invalid. If you are certain that the link is valid and would like to try again, please copy and paste the link into your browser\'s address bar and navigate to the link.') . '</p>',
    ];      
  }
  
  /**
   * Adds a message to a given form stating that the link used to access the page is invalid.
   *
   * @param array &$form
   *   The message will be added to this form.
   */      
  public function insertInvalidLinkMessage(&$form) {
    $form['content'] = [
        '#type' => 'markup',
        '#markup' => '<h2>' . new TranslatableMarkup('Invalid Link') . '</h2><p>' . new TranslatableMarkup('The link you used to access this page is invalid.') . '</p>',
    ];   
  }  
    
}
