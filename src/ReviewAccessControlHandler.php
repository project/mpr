<?php

namespace Drupal\multi_peer_review;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the Review entity type.
 *
 * @see \Drupal\multi_peer_review\Entity\Review
 */
class ReviewAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\multi_peer_review\ReviewInterface $entity */  

    /*if ($account->hasPermission('administer site configuration')) {
      return AccessResult::allowed()->cachePerPermissions();
    }*/
    
    // Check if the user has permission to administer MPR content.
    $access_result = AccessResult::allowedIfHasPermission($account, 'administer multi_peer_review_manage_all_reviews');


    return $access_result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer multi_peer_review_manage_all_reviews');
  }

}
