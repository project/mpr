<?php
/**
 * @file
 * Contains InvitationHtmlRouteProvider.php.
 */

namespace Drupal\multi_peer_review;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Invitation entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class InvitationHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($send_form_route = $this->getSendFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.send_form", $send_form_route);
    }
    
    if ($retract_form_route = $this->getRetractFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.retract_form", $retract_form_route);
    }    
    
//    if ($accept_form_route = $this->getAcceptFormRoute($entity_type)) {
//      $collection->add("entity.{$entity_type_id}.accept_form", $accept_form_route);
//    }    
    
//    if ($accept_form_route = $this->getSendFormRoute($entity_type)) {
//      $collection->add("entity.{$entity_type_id}.cancel_form", $accept_form_route);
//    }       

    return $collection;
  }

  
  
  

  /**
   * Gets the send form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getSendFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('send-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('send-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('send')) {
        $operation = 'send';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

  
  
  
  

  /**
   * Gets the retract form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRetractFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('retract-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('retract-form'));
      // Use the edit form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('retract')) {
        $operation = 'retract';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }
  
  
  
//  
//  /**
//   * Gets the acceptance form route.
//   *
//   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
//   *   The entity type.
//   *
//   * @return \Symfony\Component\Routing\Route|null
//   *   The generated route, if available.
//   */
//  protected function getAcceptFormRoute(EntityTypeInterface $entity_type) {
//    if ($entity_type->hasLinkTemplate('accept-form')) {
//      $entity_type_id = $entity_type->id();
//      $route = new Route($entity_type->getLinkTemplate('accept-form'));
//      // Use the edit form handler, if available, otherwise default.
//      $operation = 'default';
//      if ($entity_type->getFormClass('accept')) {
//        $operation = 'accept';
//      }
//      $route
//        ->setDefaults([
//          '_entity_form' => "{$entity_type_id}.{$operation}",
//          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
//        ])
//        ->setRequirement('_entity_access', "{$entity_type_id}.update")
//        ->setOption('parameters', [
//          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
//        ]);
//
//      // Entity types with serial IDs can specify this in their route
//      // requirements, improving the matching process.
//      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
//        $route->setRequirement($entity_type_id, '\d+');
//      }
//      return $route;
//    }
//  }  
//  
  
}
