<?php

namespace Drupal\multi_peer_review;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\Entity\Paper;


/**
 * Defines a class to build a listing of Paper entities.
 *
 * @see \Drupal\multi_peer_review\Entity\Paper
 */
class PaperListBuilder extends FilteredEntityListBuilder {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RendererInterface $renderer) {
    parent::__construct($entity_type, $storage);

    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('renderer')
    );
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQueryConditions(&$query) {
      
    // Filter by status.
    $status = \Drupal::request()->request->get('status');        
    if ((empty($status) == FALSE) && ($status != '_none')) {
        $query->condition('status', $status, '=');             
    }


    // Filter by paper type.
    $paper_type = \Drupal::request()->request->get('paper_type');        
    if ((empty($paper_type) == FALSE) && ($paper_type != '_none')) {
        $query->condition('paper_type', $paper_type, '=');     
    }


    // Filter by keywords.
    $this->applyKeywordFilter($query, [
        'label',
        'abstract__value',
        'subjects__value',
        'alert_recipients__value',
        'individual_contributors__value',
        'non_individual_contributors__value',
        ]);
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQuerySort(&$query) {
    $query->sort('created', 'DESC');
  }    
 
  
  /**
   * {@inheritdoc}
   */   
  protected function getFilterFormFields() {
    
    $form = [];
    
    // Prepare Paper status options.
    $options = Paper::getTranslatedStatusListOptions();
    $options['_none'] = $this->t('- All -'); 
    $form['status'] = MPRCommon::getDefaultDropDownFormField('Status', '', FALSE, '', $options);
    
    
    // Prepare Paper type options.
    $options = MPRCommon::getTranslatedListOptionsFromConfig('paper_types');
    $options['_none'] = $this->t('- All -'); 
    $form['paper_type'] = MPRCommon::getDefaultDropDownFormField('Type', '', FALSE, '', $options);    
    
        
    $form['keywords'] = MPRCommon::getDefaultSingleLineTextFormField('Keywords', '', FALSE, '');
 
    return $form;      
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $res = parent::getDefaultOperations($entity);
        
    // Shortcut to generate an Invitation for the Paper.
    if (($entity->getPeerReviewComplete() == FALSE) && ($this->checkAccess('administer multi_peer_review_manage_all_invitations') == TRUE)) {
        $res['invite_reviewer'] = [
            'title' => t('Invite Reviewer'),
            'weight' => 11,
            'url' => $this->ensureDestination(Url::fromRoute('entity.invitation.add_form', [], ['query' => ['paper' => $entity->id()]])),               
        ];
    }
    
    return $res;
  }  
  

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {    
             
    $header['id'] = $this->t('ID');
    
    $header['label'] = [
            'data' => $this->t('Title'),
            'field' => 'label',
            'specifier' => 'label',
    ];

    $header['paper_type'] = [
            'data' => $this->t('Type'),
            'field' => 'paper_type',
            'specifier' => 'paper_type',
    ];    
    
    $header['status'] = [
            'data' => $this->t('Status'),
            'field' => 'status',
            'specifier' => 'status',
    ];     
        
    
    $header['checklist_completion_percentage_text'] = $this->t('Checklist');
        
    
    $header['owner'] = $this->t('Owner');
    $header['owner'] = [
            'data' => $this->t('Owner'),
            'field' => 'cached_owner_display_name',
            'specifier' => 'cached_owner_display_name',
    ];      
    
    $header['created'] = [
            'data' => $this->t('Created'),
            'field' => 'created',
            'specifier' => 'created',
    ];  

    $header['changed'] = [
            'data' => $this->t('Modified'),
            'field' => 'changed',
            'specifier' => 'changed',
    ];      
    
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\multi_peer_review\PaperInterface $entity */
          
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();    
    $row['paper_type'] = $this->t($entity->getPaperType());
    $row['status'] = $entity->getTranslatedVerboseStatusText();
    $row['checklist_completion_percentage_text'] = $entity->getChecklistCompletionPercentText();
    $row['owner'] = $entity->getOwner()->getUsername();
    $row['created'] = MPRCommon::getFormattedDateText($entity->getCreatedTime());
    $row['changed'] = MPRCommon::getFormattedDateText($entity->getChangedTime());

    return $row + parent::buildRow($entity);
  }


}
