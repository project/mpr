<?php

namespace Drupal\multi_peer_review;

/**
 * @file
 * This file contains an abstract class that provides a template and helper methods for developing Search Engine Scripts that can be executed by the Multi Peer Review module.
 * All Search Engine Scripts should extend it.
 * Example:
 * class YourSearchEngineScript extends \Drupal\multi_peer_review\MPRSearchEngineScript
 */

/**
 * Class MPRSearchEngineScript
 * @package Drupal\multi_peer_review
 */
abstract class MPRSearchEngineScript
{
    
  /**
   * Defines characters that are commonly escaped.
   *
   */  
   const CHAR_DOUBLE_QUOTE = '"';
   const CHAR_SINGLE_QUOTE = "'";
   const CHAR_LF = "\n"; // Line Feed
   const CHAR_CR = "\r"; // Carriage Return
   const CHAR_EMPTY = '';
   
    
  /**
   * A reference to the SES object that stores this script.
   *
   * @var SES
   */  
    protected $storage_handler;
    
    
    
    /**
     * Returns the name of this Search Engine Script.
     *     
     * @return string
     */    
    abstract function getName();
    
    /**
     * Returns the version of this Search Engine Script.
     *     
     * @return string
     */        
    abstract function getVersion();
    
    /**
     * Returns a brief description of how and from where this script will obtain data.
     *     
     * @return string
     */        
    abstract function getDescription();    
    
    /**
     * Returns the name of the developer who developed this script.
     *     
     * @return string
     */        
    abstract function getDeveloperName();       
    
    /**
     * Returns the website address of the developer who developed this script.
     *     
     * @return string
     */        
    abstract function getDeveloperUrl();        
    
    
    /**
     * Add new results to the search cache. The search cache consists of SearchResultItem entities.
     * 
     * @param int $target_new_result_count
     *   Preferred number of new results to add to the search cache. The actual number does not need to meet the target.
     * @param int $search_cancel_timestamp
     *   Unix timestamp. The method should stop seeking more results at this time.
     *     
     */         
    abstract function buildSearchCache($target_new_result_count, $search_cancel_timestamp);
    
    
    /**
     * Tests the script to ensure that it is still functioning. The test process may involve testing connections to source endpoints.
     * 
     * @return bool
     *   Indicates if the test passed (TRUE) or failed (FALSE).
     */     
    abstract function selfTest();
        
    
    /**
     * The Multi Peer Review module will notify the Search Engine Script developer using this method if it is overridden.
     * 
     */  
    public function notifyScriptDeveloperOfSelfTestFailure() {
      // Not implemented by default as not all developers may use this method.
    }
    
    
    /**
     * Sets the storage handler for this instance of the SES.
     * 
     * @param var $ses_object
     *   An SES config entity.
     * 
     */     
    public function setStorageHandler($ses_object) {
        $this->storage_handler = $ses_object;
    }
    
    public function commitStorageDataToDb() {
        return $this->storage_handler->save();
    }    
    
    public function getDataDirectly() {
        return $this->storage_handler->getInternalStorage();
    }
    
    public function setDataDirectly($value, $commit_to_db = FALSE) {
        $this->storage_handler->setInternalStorage($value);     
        if ($commit_to_db == TRUE) {
            $this->commitStorageDataToDb();
        }
    }
    
    public function getData($key, $default_value = NULL) {
        $res = $default_value;
        
        $storage_data = $this->getDataDirectly();
        if (empty($storage_data) == FALSE) {
            $storage_data = unserialize($storage_data);
            
            if (array_key_exists($key, $storage_data) == TRUE) {
                $res = $storage_data[$key];
            }
        }
        
        return $res;
    }
    
    public function setData($key, $value, $commit_to_db = FALSE) {
        $storage_data = $this->getDataDirectly();
        if (empty($storage_data) == FALSE) {
            $storage_data = unserialize($storage_data);
        }        
        else {
            $storage_data = [];
        }
        
        $storage_data[$key] = $value;
        
        $storage_data = serialize($storage_data);
        
        $this->setDataDirectly($storage_data, $commit_to_db);              
    }
    
    
    public function getTextBetweenText($subject, $start_text, $end_text) {
        return MPRCommon::getTextBetweenText($subject, $start_text, $end_text);
    }
    
    public function getExternalUrl(&$web_client, $external_url, $bypass_cache = FALSE) {
        return MPRCommon::getExternalUrl($web_client, $external_url, $bypass_cache);
    }
    
    public function getSearchResultItems() {
        return Entity\SearchResultItem::getSearchResultItems($this->storage_handler->id());
    }
    
    public function getSearchResultItemByRemoteId($remote_id) {
        return Entity\SearchResultItem::loadByRemoteId($remote_id, $this->storage_handler->id());
    }
    
    public function clearSearchCache() {
        foreach ($this->getSearchResultItems() as $item_id => $item) {
            $item->delete();
        }
    }


}