<?php

namespace Drupal\multi_peer_review;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Defines a class to build a listing of filtered entities.
 *
 */
abstract class FilteredEntityListBuilder extends EntityListBuilder {

  private $accessResults = [];  
  private $accessUser = NULL;
    
    
  protected function checkAccess($permission) {
      
    $res = FALSE;  
    if (array_key_exists($permission, $this->accessResults) == TRUE) {
        $res = $this->accessResults[$permission];
    }
    else {
        if ($this->accessUser === NULL) {
            $this->accessUser = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());            
        }
        
        $res = \Drupal\Core\Access\AccessResult::allowedIfHasPermission($this->accessUser, $permission)->isAllowed();
        $this->accessResults[$permission] = $res;        
    }
      
    return $res;
  }

  protected function getListItemQuery() {
    $query = $this->getStorage()->getQuery();
    $query->condition('archived', FALSE, '=');
    
    return $query;
  }
  
  protected abstract function applyListItemQueryConditions(&$query);
  
  
  protected abstract function applyListItemQuerySort(&$query);
  
  
  protected abstract function getFilterFormFields();

  
  protected function applyKeywordFilter(&$query, $include_entity_fields, $search_keywords = NULL) {      
    
    if ($search_keywords == NULL) {
        $search_keywords = \Drupal::request()->request->get('keywords');
    }
    
    if (empty($search_keywords) == FALSE) {

        // Get database instance.
        $db = \Drupal::database();
        
        // Create an orConditionGroup.
        $orGroup = $query->orConditionGroup();

        // Add each keyword to the condition.
        foreach (explode(' ', $search_keywords) as $search_keyword) {

            $search_keyword_trimmed = trim($search_keyword);
            if ($search_keyword_trimmed != '') {
                $search_keyword_trimmed = '%' . $db->escapeLike($search_keyword_trimmed) . '%';

                foreach ($include_entity_fields as $field_name) {
                    $orGroup->condition($field_name, $search_keyword_trimmed, 'LIKE');
                }
            }
        }

        // Add the group to the query.
        $query->condition($orGroup);            
    }       
  }


  protected function applyListItemQueryArchivedCondition(&$query) {
    $query->condition('archived', FALSE, '=');
  }


  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery();
    $this->applyListItemQueryArchivedCondition($query);
    
    $operation = \Drupal::request()->request->get('op');
    if ($operation != $this->t('Reset')) {
       $this->applyListItemQueryConditions($query);
    }
            
    if (empty(\Drupal::request()->get('sort')) == TRUE) {
        // Apply default sort.
        $this->applyListItemQuerySort($query);
    }
    else {
        // Sort according to table header.
        $header = $this->buildHeader();
        $query->tableSort($header);             
    }
    
    
    // If a limit is specified, ensure to reset paging when submitting the form.
    if (empty($this->limit) == FALSE) {
        if (($operation == $this->t('Filter')) || ($operation == $this->t('Reset'))) {
            \Drupal::requestStack()->getCurrentRequest()->query->remove('page');
        }
    }
    
   
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    
    return $query->execute();
  }
  

  public function getEmptyTableMessage() {
    return $this->t('No @label found.', ['@label' => $this->entityType->getPluralLabel()]);
  }
  
    
  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    
      
 
    $form_definition = $this->getFilterFormFields();
    $form_keys = array_keys($form_definition);
    
    
    
    $operation = \Drupal::request()->request->get('op');
    
    
    $defaults = [];
    if (($operation == 'Reset') || ($operation == $this->t('Reset'))) {
        foreach ($form_keys as $form_key) {
            $default_value = $form_definition[$form_key]['#default_value'];
            if (empty($default_value) == TRUE) {
                $default_value = NULL;
            }
            $defaults[$form_key] = $default_value;
        }
    }
    else {    
        foreach ($form_keys as $form_key) {
            $defaults[$form_key] = \Drupal::request()->request->get($form_key);
        }        
    }
    
    
    // Determine if reset button should be displayed.
    $show_reset_button = FALSE;
    if (empty($defaults) == FALSE) {
        foreach ($defaults as $key => $value) {            
            if ((empty($value) == FALSE) && ($value != '_none')) {                
                $show_reset_button = TRUE;
                break;
            }
        }
    }
    
    
    // Initialise form.
    $form = [
        '#type' => 'form',
        '#name' => 'search_form',
        '#attributes' => [
            'class' => ['form--inline', 'clearfix'],
        ],        
    ];
    
    // Build form.
    foreach ($form_definition as $field_key => $field_value) {
        $form[$field_key] = $field_value;
        $form[$field_key]['#name'] = $field_key;
        $form[$field_key]['#value'] = $defaults[$field_key];          
    }
    
    
    // Configure form actions.
    $form['actions'] = [
        '#type' => 'actions',
        '#attributes' => [
            'class' => ['form-actions', 'js-form-wrapper', 'form-wrapper'],
        ],
    ];
    $form['actions']['filter_button'] = [
        '#type' => 'button',           
        '#value' => $this->t('Filter'),        
    ];
    if ($show_reset_button == TRUE) {
        $form['actions']['reset_button'] = [
            '#type' => 'button',        
            '#value' => $this->t('Reset'),        
        ];    
    }
    
    
    $build['form'] = $form;    
    
    // Merge array with parent array.
    foreach (parent::render() as $key => $value) {
        $build[$key] = $value;
    }
    
    
    // Change message to imply that records may exist but they may be filtered out.
    $build['table']['#empty'] = $this->getEmptyTableMessage();
    
    
    return $build;
  }  
  


}
