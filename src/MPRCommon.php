<?php

namespace Drupal\multi_peer_review;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines common functions and constants used by the Multi Peer Review module.
 * 
 */
class MPRCommon 
{    
    
  /**
   * Defines the max character length for different text boxes.
   *
   */    
  const HTML_TEXT_LENGTH = 1048576;
  const TEXT_AREA_LENGTH = 4096;
    
  /**
   * Defines 'document' files, which could be editable or rasterised.
   *
   */        
  const DOCUMENT_FILE_EXTENSIONS = 'docx doc pdf jpg png txt';
  const DOCUMENT_FILE_DESCRIPTIONS = 'Microsoft Word documents (docx, doc), PDFs (pdf), Images (jpg, png), Text documents (txt)';

  /**
   * Default text for broken relationships between entities.
   *
   */     
  const ENTITY_ISSUE_BROKEN_RELATIONSHIP = '-Deleted-';
    
  /**
   * Default text for non-applicable values.
   *
   */     
  const VALUE_STATE_NON_APPLICABLE = 'N/A';    
    
    
  /**
   * Named date formats defined and used by the Multi Peer Review module.
   *
   */     
  const NAMED_DATE_FORMAT_DEFAULT_DATE_TIME = 'multi_peer_review_default_date_time';
  const NAMED_DATE_FORMAT_SHORT_DATE = 'multi_peer_review_short_date';    
  const NAMED_DATE_FORMAT_VERBOSE_DATE = 'multi_peer_review_verbose_date';    
        
  
  
  /**
   * State API keys defined and used by the Multi Peer Review module.
   *
   */     
  const EXECUTION_INTERVAL_MINUTE = 'multi_peer_review.next_exec_time_minute';
  const EXECUTION_INTERVAL_HOURLY = 'multi_peer_review.next_exec_time_hourly';
  const EXECUTION_INTERVAL_DAILY = 'multi_peer_review.next_exec_time_daily';    
  const EXECUTION_INTERVAL_SES = 'multi_peer_review.ses.next_exec_time_variable';     
  
  
  /**
   * Roles defined and used by the Multi Peer Review module.
   *
   */
  const ROLE_GUEST = 'multi_peer_review_guest';
  const ROLE_STAFF = 'multi_peer_review_staff';
  const ROLE_REVIEWER = 'multi_peer_review_reviewer';
  
  
  
  /**
   * May contain an error message set by any process in this module. Used as a module scope token in hook_tokens.
   *
   * @var string
   */
  static public $last_error = '';
  
    
    
  /**
   * Constructs a BaseFieldDefinition object for a integer field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
  public static function getDefaultIntegerDbField($label, $description, $required) {                
    $res = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup($label))
      ->setDescription(new TranslatableMarkup($description))
      ->setRevisionable(TRUE)
      ->setRequired($required);        

    return ($res);
  }    
        
  /**
   * Constructs a BaseFieldDefinition object for a decimal field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultDecimalDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('decimal')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)
          ->setRequired($required);        
        
        return ($res);
    }      
    
  /**
   * Constructs a BaseFieldDefinition object for a single-line text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param int $max_length
   *   (optional) The maximum length of characters allowed to be entered into this field.
   */    
    public static function getDefaultSingleLineTextDbField($label, $description, $required, $max_length = 255) {                
        $res = BaseFieldDefinition::create('string')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)
          ->setSetting('max_length', $max_length)
          ->setRequired($required);        

        return ($res);
    }
    
    
  /**
   * Constructs a BaseFieldDefinition object for an email address text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultSingleEmailAddressDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('email')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)
          ->setRequired($required);        

        return ($res);
    }    
        
    
  /**
   * Constructs a BaseFieldDefinition object for an HTML text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultHtmlTextDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('text_long')
          ->setLabel(new TranslatableMarkup($label))          
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)
          ->setSetting('max_length', MPRCommon::HTML_TEXT_LENGTH)            
          ->setRequired($required);        

        return ($res);
    }    
    
  /**
   * Constructs a BaseFieldDefinition object for multi-line text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultMultiLineTextDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('text_long')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)
          ->setSetting('max_length', MPRCommon::TEXT_AREA_LENGTH)
          ->setRequired($required);        
        
        return ($res);
    }       
    
    
  /**
   * Constructs a BaseFieldDefinition object for a file upload control.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultFileDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('file')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)          
          ->setRequired($required);        
        
        return ($res);
    }          
    
  /**
   * Constructs a BaseFieldDefinition object for a file upload control that allows multiple uploads.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultMultiFileDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('text_long')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)          
          ->setRequired($required);        
        
        return ($res);
    }      
    
  /**
   * Constructs a BaseFieldDefinition object for a date control.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultDateDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('datetime')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)          
          ->setRequired($required);        
        
        return ($res);
    }     
    
  /**
   * Constructs a BaseFieldDefinition object for a timestamp-based control.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultTimestampDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('timestamp')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)          
          ->setRequired($required);        
        
        return ($res);
    }       
    
  /**
   * Constructs a BaseFieldDefinition object for a checkbox control.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultCheckBoxDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('boolean')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)  
          ->setDefaultValue(FALSE)
          ->setRequired($required);        
        
        return ($res);
    }      
    
    
  /**
   * Constructs a BaseFieldDefinition object for a drop-down list.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   */    
    public static function getDefaultDropDownDbField($label, $description, $required) {                
        $res = BaseFieldDefinition::create('list_string')
          ->setLabel(new TranslatableMarkup($label))
          ->setDescription(new TranslatableMarkup($description))
          ->setRevisionable(TRUE)
          //->setSetting('max_length', $max_length)
          ->setRequired($required);        
        
        return ($res);
    }    
    
    
  /**
   * Provides a form field definition for a single line text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param var $default_value
   *   The default value of the field. Typically, it is the value that was last saved.
   * @param int $max_length
   *   (optional) The maximum length of characters allowed to be entered into this field.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */        
    public static function getDefaultSingleLineTextFormField($label, $description, $required, $default_value, $max_length = 255, $placeholder_replacements = []) {        
        $res = [
          '#type' => 'textfield',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),
          '#maxlength' =>  $max_length,            
          '#default_value' => $default_value,
          //'#size' => 20, // Not used at the moment
          '#required' => $required,            
          '#description_display' => !empty($description), 
        ];     
        
        return ($res);
    }       
    
    
    
  /**
   * Provides a form field definition for an HTML text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param var $default_value
   *   The default value of the field. Typically, it is the value that was last saved.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */        
    public static function getDefaultHtmlTextFormField($label, $description, $required, $default_value, $placeholder_replacements = []) {        
        $res = [
          '#type' => 'text_format',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),
          '#maxlength' => MPRCommon::HTML_TEXT_LENGTH,            
          '#default_value' => $default_value,
          '#required' => $required,          
          '#format' => 'multi_peer_review_html', //the format used for editor.
          '#description_display' => !empty($description),             
        ];     
        
        return ($res);
    }
    
  /**
   * Provides a form field definition for an HTML text box.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param var $default_value
   *   The default value of the field. Typically, it is the value that was last saved.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */        
    public static function getDefaultMultiLineTextFormField($label, $description, $required, $default_value, $placeholder_replacements = []) {        
        $res = [
          '#type' => 'textarea',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),
          '#maxlength' => MPRCommon::TEXT_AREA_LENGTH,            
          '#default_value' => $default_value,
          '#required' => $required,
          '#description_display' => !empty($description),             
            //'#cols' => 20, // Not used for now
            //'#rows' => 3,  // Not used for now
        ];     
        
        return ($res);
    }    
    
    
  /**
   * Provides a form field definition for a drop-down/combo box field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param var $default_value
   *   The default value of the field. Typically, it is the value that was last saved.
   * @param array $options
   *   The drop-down selection options. 
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */       
    public static function getDefaultDropDownFormField($label, $description, $required, $default_value, $options, $placeholder_replacements = []) {
        
        $res = [
          '#type' => 'select',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),
          '#options' => $options,
          '#empty_value' => '_none',
          '#sort_options' => TRUE,        
          '#default_value' => $default_value,        
          '#required' => $required,
          '#description_display' => !empty($description),             
        ];            
        
        return ($res);
    }
    
    
  /**
   * Provides a form field definition for a file upload field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param array $default_value
   *   The default value of the field. It will be a file array containing these keys: target_id, display, description.
   * @param string $valid_file_extensions
   *   (optional) A string of space delimited file extensions without leading dots that should be accepted. E.g. txt doc xlsx,
   *   By default, all files are accepted.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */       
    public static function getDefaultFileFormField($label, $description, $required, $default_value, $valid_file_extensions = '', $placeholder_replacements = []) {
        
        // Old code that relied on manual file management
        /*
        $res = [
          '#type' => 'file',
          '#title' => new TranslatableMarkup($label),
          '#description' => new TranslatableMarkup($description),
          '#required' => $required,
          '#default_value' => $default_value,            
          '#upload_validators' => [
            'file_validate_extensions' => $valid_file_extensions, 
          ]];
        */

        //$max_filesize = \Drupal\Component\Utility\Environment::getUploadMaxSize();

        // Get the default storage schema. E.g. 'public' storage
        $default_scheme = (\Drupal::config('system.file'))->get('default_scheme');        
        
        $res = [
          '#type' => 'managed_file',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),          
          '#upload_location' => $default_scheme . '://multi_peer_review', // Directory specifically for Multi Peer Review files
          //'#upload_validators' => [
          //  'file_validate_extensions' => $valid_file_extensions,
          //  'file_validate_size' => [$max_filesize],
          //],
          '#required' => $required,
          '#description_display' => !empty($description),             
        ];
        
        if ($valid_file_extensions != '')
        {
            $res['#upload_validators'] = ['file_validate_extensions' => [$valid_file_extensions]];
        }
        
        if ($default_value['target_id'] != NULL)
        {
            $res['#default_value'] = [$default_value['target_id']];
        }
        
        return ($res);
    }    
    
    
  /**
   * Provides a form field definition for a multiple file upload field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param bool $required
   *   Determines whether or not the field is required.
   * @param array $default_value
   *   The default value of the field. It will be an array of integers that refer to file ids (fids).
   * @param string $valid_file_extensions
   *   (optional) A string of space delimited file extensions without leading dots that should be accepted. E.g. txt doc xlsx,
   *   By default, all files are accepted.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */       
    public static function getDefaultMultiFileFormField($label, $description, $required, $default_value, $valid_file_extensions = '', $placeholder_replacements = []) {        
        // Get the default storage schema. E.g. 'public' storage
        $default_scheme = (\Drupal::config('system.file'))->get('default_scheme');        
        
        $res = [
          '#type' => 'managed_file',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),          
          '#upload_location' => $default_scheme . '://multi_peer_review', // Directory specifically for Multi Peer Review files          
          '#multiple' => TRUE,
          '#required' => $required,
          '#description_display' => !empty($description),             
        ];
        
        if ($valid_file_extensions != '')
        {
            $res['#upload_validators'] = ['file_validate_extensions' => [$valid_file_extensions]];
        }
        
        if ($default_value != NULL)
        {
            $res['#default_value'] = $default_value;           
        }
        
        return ($res);
    }    
    
    
  /**
   * Provides a form field definition for a checkbox field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param var $default_value
   *   The default value of the field. Ideally a Boolean value, but the number 1 in numeric or string format will be accepted as TRUE.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */       
    public static function getDefaultCheckBoxFormField($label, $description, $default_value, $placeholder_replacements = []) {
        
        if ($default_value === NULL) {
            $effective_default_value = FALSE;
        }
        else {
            if (($default_value == TRUE) || ($default_value == 1) || ($default_value == '1')) {
                $effective_default_value = TRUE;
            }
            else {
                $effective_default_value = FALSE;
            }
        }
        
        $res = [
          '#type' => 'checkbox',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),          
          '#default_value' => $effective_default_value,
          '#description_display' => !empty($description), 
        ];            
        
        
        return ($res);
    }    
    
    
  /**
   * Provides a form field definition for a date field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param string $default_value
   *   The default value of the field. Typically, it is the value that was last saved.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */     
    public static function getDefaultDateFormField($label, $description, $required, $default_value, $placeholder_replacements = []) {                
        $res = [
          '#type' => 'date',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),          
          '#default_value' => $default_value,        
          '#required' => $required,
          '#description_display' => !empty($description), 
        ];            
        
        return ($res);
    }       
    
  /**
   * Provides a form field definition for an email address field.
   *
   * @param string $label
   *   The field label.
   * @param string $description
   *   The field description.
   * @param string $default_value
   *   The default value of the field. Typically, it is the value that was last saved.
   * @param array $placeholder_replacements
   *   (optional) Placeholder replacement values used when translating the label and description.
   */   
    public static function getDefaultSingleEmailAddressFormField($label, $description, $required, $default_value, $placeholder_replacements = []) {                
        $res = [
          '#type' => 'email',
          '#title' => new TranslatableMarkup($label, $placeholder_replacements),
          '#description' => new TranslatableMarkup($description, $placeholder_replacements),          
          '#default_value' => $default_value,        
          '#required' => $required,
          '#description_display' => !empty($description),             
        ];            
        
        return ($res);
    }      
    
    
  /**
   * Returns the value set for a Multi Peer Review module setting.
   *
   * @param string $setting_key
   *   The key of the setting parameter to load.
   * 
   * @return string
   *   String value of setting.
   */       
    public static function getConfigValue($setting_key) {                        
        return (\Drupal::config('multi_peer_review.config_settings'))->get($setting_key);
    }    
    
  /**
   * Returns a comma-separated string of valid email addresses. 
   *
   * @param string $value
   *   The value obtained from a single or multi email address field. Users are advised to separate each address using commas, but some may accidentally use line breaks.
   * 
   * @return string
   *   A comma-separated string consisting of only valid email addresses from single or multi email address data.
   */       
    public static function getValidEmailAddressesFromMultiEmailAddressData($value) {        
        
        $res = '';
        if (empty($value) == FALSE) {
            
            // Normalise delimiters
            $emails = $value;
            $emails = str_replace(';', "\n", $emails);
            $emails = str_replace(',', "\n", $emails);
            $emails = str_replace("\r", '', $emails);
            
            $valid_emails = [];
            foreach (explode("\n", $emails) as $email) {
                
                $trimmed_email = trim($email);
                if ((empty($trimmed_email) == FALSE) && (\Drupal::service('email.validator')->isValid($trimmed_email) == TRUE)) {
                    array_push($valid_emails, $trimmed_email);
                }
                
            }
            
            if (count($valid_emails) != 0) {
                $res = implode(', ', $valid_emails);
            }
        }
               
        return ($res);
    }    
    
    
    public static function getArrayFromMultiLineValues($values) {

      $res = [];
      if (empty($values) == FALSE) {
          $normalised_values = str_replace("\r", '', $values);
          
          foreach (explode("\n", $normalised_values) as $item) {
              $trimmed_item = trim($item);
              if ($trimmed_item != '') {
                  array_push($res, $trimmed_item);
              }
          }              
      }

      return $res;
    }      
    
    public static function getCsvFromMultiLineValues($values) {

      $res = '';
      
      $items = MPRCommon::getArrayFromMultiLineValues($values);
      if (empty($items) == FALSE) { 
        $res = implode(', ', $items);
      }
      
//      if (empty($values) == FALSE) {
//          $normalised_values = str_replace("\r", '', $values);
//
//          $items = [];
//          foreach (explode("\n", $normalised_values) as $item) {
//              $trimmed_item = trim($item);
//              if ($trimmed_item != '') {
//                  array_push($items, $trimmed_item);
//              }
//          }    
//          $res = implode(', ', $items);
//      }

      return $res;
    }      
    
  /**
   * Returns an associative array containing options fit for use in list fields. Text values are translated.
   *
   * @param string $setting_key
   *   The key of the setting parameter to load.
   * 
   * @return array
   *   Array of strings.
   */       
    public static function getTranslatedListOptionsFromConfig($setting_key) {        
        
        $res = [];
        
        $raw_data = MPRCommon::getConfigValue($setting_key);
        $raw_data = str_replace("\r", '', $raw_data);
  
        $items = explode("\n", $raw_data);
        foreach ($items as $item)
        {            
            $array_item = trim($item);
            if (empty($array_item) == FALSE) {
                $res[$array_item] = new TranslatableMarkup($array_item);
            }
        }
        
        asort($res);
        
        return ($res);
    }
    
  /**
   * Returns an associative array fit for use in list fields. Text values are translated.
   *
   * @param string $entity_type_id
   *   The name of the entity type to load. If the entity does not implement MPRDropDownListItem, it's id and label will be used when generating options.
   * 
   * @return array
   *   Associative array of strings.
   */       
    public static function getTranslatedListOptionsFromEntityType($entity_type_id) {        
        
        $entities = \Drupal::entityTypeManager()->getStorage($entity_type_id)->loadMultiple();
        
        return MPRCommon::getTranslatedListOptionsFromEntities($entities);        
    }    
    
    
  /**
   * Returns an associative array fit for use in list fields. Text values are translated.
   *
   * @param array $entities
   *   An array of entities. If an entity does not implement MPRDropDownListItem, it's id and label will be used when generating options.
   * 
   * @return array
   *   Associative array of strings.
   */       
    public static function getTranslatedListOptionsFromEntities($entities) {        
        
        $res = [];
                
        $is_drop_down_list_item = NULL;        
        foreach ($entities as $entity) {       

            if ($is_drop_down_list_item === NULL) {
                $is_drop_down_list_item = in_array("Drupal\multi_peer_review\MPRDropDownListItem", class_implements($entity));                
            }
                        
            if ($is_drop_down_list_item === FALSE) {
                $res[$entity->id()] = new TranslatableMarkup($entity->label());  
            }
            else {
                $res[$entity->getListItemValue()] = new TranslatableMarkup($entity->getListItemText());
            }

        }      

        asort($res);        
               
        return $res;
    }       


  /**
   * Returns a string containing a unique id based on the Unix Epoch.
   * 
   * @return string
   *   A numeric string such as 1596807083016428000.
   */      
    public static function getNewCustomTextId() {
        $res = microtime();        
        $ar = explode(' ', $res);

        $base_time = 1597200000;
        $current_time = intval($ar[1]);
        $cut_time = $current_time - $base_time;

        $res = strval($cut_time) . $ar[0];
        $res = str_replace('.', '', $res);
        
        return $res;
    }
    
  /**
   * Returns a string based on the caller and the time. It is suitable for email hashes.
   * 
   * @param object $caller
   *   The instance of an object calling this method.
   * @param string $additional_salt
   *   (optional) Additional salt to add to the hash, for instance, an entity id.
   * 
   * @return string
   *   An alphanumeric string such as 20100810214f1fafdf377197aaef3a42bd0d1819f6fa0ada21.
   */      
    public static function getNewEmailHash($caller, $additional_salt = '') {
        $caller_class = get_class($caller);         
        $res = date("YmdH") . sha1('mprA' . str_rot13(strval((time() % 86868))) . 'mprB' . $additional_salt . 'mprC' . strrev($caller_class));
        return $res;
    }    
        
    
  /**
   * Validates an email hash string.
   * 
   * @param string $email_hash
   *   An email hash obtained from a query string or other source that may have been tampered with.
   * 
   * @return bool
   *   If false, the email hash contains unexpected characters.
   */      
    public static function isValidEmailHash($email_hash) {
        
        $res = FALSE;
        
        if ((empty($email_hash) == FALSE) && (is_string($email_hash) == TRUE)) {
              
            $max = strlen($email_hash);
            
            // All email hashes start with yyyyMMddHH, which is 10 characters long.
            if ($max > 10) {
                
                $valid_chars = 'abcdef1234567890';          
                $index = 0;
                
                // Assume the hash is valid unless proven otherwise.
                $res = TRUE;
                while ($index < $max) {
                    $chr = substr($email_hash, $index, 1);
                    if (strpos($valid_chars, $chr) === FALSE) {
                        // Character not found in string of valid characters.
                        $res = FALSE;
                        $index = $max; // Exit loop gracefully.
                    }
                    $index++;
                }
            }                
        }
        
        return $res;
    }    
    
    
    public static function stripNonAlphanumericCharacters($text, $symbols_to_allow = '') {
        
        $res = '';
        $valid_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' . $symbols_to_allow;
        $index = 0;
        $max = strlen($text);
        while ($index < $max) {
            $char = substr($text, $index, 1);
            
            if (strpos($valid_chars, $char) !== FALSE) {
                $res = $res . $char;
            }
            
            $index++;
        }
        
        return $res;
    }      
    
    public static function getFormattedDateText($time, $named_date_format = MPRCommon::NAMED_DATE_FORMAT_DEFAULT_DATE_TIME) {
        
        $res = MPRCommon::VALUE_STATE_NON_APPLICABLE;
        
        if (empty($time) == FALSE) {
            // Convert short dates (yyyy-MM-dd) to timestamp
            if (is_int($time) == FALSE) {
                if ((strpos($time, '-') > 0) && (strlen($time) == 10)) {                
                    $time = strtotime($time);
                }
            }
            
            $res = \Drupal::service('date.formatter')->format($time, $named_date_format);
        }
        
        return $res;
    }    
    
    
    public static function getEntityPlaceholderReplacements($entity, $placeholder_prefix = '%') {
        
        $res = [];
        
        $token_info = forward_static_call([get_class($entity), 'getTokenInfo']);       
        foreach ($token_info['tokens'] as $type => $token) {            
            foreach (array_keys($token) as $name) {
                $placeholder_key = $placeholder_prefix . $type . '-' . $name;
                $res[$placeholder_key] = $entity->getTokenReplacementValue($name);
            }
        }
        
        return $res;
    }        
    
    
    public static function addWebsitePlaceholderReplacements(&$placeholder_replacements, $placeholder_prefix = '%') {   
        $config = \Drupal::config('system.site');        
        $placeholder_replacements[$placeholder_prefix . 'site-name'] = $config->get('name');        
        
        $home = \Drupal\Core\Url::fromRoute('<front>');
        $home->setAbsolute();
        $placeholder_replacements[$placeholder_prefix . 'site-url'] = $home->toString();
        
        $current_user = \Drupal::currentUser();
        $placeholder_replacements[$placeholder_prefix . 'user-display-name'] = $current_user->getDisplayName();
        $placeholder_replacements[$placeholder_prefix . 'user-account-name'] = $current_user->getAccountName();
        $placeholder_replacements[$placeholder_prefix . 'user-mail'] = $current_user->getEmail();
    }      
    
    public static function buildEntityPlaceholderReplacements(&$placeholder_replacements, $entity, $placeholder_prefix = '%') {        
        foreach (MPRCommon::getEntityPlaceholderReplacements($entity, $placeholder_prefix) as $key => $value) {
            $placeholder_replacements[$key] = $value;
        }
    }         
    
    
    public static function getTextBetweenText($subject, $start_text, $end_text) {
        $res = '';
        
        $pos = strpos($subject, $start_text);
        if ($pos !== FALSE) {
            $res = substr($subject, $pos + strlen($start_text));
            
            $pos = strpos($res, $end_text);
            if ($pos !== FALSE) {
                $res = substr($res, 0, $pos);
            }
        }
        
        return $res;
    }    
    
    
    public static function getExternalUrl(&$web_client, $external_url, $bypass_cache = FALSE) {
        
        $content_data = '';
        $url_hash = sha1($external_url);
        $content_origin = 'Web';
        
        // Get page source from the cache if possible and requested.
        if ($bypass_cache == FALSE) {
            $cache_item = Entity\WebRequestCacheItem::load($url_hash);
        }
        else {
            $cache_item = NULL;
        }
        
        if (empty($cache_item) == FALSE) {
            $content_data = $cache_item->getPageSource();
            $content_origin = 'Cache';
        }
        else {
            // Get page source directly from URL.
            $resp = $web_client->get($external_url);
            if ($resp->getStatusCode() == 200) {   
                $content_data = $resp->getBody()->getContents();
                
                if ($bypass_cache == FALSE) {
                    // Store response in cache.
                    $cache_item = Entity\WebRequestCacheItem::create([
                        'id' => $url_hash,
                        'label' => $url_hash,
                        'external_url' => $external_url,
                        'page_source' => $content_data,
                    ]);
                    $cache_item->save();
                }
            }                        
        }
        
        return ['data' => $content_data, 'origin' => $content_origin];
    }    
    
    
    public static function getYesNoFromBool($bool_value) {        
        if ($bool_value == TRUE) {
            $res = 'Yes';
        } 
        else {
            $res = 'No';
        }
        
        return $res;
    }    
    
    
    public static function getSecondsFromDays($days) {       
        
        // Convert days to seconds: days x 24 hours, x 60 minutes, x 60 seconds
        return ((($days * 24) * 60) * 60);
        
    }                  
    
    
    public static function sendEmailToTechnicalContact($message_html = NULL) {       
        
        $res = FALSE;
        
        if ($message_html != NULL) {
            MPRCommon::$last_error = $message_html;
        }
        
        // Load email template.
        $contact_email = MPRCommon::getConfigValue('technical_contact_email');
        if (empty($contact_email) == FALSE) {
                                    
            $email = MPREmail::createFromEmailTemplate('technical_incident', [], $contact_email, '', '', []);
            $res = $email->send();            
        }
        
        return $res;
    }        
   
    
  public static function getDebugModeEnabled() {
    $res = MPRCommon::getConfigValue('debug_mode');
    
    if (empty($res) == TRUE) {
        $res = FALSE;
    }
    else {
        $res = boolval($res);
    }
    
    return $res;
  }
    
  
  public static function getDistinctValues($array) {
    $res = [];
    
    foreach ($array as $item) {
        if (in_array($item, $res) == FALSE) {
            array_push($res, $item);
        }
    }
    
    return $res;
  }  
  
  
  public static function isAdminUser() {
    return \Drupal::service('router.admin_context')->isAdminRoute();
  }    
  
  public static function filterHtmlTextFormField(&$form, &$form_state, $field_name) {
    $filtered_html = MPRCommon::filterHtml($form[$field_name]['value']['#value']);
    $form_state->setValue($field_name, $filtered_html);
  }
  
  public static function filterHtml($html) {
    $token_prefixes = [
        'paper',        
        'invitation',
        'review',
        'reviewer',
    ];  
    
    $allowed_html_tags = MPRCommon::getConfigValue('allowed_html_tags');
    if (empty($allowed_html_tags) == FALSE) {
        $allowed_html_tags = MPRCommon::getArrayFromMultiLineValues($allowed_html_tags);
        $allowed_html_tags = MPRCommon::getDistinctValues($allowed_html_tags);
    }
    else {
        $allowed_html_tags = \Drupal\Component\Utility\Xss::getAdminTagList();
    }
    
    $res = $html;

    // Change tokens in links to values that will not be filtered.
    // Example: href="[paper:title] to href="[paper|title]
    foreach ($token_prefixes as $token_prefix) {
        $res = str_replace('href="[' . $token_prefix . ':', 'href="[' . $token_prefix . '|', $res);
    }
    
    // Filter HTML to remove cross-site scripts.
    $res = \Drupal\Component\Utility\Xss::filter($res, $allowed_html_tags);
    
    // Revert tokens in links to original values.
    // Example: href="[paper|title] to href="[paper:title]
    foreach ($token_prefixes as $token_prefix) {
        $res = str_replace('href="[' . $token_prefix . '|', 'href="[' . $token_prefix . ':', $res);
    }        
    
    return $res;
  }      
  
  
  public static function deleteModuleFiles($only_orphaned_files = FALSE) {
    // Delete files used by the module.    
    $file_usage = \Drupal::service('file.usage');
    $file_system = \Drupal::service('file_system');
    foreach (\Drupal\file\Entity\File::loadMultiple() as $file_entity) {      
        
        // Only process files stored in the multi_peer_review directory.
        $file_uri = $file_entity->getFileUri();
        if (strpos($file_uri, '//multi_peer_review/') !== FALSE) {            
            //$file_spec = $file_system->realpath($file_entity->getFileUri());   
            //$file_exists = file_exists($file_spec);

            $list = $file_usage->listUsage($file_entity);          
            if (empty($list) == TRUE) {                   
                // The file is not linked to any entity.
                $file_entity->delete();            
            }
            else {            
                if ($only_orphaned_files == FALSE) {
                    // The file is linked to an entity.
                    // Check usage.
                    $module_count = 0;
                    foreach ($list as $module_type_key => $module_type_value) {
                        $module_count++;
                    }                   

                    // If the file is used only in this module, then the record can be deleted.
                    // Otherwise, the record should simply be unlinked.              
                    if (($module_count == 1) && (array_key_exists('multi_peer_review', $list) == TRUE)) {

                        // Unlink this file.                
                        $file_usage->delete($file_entity, 'multi_peer_review', NULL, NULL, 0);

                        // Delete the file record.
                        $file_entity->delete();                    
                    }
                    else {
                        // Unlink this file.                
                        $file_usage->delete($file_entity, 'multi_peer_review', NULL, NULL, 0);
                    }
                }
            }
        }
    }      
  }
  
    
  /**
   * Appends a line to a debug file. Only used during module development.
   * @param string $line
   *   The line to append to the debug file.
   */      
    public static function debug($line) {
        $file_spec = '';
        if (file_exists('/tmp') == TRUE) {
            $file_spec = '/tmp/mpr_output.txt';
        }
        else {
            if (file_exists('C:\\temp') == TRUE) {
                $file_spec = 'C:\\temp\\mpr_output.txt';
            }
        }
        
        if ($file_spec != '') {
            if ($file_spec != '') {
                file_put_contents($file_spec, date(DATE_ATOM) . "\t" . $line . "\r\n", FILE_APPEND);
            }            
        }
    }

}
