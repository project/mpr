<?php

namespace Drupal\multi_peer_review\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\Entity\EmailTemplate;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class UserReviewController.
 */
class UserReviewController extends UserController {
 
  
  
  /**
   * Loads entities commonly used in this form.
   *
   * @param int &$review_id
   *   The id of the Review that this form is referencing.
   * @param int &$user_id
   *   The id of the User accessing this method.
   * @param object &$review
   *   Review record loaded during the process.
   * @param object &$invitation
   *   Invitation record loaded during the process.
   * @param object &$paper
   *   Paper record loaded during the process.
   * @param object &$reviewer
   *   Reviewer record loaded during the process.
   * 
   * @return bool
   *   True if the loading process was successful.
   */     
  public function loadCommonParameters(&$review_id, &$user_id, &$review, &$invitation, &$paper, &$reviewer) {
    $res = FALSE;
      
    // Determine the requested entity.
    $current_route = \Drupal::routeMatch();   
    $review_id = $current_route->getParameters()->get('review');
    $user_id = $current_route->getParameters()->get('user');
    
    if ((empty($review_id) == FALSE) && (empty($user_id) == FALSE)) {
      $review = \Drupal\multi_peer_review\Entity\Review::load($review_id);
      if (empty($review) == FALSE) {           
        // Check user access.
        if ($review->getOwnerId() == $user_id) {

          $invitation = $review->fabricateAndLoadInvitation();
          if (empty($invitation) == FALSE) {

            $paper = $invitation->fabricateAndLoadPaper();
            $reviewer = $invitation->fabricateAndLoadReviewer();                    
            if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE)) {
              $res = TRUE;
            }

          }

        }

      }
    }
    
    return $res;      
  }
  
    
  /**
   * Submit Review process.
   *
   * @return array
   *   Return form array.
   */
  public function submitReview() {
    
    $loaded = FALSE;
    $form = [];
    
    // Load common parameters.
    $review_id = 0;
    $user_id = 0;
    $review = NULL;
    $invitation = NULL;
    $paper = NULL;
    $reviewer = NULL;
    if ($this->loadCommonParameters($review_id, $user_id, $review, $invitation, $paper, $reviewer) == TRUE) {
        
      // The form is only valid if the Review is Pending.
      if ($review->getStatus() == Review::STATUS_PENDING) {
        $placeholder_replacements = [];
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

        // Build form.            
        $form = $this->entityFormBuilder()->getForm($review , 'submit');    


        $form['content'] = [
          '#type' => 'markup',
          '#markup' => '<p>' . $this->t('Please complete this form to submit your review of %paper-title.', $placeholder_replacements) . '</p>',
          '#weight' => -1,
        ];            


        $loaded = TRUE;
      }        
        
    }
    
    if ($loaded == FALSE) {        
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }


  /**
   * Cancel Review process.
   *
   * @return array
   *   Return form array.
   */
  public function cancelReview() {
    
    $loaded = FALSE;
    $form = [];
    
    
    // Load common parameters.
    $review_id = 0;
    $user_id = 0;
    $review = NULL;
    $invitation = NULL;
    $paper = NULL;
    $reviewer = NULL;
    if ($this->loadCommonParameters($review_id, $user_id, $review, $invitation, $paper, $reviewer) == TRUE) {
        
      // The form is only valid if the Review is Pending.
      if ($review->getStatus() == Review::STATUS_PENDING) {
        $placeholder_replacements = [];
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

        // Build form.            
        $form = $this->entityFormBuilder()->getForm($review , 'cancel');    


        $form['content'] = [
          '#type' => 'markup',
          '#markup' => '<p>' . $this->t('Complete this form and click on the Confirm button to cancel your review of %paper-title.', $placeholder_replacements) . '</p>',
          '#weight' => -1,
        ];            


        $loaded = TRUE;
      }                
        
    }
    
    
    
    if ($loaded == FALSE) {        
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }

  
  /**
   * View Review process.
   *
   * @return array
   *   Return form array.
   */
  public function viewReview() {
    
    $loaded = FALSE;
    $form = [];
    
    // Load common parameters.
    $review_id = 0;
    $user_id = 0;
    $review = NULL;
    $invitation = NULL;
    $paper = NULL;
    $reviewer = NULL;
    if ($this->loadCommonParameters($review_id, $user_id, $review, $invitation, $paper, $reviewer) == TRUE) {

      // The form is only valid if the Review is Submitted.
      if ($review->getStatus() == 'Submitted') {
        $placeholder_replacements = [];
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

        // Build form.            
        $this->getHelper()->insertPaperDetails($form, $paper, FALSE);


        // Submission.
        $form['review_container'] = [
          '#type' => 'details',          
          '#title' => new TranslatableMarkup('Submission Details'),
          '#open' => TRUE,
        ];


        $form['review_container']['header'] = [              
          '#type' => 'markup',
          '#markup' => '<dl>',
        ];
        $detail_fields = [
          'submitted_timestamp' => new TranslatableMarkup('Submitted'), 
          'submit_message' => new TranslatableMarkup('Message'),       
          'email_attachments' => new TranslatableMarkup('Attachments'),       
        ];
        foreach ($detail_fields as $field_name => $field_label) {

          switch ($field_name) {
            case 'submitted_timestamp':
              $row_value = MPRCommon::getFormattedDateText($review->getSubmittedTimestamp());
              break;
            case 'email_attachments':
              $row_value = '';

              $file_links = [];
              foreach ($review->getEmailAttachments() as $fid) {
                $file = \Drupal\file\Entity\File::load($fid);
                if (empty($file) == FALSE) {
                  $file_link = '<a target="_blank" href="' . 
                      file_create_url($file->getFileUri()) . '">' . 
                      htmlentities($file->getFilename()) . ' (' . format_size($file->getSize()) . ')</a>';

                  array_push($file_links, $file_link);
                }
              }

              if (count($file_links) != 0) {
                $row_value = implode('<br />', $file_links);
              }

              break;
            default:
              $row_value = $review->get($field_name)->getValue()[0]['value'];
              break;
          }        



          if (empty($row_value) == FALSE) {
            $form['review_container'][$field_name]['title'] = [              
              '#type' => 'markup',
              '#markup' => '<dt>' . $field_label . '</dt>',            
            ];
            $form['review_container'][$field_name]['content'] = [              
              '#type' => 'markup',
              '#markup' => '<dd>' . $row_value . '</dd>',            
            ];        
          }
        }
        $form['review_container']['footer'] = [              
          '#type' => 'markup',
          '#markup' => '</dl>',
        ];      


        $form['back_link'] = [
          '#type' => 'link',
          '#url' => Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]),
          '#title' => $this->t('Back'),
        ];                


        $loaded = TRUE;
      }        

    }
    
    if ($loaded == FALSE) {        
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }  
  
  
}
