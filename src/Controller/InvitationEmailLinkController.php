<?php

namespace Drupal\multi_peer_review\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class InvitationEmailLinkController.
 */
class InvitationEmailLinkController extends UserController {

    
    
    
  /**
   * Accept invitation process.
   *
   * @return array
   *   Return form array.
   */
  public function acceptInvitation() {
    
    $loaded = FALSE;
    $form = [];

    
    $email_hash = \Drupal::request()->query->get('eml');
    if (MPRCommon::isValidEmailHash($email_hash) == TRUE) {
      
      $invitations = \Drupal\multi_peer_review\Entity\Invitation::getInvitations(NULL, $email_hash);
      if (count($invitations) == 1) {
        $invitation = current($invitations);
        $paper = $invitation->fabricateAndLoadPaper();
        $reviewer = $invitation->fabricateAndLoadReviewer();

        // The form is only valid if the required related entities exist and the invitation is Pending.
        if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE) && ($invitation->getStatus() == Invitation::STATUS_PENDING)) {
          $placeholder_replacements = [];
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

          // Build form.            
          $form = $this->entityFormBuilder()->getForm($invitation, 'accept');


          $form['content'] = [
              '#type' => 'markup',
              '#markup' => '<p>' . $this->t('Please confirm your committment to review %paper-title by submitting this form.', $placeholder_replacements) . '</p>',
              '#weight' => -1,
          ];            


          $loaded = TRUE;
        }
      }

    }
    
    
    if ($loaded == FALSE) {
      $this->insertInvalidEmailLinkMessage($form);
    }
    
      
    return $form;
  }

  
  
    
  /**
   * Decline invitation process.
   *
   * @return array
   *   Return form array.
   */
  public function declineInvitation() {
    
    $loaded = FALSE;
    $form = [];
    
    
    $email_hash = \Drupal::request()->query->get('eml');
    if (MPRCommon::isValidEmailHash($email_hash) == TRUE) {
      
      $invitations = \Drupal\multi_peer_review\Entity\Invitation::getInvitations(NULL, $email_hash);
      if (count($invitations) == 1) {
        $invitation = current($invitations);
        $paper = $invitation->fabricateAndLoadPaper();
        $reviewer = $invitation->fabricateAndLoadReviewer();

        // The form is only valid if the required related entities exist and the invitation is Pending.
        if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE) && ($invitation->getStatus() == Invitation::STATUS_PENDING)) {
          $placeholder_replacements = [];
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

          // Build form.            
          $form = $this->entityFormBuilder()->getForm($invitation, 'decline');


          $form['content'] = [
              '#type' => 'markup',
              '#markup' => '<p>' . $this->t('Please confirm that you are declining the invitation to review %paper-title by submitting this form.', $placeholder_replacements) . '</p>
                  <p>' . $this->t('Note: All fields are optional.') . '</p>',
              '#weight' => -1,
          ];            


          $loaded = TRUE;
        }
      }
        
    }
    
    
    if ($loaded == FALSE) {
      $this->insertInvalidEmailLinkMessage($form);
    }
    
      
    return $form;
  }
  
  
  
  
}
