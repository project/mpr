<?php


namespace Drupal\multi_peer_review\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRFormHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserController.
 */
abstract class UserController extends ControllerBase {
    
    
  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;
  
    
  /**
   * The form helper.
   *
   * @var \Drupal\multi_peer_review\MPRFormHelper
   */
  private $helper;
 
  
  /**
   * Constructs an UserController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  
  
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }      
  
  
  /**
   * Returns the Multi Peer Review form helper instance for this object.
   * The form helper will be initialised the first time this method is called.
   * 
   * @return \Drupal\multi_peer_review\MPRFormHelper
   *   The instance of this object's helper.
   */    
  public function getHelper() {
    if (empty($this->helper) == TRUE) {
      $this->helper = new MPRFormHelper();
      $this->helper->initialise($this);
    }
    
    return $this->helper;
  }    
  
  
  /**
   * Adds a message to a given form stating that the email link used to access the page is invalid.
   *
   * @param array &$form
   *   The message will be added to this form.
   */   
  public function insertInvalidEmailLinkMessage(&$form) {
    $this->getHelper()->insertInvalidEmailLinkMessage($form);   
  }
  
  /**
   * Adds a message to a given form stating that the link used to access the page is invalid.
   *
   * @param array &$form
   *   The message will be added to this form.
   */    
  public function insertInvalidLinkMessage(&$form) {
    $this->getHelper()->insertInvalidLinkMessage($form);
  }    
    
  
}
