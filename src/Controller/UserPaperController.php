<?php

namespace Drupal\multi_peer_review\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Class UserPaperController.
 */
class UserPaperController extends UserController {

    
  /**
   * Loads entities commonly used in this form.
   *
   * @param int &$paper_id
   *   The id of the Paper that this form is referencing.
   * @param int &$user_id
   *   The id of the User accessing this method.
   * @param object &$paper
   *   Paper record loaded during the process.
   * 
   * @return bool
   *   True if the loading process was successful.
   */     
  public function loadCommonParameters(&$paper_id, &$user_id, &$paper) {
    $res = FALSE;
      
    // Determine the requested entity.
    $current_route = \Drupal::routeMatch();   
    $paper_id = $current_route->getParameters()->get('paper');
    $user_id = $current_route->getParameters()->get('user');
    
    if ((empty($paper_id) == FALSE) && (empty($user_id) == FALSE)) {
        
      // We do not attempt to restrict the Paper from the user.

      $paper = Paper::load($paper_id);
      if (empty($paper) == FALSE) {
          $res = TRUE;
      }

    }
    
    return $res;      
  }
  
    
  /**
   * View Paper process.
   *
   * @return array
   *   Return form array.
   */
  public function viewPaper() {
    
    $loaded = FALSE;
    $form = [];
    $show_paper_download_link = TRUE;
        
    // Load common parameters.
    $paper_ud = 0;
    $user_id = 0;    
    $paper = NULL;    
    if ($this->loadCommonParameters($paper_id, $user_id, $paper) == TRUE) {

      $placeholder_replacements = [];        
      MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);        

      // Configure back link based on access.
      if (AccessResult::allowedIfHasPermission($this->currentUser(), 'access multi_peer_review_manage_own_reviews')->isAllowed() == TRUE) {
        $destination_url = Url::fromRoute('multi_peer_review.account.reviews', ['user' => $user_id]);

        // The user is a Reviewer and should not be allowed to
        // see the Paper file until they accept to review it.
        $show_paper_download_link = FALSE;

        // Check Invitations to determine if this user has accepted one for this Paper.
        $reviewer = Reviewer::getReviewerByOwner($user_id);
        if (empty($reviewer) == FALSE) {
          $invitations = Invitation::getInvitations(FALSE, NULL, NULL, $paper_id, $reviewer->id());
          foreach ($invitations as $invitation) {
            if ($invitation->getStatus() == Invitation::STATUS_ACCEPTED) {
              $show_paper_download_link = TRUE;
              break;
            }
          }
        }

      }
      else {
        if (AccessResult::allowedIfHasPermission($this->currentUser(), 'access multi_peer_review_manage_own_papers')->isAllowed() == TRUE) {
          $destination_url = Url::fromRoute('multi_peer_review.account.papers', ['user' => $user_id]);
        }
        else {
          $destination_url = NULL;
        }
      }


      // Build form.            
      $this->getHelper()->insertPaperDetails($form, $paper, TRUE, $show_paper_download_link );


      if (empty($destination_url) == FALSE) {
        $form['back_link'] = [
          '#type' => 'link',
          '#url' => $destination_url,
          '#title' => $this->t('Back'),
        ];            
      }


      $loaded = TRUE;      

    }
    
    if ($loaded == FALSE) {
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }



  /**
   * Delete Paper process.
   *
   * @return array
   *   Return form array.
   */
  public function deletePaper() {
    
    $loaded = FALSE;
    $form = [];
    
    // Load common parameters.
    $paper_ud = 0;
    $user_id = 0;    
    $paper = NULL;    
    if ($this->loadCommonParameters($paper_id, $user_id, $paper) == TRUE) {
      if ($paper->getStatus() == Paper::STATUS_IDLE) {
        // Build form.            
        $form = $this->entityFormBuilder()->getForm($paper, 'delete');


        $loaded = TRUE;              
      }        
    }    
    
    if ($loaded == FALSE) {
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }  
  
  
  
}
