<?php

namespace Drupal\multi_peer_review\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Class UserInvitationController.
 */
class UserInvitationController extends UserController {

    
  /**
   * Loads entities commonly used in this form.
   *
   * @param int &$invitation_id
   *   The id of the Invitation that this form is referencing.
   * @param int &$user_id
   *   The id of the User accessing this method.
   * @param object &$invitation
   *   Invitation record loaded during the process.
   * @param object &$reviewer
   *   Reviewer record loaded during the process.
   * @param object &$paper
   *   Paper record loaded during the process.
   * 
   * @return bool
   *   True if the loading process was successful.
   */   
  public function loadCommonParameters(&$invitation_id, &$user_id, &$invitation, &$reviewer, &$paper) {
    $res = FALSE;
      
    // Determine the requested entity.
    $current_route = \Drupal::routeMatch();   
    $invitation_id = $current_route->getParameters()->get('invitation');
    $user_id = $current_route->getParameters()->get('user');
    
    if ((empty($invitation_id) == FALSE) && (empty($user_id) == FALSE)) {
        
        // Find related Reviewer.
      $reviewer = Reviewer::getReviewerByOwner($user_id);        
      if (empty($reviewer) == FALSE) {      

        $invitation = \Drupal\multi_peer_review\Entity\Invitation::load($invitation_id);
        if (empty($invitation) == FALSE) {           
          // Check if the Invitation is related to the Reviewer.
          if ($invitation->getReviewer() == $reviewer->id()) {

            $paper = $invitation->fabricateAndLoadPaper();
            if (empty($paper) == FALSE) {                    
              $res = TRUE;
            }

          }

        }

      }
    }
    
    return $res;      
  }  
  
    
  /**
   * Accept invitation process.
   *
   * @return array
   *   Return form array.
   */
  public function acceptInvitation() {
    
    $loaded = FALSE;
    $form = [];
    
    
    // Load common parameters.
    $invitation_id = 0;
    $user_id = 0;
    $invitation = NULL;
    $reviewer = NULL;
    $paper = NULL;
    if ($this->loadCommonParameters($invitation_id, $user_id, $invitation, $reviewer, $paper) == TRUE) {
        
      // The form is only valid if the Invitation is Pending.
      if ($invitation->getStatus() == Invitation::STATUS_PENDING) {
        $placeholder_replacements = [];
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

        // Build form.            
        $form = $this->entityFormBuilder()->getForm($invitation, 'accept');


        $form['content'] = [
          '#type' => 'markup',
          '#markup' => '<p>' . $this->t('Please confirm your committment to review %paper-title by submitting this form.', $placeholder_replacements) . '</p>',
          '#weight' => -1,
        ];            


        $loaded = TRUE;
      }        
        
    }
    
   
    
    if ($loaded == FALSE) {
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }

  
  
    
  /**
   * Decline invitation process.
   *
   * @return array
   *   Return form array.
   */
  public function declineInvitation() {
    
    $loaded = FALSE;
    $form = [];
    
    
    // Load common parameters.
    $invitation_id = 0;
    $user_id = 0;
    $invitation = NULL;
    $reviewer = NULL;
    $paper = NULL;
    if ($this->loadCommonParameters($invitation_id, $user_id, $invitation, $reviewer, $paper) == TRUE) {
        
      // The form is only valid if the Invitation is Pending.
      if ($invitation->getStatus() == Invitation::STATUS_PENDING) {
        $placeholder_replacements = [];
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

        // Build form.            
        $form = $this->entityFormBuilder()->getForm($invitation, 'decline');


        $form['content'] = [
          '#type' => 'markup',
          '#markup' => '<p>' . $this->t('Please confirm that you are declining the invitation to review %paper-title by submitting this form.', $placeholder_replacements) . '</p>
              <p>' . $this->t('Note: All fields are optional.') . '</p>',
          '#weight' => -1,
        ];            


        $loaded = TRUE;
      }        
        
        
    }
    
    
    if ($loaded == FALSE) {
      $this->insertInvalidLinkMessage($form);
    }
    
      
    return $form;
  }
  
  
  
  
}
