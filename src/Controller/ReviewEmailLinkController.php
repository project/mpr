<?php

namespace Drupal\multi_peer_review\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\Entity\EmailTemplate;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Class ReviewEmailLinkController.
 */
class ReviewEmailLinkController extends UserController {

          
    
  /**
   * Submit Review process.
   *
   * @return array
   *   Return form array.
   */
  public function submitReview() {
    
    $loaded = FALSE;
    $form = [];
    
    
    $email_hash = \Drupal::request()->query->get('eml');
    if (MPRCommon::isValidEmailHash($email_hash) == TRUE) {
        
      $reviews = \Drupal\multi_peer_review\Entity\Review::getReviews(NULL, $email_hash);
      if (count($reviews) == 1) {
        $review = current($reviews);
        $invitation = $review->fabricateAndLoadInvitation();
        $paper = $invitation->fabricateAndLoadPaper();
        $reviewer = $invitation->fabricateAndLoadReviewer();

        // The form is only valid if the required related entities exist and the invitation is Pending.
        if ((empty($review) == FALSE) && (empty($invitation) == FALSE) && (empty($paper) == FALSE) && (empty($reviewer) == FALSE) && ($review->getStatus() == Review::STATUS_PENDING)) {
          $placeholder_replacements = [];
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

          // Build form.            
          $form = $this->entityFormBuilder()->getForm($review, 'submit');


          $form['content'] = [
            '#type' => 'markup',
            '#markup' => '<p>' . $this->t('To submit your review of %paper-title, please attach the review document to this form and submit it.', $placeholder_replacements) . '</p>',
            '#weight' => -1,
          ];            


          $loaded = TRUE;
        }
      }
        
    }
    
    
    if ($loaded == FALSE) {
      $this->insertInvalidEmailLinkMessage($form);
    }
    
      
    return $form;
  }

  
  

  /**
   * Cancel Review process.
   *
   * @return array
   *   Return form array.
   */
  public function cancelReview() {
    
    $loaded = FALSE;
    $form = [];
    
    
    $email_hash = \Drupal::request()->query->get('eml');
    if (MPRCommon::isValidEmailHash($email_hash) == TRUE) {
        
      $reviews = \Drupal\multi_peer_review\Entity\Review::getReviews(NULL, $email_hash);
      if (count($reviews) == 1) {
        $review = current($reviews);
        $invitation = $review->fabricateAndLoadInvitation();
        $paper = $invitation->fabricateAndLoadPaper();
        $reviewer = $invitation->fabricateAndLoadReviewer();

        // The form is only valid if the required related entities exist and the invitation is Pending.
        if ((empty($review) == FALSE) && (empty($invitation) == FALSE) && (empty($paper) == FALSE) && (empty($reviewer) == FALSE) && ($review->getStatus() == 'Pending')) {
          $placeholder_replacements = [];
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $review);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $invitation);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper);
          MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer);                  

          // Build form.            
          $form = $this->entityFormBuilder()->getForm($review, 'cancel');


          $form['content'] = [
            '#type' => 'markup',
            '#markup' => '<p>' . $this->t('To submit your review of %paper-title, please attach the review document to this form and submit it.', $placeholder_replacements) . '</p>',
            '#weight' => -1,
          ];            


          $loaded = TRUE;
        }
      }
        
    }
    
    
    if ($loaded == FALSE) {
      $this->insertInvalidEmailLinkMessage($form);
    }
    
      
    return $form;
  }  
  
}
