<?php

namespace Drupal\multi_peer_review;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\multi_peer_review\MPRCommon;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\multi_peer_review\FilteredEntityListBuilder;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;


/**
 * Defines a class to build a listing of Reviewer entities.
 *
 * @see \Drupal\multi_peer_review\Entity\Reviewer
 */
class ReviewerListBuilder extends FilteredEntityListBuilder {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RendererInterface $renderer) {
    parent::__construct($entity_type, $storage);

    $this->renderer = $renderer;        
    
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('renderer')
    );
  }
  
  
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQueryConditions(&$query) {
    // Filter by selected Paper.
    $paper_id = \Drupal::request()->request->get('paper');        
    if ((empty($paper_id) == FALSE) && ($paper_id != '_none')) {

        $paper = Paper::load($paper_id);
        if (empty($paper) == FALSE) {

            $subjects = MPRCommon::getArrayFromMultiLineValues($paper->getSubjects());                
            if (count($subjects) != 0) {
                // Create an orConditionGroup.
                $orGroup = $query->orConditionGroup();                    
                foreach ($subjects as $subject) {                        
                    $orGroup->condition('subjects__value', '%' . \Drupal::database()->escapeLike($subject) . '%', 'LIKE');
                }
                // Add the group to the query.
                $query->condition($orGroup);                      
            }

        }            
    }


    // Filter by data source.
    $data_source = \Drupal::request()->request->get('data_source');        
    if ((empty($data_source) == FALSE) && ($data_source != '_none')) {
        $query->condition('data_source', $data_source, '=');      
    }
    
    
    // Filter by panel.
    $panel = \Drupal::request()->request->get('panel');        
    if ((empty($panel) == FALSE) && ($panel != '_none')) {
        
        // Create an orConditionGroup.
        $orGroup = $query->orConditionGroup();           
        
        $orGroup->condition('panel', $panel, '=');
        $orGroup->condition('panel', '_none', '=');
        
        // Add the group to the query.
        $query->condition($orGroup);     
    }    


    // Filter by keywords.
    $this->applyKeywordFilter($query, [
        'first_name', 
        'last_name', 
        'email', 
        'internal_comments__value', 
        'biography__value', 
        'subjects__value', 
        'affiliations__value', 
        'id_orcid', 
        'id_isni', 
        'id_researcherid',
        ]);     
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function applyListItemQuerySort(&$query) {
    $query->sort('first_name');
  }
  
  
  /**
   * {@inheritdoc}
   */   
  protected function getFilterFormFields() {
    
    $form = [];
    
    // Prepare Paper options. Do not include reviewed Papers.
    $options = [];
    $options['_none'] = $this->t('- None -');
    foreach (Paper::getPapers(FALSE, NULL, Paper::STATUS_IDLE) as $paper) {
        $options[$paper->id()] = $paper->label();
    }    
    foreach (Paper::getPapers(FALSE, NULL, Paper::STATUS_SEEKING_REVIEWERS) as $paper) {
        $options[$paper->id()] = $paper->label();
    }      
    // Paper's that are being reviewed as included in case additional Reviewers are required.
    foreach (Paper::getPapers(FALSE, NULL, Paper::STATUS_REVIEW_PENDING) as $paper) {
        $options[$paper->id()] = $paper->label();
    }       
    $form['paper'] = MPRCommon::getDefaultDropDownFormField('Suitable for Paper', '', FALSE, '', $options);
    
    
    // Prepare data source options.
    $options = [];
    $options['_none'] = $this->t('- All -');
    $options['Manual'] = 'Manual';
    $options['apo'] = 'APO';
    $form['data_source'] = MPRCommon::getDefaultDropDownFormField('Data Source', '', FALSE, '', $options);    
    
    
    // Prepare panel options.
    $options = MPRCommon::getTranslatedListOptionsFromConfig('panels');
    $options['_none'] = $this->t('- All -');
    $form['panel'] = MPRCommon::getDefaultDropDownFormField('Panel', '', FALSE, '', $options);    
    
    
    $form['keywords'] = MPRCommon::getDefaultSingleLineTextFormField('Keywords', '', FALSE, '');
 
    return $form;      
  }
      
  

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $res = parent::getDefaultOperations($entity);
    
    
    // Shortcut to invite Reviewer.
    if ($this->checkAccess('administer multi_peer_review_manage_all_invitations') == TRUE) {
        $res['invite'] = [
            'title' => t('Invite'),
            'weight' => 11,
            'url' => $this->ensureDestination(Url::fromRoute('entity.invitation.add_form', [], ['query' => ['reviewer' => $entity->id()]])),               
        ];
    }
    
    // If the current owner is the same as the original owner, assume that
    // the Reviewer has not been assigned to a user account.
    if (($entity->getOwnerId() == $entity->getOriginalOwner()) && ($this->checkAccess('administer multi_peer_review_users_add') == TRUE)) {
        $res['new_account'] = [
            'title' => t('Create User Account'),
            'weight' => 11,
            'url' => $this->ensureDestination($entity->toUrl('new-account-form')),      
        ];
    }


    return $res;
  }  
  
  

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {    
    
    $header['id'] = $this->t('ID');
    
    $header['name'] = [
            'data' => $this->t('Name'),
            'field' => 'first_name',
            'specifier' => 'first_name',
    ];    
    
    $header['panel'] = [
            'data' => $this->t('Panel'),
            'field' => 'panel',
            'specifier' => 'panel',
    ];      
    
    $header['subjects'] = [
            'data' => $this->t('Subjects'),
            'field' => 'subjects__value',
            'specifier' => 'subjects__value',
    ];       
    
    $header['average_review_time'] = [
            'data' => $this->t('Average Review Time'),
            'field' => 'cached_average_review_time',
            'specifier' => 'cached_average_review_time',
    ];      
    
    $header['pending_invitations'] = [
            'data' => $this->t('Pending Invitations'),
            'field' => 'cached_pending_invitations',
            'specifier' => 'cached_pending_invitations',
    ];     
    
    $header['pending_reviews'] = [
            'data' => $this->t('Pending Reviews'),
            'field' => 'cached_pending_reviews',
            'specifier' => 'cached_pending_reviews',
    ];  

    $header['completed_reviews'] = [
            'data' => $this->t('Completed Reviews'),
            'field' => 'cached_completed_reviews',
            'specifier' => 'cached_completed_reviews',
    ];      
    
    $header['recent_invitation_acceptance_rate'] = [
            'data' => $this->t('Recent Invitation Acceptance Rate'),
            'field' => 'cached_recent_invitation_acceptance_rate',
            'specifier' => 'cached_recent_invitation_acceptance_rate',
    ];      
                            
    $header['changed'] = [
            'data' => $this->t('Modified'),
            'field' => 'changed',
            'specifier' => 'changed',
    ];   

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\multi_peer_review\ReviewerInterface $entity */
    
    $row['id'] = $entity->id();
    $row['name'] = $entity->getFullName();
    $row['panel'] = $this->t($entity->getPanelListingText());
    $row['subjects'] = $entity->getSubjectsCsv();
    $row['average_review_time'] = $entity->getCachedAverageReviewTimeText();
    $row['pending_invitations'] = $entity->getCachedPendingInvitations();
    $row['pending_reviews'] = $entity->getCachedPendingReviews();
    $row['completed_reviews'] = $entity->getCachedCompletedReviews();
    $row['recent_invitation_acceptance_rate'] = $entity->getCachedRecentInvitationAcceptanceRateText();
    $row['changed'] = MPRCommon::getFormattedDateText($entity->getChangedTime());

    return $row + parent::buildRow($entity);
  }


}
