<?php

namespace Drupal\multi_peer_review\Plugin\Mail;


/**
 * Defines the default Multi Peer Review mail back-end, using PHP's native mail() function.
 *
 * @Mail(
 *   id = "multi_peer_review_html_enabled_php_mail",
 *   label = @Translation("Default Multi Peer Review PHP mailer"),
 *   description = @Translation("Sends the message as HTML, using PHP's native mail() function, but without converting it to plain-text.")
 * )
 */
class HtmlEnabledPhpMail extends \Drupal\Core\Mail\Plugin\Mail\PhpMail {

  
  /**
   * Concatenates and wraps the email body for plain-text mails.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Join the body array into one string.
    $message['body'] = implode("\n\n", $message['body']);
    
    // Convert any HTML to plain-text.
    //$message['body'] = MailFormatHelper::htmlToText($message['body']);
    // Wrap the mail body for sending.
    //$message['body'] = MailFormatHelper::wrapMail($message['body']);

    return $message;
  }



}
