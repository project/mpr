<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines an interface for the EmailTemplate entity type.
 */
interface EmailTemplateInterface extends ConfigEntityInterface {

  /**
   * Gets staff notes for the Email Template.
   *
   * @return string
   *   Email template staff notes.
   */
  public function getStaffNotes();         
    
    
  /**
   * Gets the Email Template subject.
   *
   * @return string
   *   Email template subject line.
   */
  public function getSubject();        
    
  /**
   * Gets the Email Template body.
   *
   * @return string
   *   Email template body content in HTML format.
   */
  public function getBody();    

}
