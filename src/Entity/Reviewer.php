<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\multi_peer_review\Entity\ReviewerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRDropDownListItem;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * The Reviewer entity class.
 *
 * @ContentEntityType(
 *   id = "reviewer",
 *   label = @Translation("Reviewer"),
 *   label_collection = @Translation("Reviewers"),
 *   label_singular = @Translation("reviewer"),
 *   label_plural = @Translation("reviewers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count reviewer",
 *     plural = "@count reviewers"
 *   ),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\ReviewerListBuilder",
 *     "access" = "Drupal\multi_peer_review\ReviewerAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\multi_peer_review\ReviewerHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\ReviewerForm",
 *       "add" = "\Drupal\multi_peer_review\Form\ReviewerForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\ReviewerForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\ReviewerDeleteForm",
 *       "activate" = "\Drupal\multi_peer_review\Form\ReviewerActivateForm",
 *       "deploy" = "\Drupal\multi_peer_review\Form\ReviewerDeployForm",
 *       "new-account" = "\Drupal\multi_peer_review\Form\ReviewerNewAccountForm",
 *     },
 *   },
 *   admin_permission = "administer multi_peer_review_manage_all_reviewers",
 *   base_table = "reviewer",
 *   revision_table = "reviewer_revision",
 *   data_table = "reviewer_field_data",
 *   revision_data_table = "reviewer_field_revision",
 *   field_ui_base_route = "entity.reviewer.collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/multi-peer-review/reviewers/add",
 *     "edit-form" = "/admin/multi-peer-review/reviewers/manage/{reviewer}/edit",
 *     "delete-form" = "/admin/multi-peer-review/reviewers/manage/{reviewer}/delete",
 *     "activate-form" = "/admin/multi-peer-review/reviewers/manage/{reviewer}/activate",
 *     "deploy-form" = "/admin/multi-peer-review/reviewers/manage/{reviewer}/deploy",
 *     "new-account-form" = "/admin/multi-peer-review/reviewers/manage/{reviewer}/new-account",
 *     "collection" = "/admin/multi-peer-review/reviewers",
 *   },
 * )
 */
class Reviewer extends CommonContentEntity implements ReviewerInterface, MPRDropDownListItem {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  

  protected $title;
  protected $first_name; 
  protected $last_name;
  protected $email;
  protected $phone; 
  protected $panel; 
  protected $internal_comments; 
  protected $biography; 
  protected $subjects; 
  protected $affiliations; 
  protected $data_source;
  protected $id_orcid; 
  protected $id_isni; 
  protected $id_researcherid;     
  protected $source_search_result_item;
  protected $original_owner;

 
  
  protected $cached_average_review_time;
  protected $cached_pending_invitations;
  protected $cached_pending_reviews;
  protected $cached_completed_reviews;
  protected $cached_recent_invitation_acceptance_rate;  
    

  

  public function getTitle() {
    return $this->title;
  }   
  
  public function getFirstName() {
    return $this->first_name;
  }    

  public function getLastName() {
    return $this->last_name;
  }   
  
  public function getFullName() {
    return trim($this->first_name . ' ' . $this->last_name);
  }   

  public function getTitleAndFullName() {
    $parts = [];
    if (empty($this->title) == FALSE) {
        array_push($parts, trim($this->title));
    }
    if (empty($this->first_name) == FALSE) {
        array_push($parts, trim($this->first_name));
    }
        if (empty($this->last_name) == FALSE) {
        array_push($parts, trim($this->last_name));
    }
    return implode(' ', $parts);
  }    
  
  public function getEmail() {
    return $this->email;
  }     
  
  public function getPhone() {
    return $this->phone;
  }     
  
  public function getPanel() {
    return $this->panel;
  }     
  
  public function getDataSource() {
    return $this->data_source;
  }    
  
  public function getPanelListingText() {
    $res = $this->getPanel();
    if ($res == '_none') {
        $res = 'All';
    }
    return ($res);
  }     
  
  public function getInternalComments() {
    return $this->internal_comments['value'];
  }     
  
  public function getBiography() {
    return $this->biography['value'];
  }     
  
  public function getSubjects() {
    return $this->subjects['value'];
  }    
  
  public function getSubjectsCsv() {
    return MPRCommon::getCsvFromMultiLineValues($this->getSubjects()); 
  }   
  
  public function getAffiliations() {
    return $this->affiliations['value'];
  }     
  

  public function getORCID() {
    return $this->id_orcid;
  }    
    
  public function getISNI() {
    return $this->id_isni;
  }     
  
  public function getResearcherID() {
    return $this->id_researcherid;
  }     
  
  public function getSourceSearchResultItem() {
    return $this->source_search_result_item;
  }

  
  public function getOriginalOwner() {
    return $this->original_owner;
  }       
  
  
  public function getCachedAverageReviewTime() {
    $res = $this->cached_average_review_time;
    if (empty($res) == TRUE) {
        $res = 0;
    }
    return $res;
  }

  public function getCachedAverageReviewTimeText() {
    $res = $this->getCachedAverageReviewTime();
    if ($res == 0) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    return $res;
  }  
  
  public function getCachedPendingInvitations() {
    $res = $this->cached_pending_invitations;
    if (empty($res) == TRUE) {
        $res = 0;
    }
    return $res;
  }

  public function getCachedPendingReviews() {
    $res = $this->cached_pending_reviews;
    if (empty($res) == TRUE) {
        $res = 0;
    }
    return $res;
  }

  public function getCachedCompletedReviews() {
    $res = $this->cached_completed_reviews;
    if (empty($res) == TRUE) {
        $res = 0;
    }
    return $res;
  }

  public function getCachedRecentInvitationAcceptanceRate() {
    $res = $this->cached_recent_invitation_acceptance_rate;
    if (empty($res) == TRUE) {
        $res = 0;
    }
    return $res;
  }  
  
  public function getCachedRecentInvitationAcceptanceRateText() {
    $res = $this->getCachedRecentInvitationAcceptanceRate();
    if ($res == -1) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = strval($res) . '%';
    }
    return $res;
  }    
  
  
  /**
   * {@inheritdoc}
   */
  public function getListItemText() {
      return $this->getFullName();
  }
  
  /**
   * {@inheritdoc}
   */
  public function getListItemValue() {
      return $this->id();
  }
  
  
  
  /**
   * {@inheritdoc}
   */
  public function getTranslatedDescriptiveLabel() {
    return new TranslatableMarkup($this->getFullName());
  }    
  
  
  /**
   * {@inheritdoc}
   */    
  public function updateCachedSearchMetaData() {    
    // Reviewers do not have any search meta data.
  }  
  

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    
    $fields['id']->setDescription(new TranslatableMarkup('The Reviewer ID.'));

    $fields['title'] = MPRCommon::getDefaultSingleLineTextDbField('Title', 
            'Reviewer’s title.', FALSE);        
    
    $fields['first_name'] = MPRCommon::getDefaultSingleLineTextDbField('First Name', 
            'Reviewer’s first name.', TRUE);
    
    $fields['last_name'] = MPRCommon::getDefaultSingleLineTextDbField('Last Name', 
            'Reviewer’s last name.', TRUE);    
       
    $fields['email'] = MPRCommon::getDefaultSingleEmailAddressDbField('Email', 
            'Reviewer’s email.', TRUE);        
    
    $fields['phone'] = MPRCommon::getDefaultSingleLineTextDbField('Phone',
            'Reviewer’s contact number. It may be a landline or mobile number.', FALSE);            
    
    $fields['data_source'] = MPRCommon::getDefaultSingleLineTextDbField('Data Source',
            'Name of the search engine script that found the Reviewer.', FALSE);                
    
    $fields['panel'] = MPRCommon::getDefaultSingleLineTextDbField('Panel', 
            'The Panel that the Reviewer is assigned to (if any).', FALSE);

    $fields['subjects'] = MPRCommon::getDefaultMultiLineTextDbField('Subjects',
            'The subjects that the Reviewer specialises in. Each subject must be entered on a single line.', FALSE);    
    
    $fields['affiliations'] = MPRCommon::getDefaultMultiLineTextDbField('Affiliations',
            'The Reviewer’s affiliations. Each affiliation must be entered on a single line.', FALSE);        
    
    $fields['internal_comments'] = MPRCommon::getDefaultMultiLineTextDbField('Internal Comments',
            'Comments about the Reviewer by staff. Only other staff can view these comments.', FALSE);       
    
    $fields['biography'] = MPRCommon::getDefaultMultiLineTextDbField('Biography',
            'A brief biography of the Reviewer.', FALSE);       
    
    $fields['id_orcid'] = MPRCommon::getDefaultSingleLineTextDbField('ORCID',
            'The Reviewer’s unique identifier on the ORCID registry (https://orcid.org/).', FALSE);       

    $fields['id_isni'] = MPRCommon::getDefaultSingleLineTextDbField('ISNI',
            'The Reviewer’s unique identifier on the ISNI platform (http://www.isni.org/).', FALSE);   

    $fields['id_researcherid'] = MPRCommon::getDefaultSingleLineTextDbField('ResearcherID',
            'The Reviewer’s unique identifier on the Publons platform (https://publons.com).', FALSE);       
        
    
    $fields['source_search_result_item'] = MPRCommon::getDefaultSingleLineTextDbField('Source Search Result Item', 
            'The ID of the source Search Result Item.', FALSE);      
         

    $fields['original_owner'] = MPRCommon::getDefaultIntegerDbField('Original Owner', 
            'The original owner of this Reviewer.', FALSE);        
    
    // Fields for storing cached figures.
    $fields['cached_average_review_time'] = MPRCommon::getDefaultDecimalDbField('Average Review Time', 
            'Average review time in seconds. May be shown as Months, Days and Hours.', FALSE);    
    
    $fields['cached_pending_invitations'] = MPRCommon::getDefaultIntegerDbField('Pending Invitations', 
            'Number of pending Invitations.', FALSE);    
    
    $fields['cached_pending_reviews'] = MPRCommon::getDefaultIntegerDbField('Pending Reviews', 
            'Number of pending Reviews.', FALSE);    

    $fields['cached_completed_reviews'] = MPRCommon::getDefaultIntegerDbField('Completed Reviews', 
            'Number of completed Reviews.', FALSE);    
    
    $fields['cached_recent_invitation_acceptance_rate'] = MPRCommon::getDefaultDecimalDbField('Recent Invitation Acceptance Rate', 
            'Percentage based on recently Accepted and Declined Invitations.', FALSE);   
        

    return $fields;
  }

  
  
  /**
   * {@inheritdoc}
   */
  public function save() {
      
      // Prepare unique email hash if this is a new record.
      if ($this->isNew() == TRUE) {      
          $this->set('email_hash', MPRCommon::getNewEmailHash($this, $this->label()));
          
          $original_owner = \Drupal::currentUser()->id();
          $this->set('original_owner', $original_owner);
      }          
      
      
      // Set search cache.
      $this->updateCachedSearchMetaData();
      
           
      // Call standard save process.
      $status = parent::save();
      
      
      // Update caches of related entities.
      if ($status == SAVED_UPDATED) {          
          // Update Invitations.
          $invitation_ids = [];
          foreach (Invitation::getInvitations(NULL, NULL, NULL, NULL, $this->id()) as $invitation) {              
            Invitation::rebuildCachedFigures($invitation);                        
            array_push($invitation_ids, $invitation->id());
          }
          
          // Update Reviews.
          foreach (Review::getReviews() as $review) {
              if (in_array($review->getInvitation(), $invitation_ids)) {
                Review::rebuildCachedFigures($review);
              }
          }               
      }      
      
      
      return $status;
  }  
  
  /**
   * {@inheritdoc}
   */
  public function delete() {
      
    // Restore archived Search Result Item, if any.
    $source_id = $this->getSourceSearchResultItem();
    if (empty($source_id) == FALSE) {
        $source_item = SearchResultItem::load($source_id);
        if (empty($source_item) == FALSE) {
            $source_item->set('archived', FALSE);
            $source_item->save();
        }
    }
      
    parent::delete();
  }
  
  public static function getReviewerByOwner($owner_id) {
    $res = NULL;
    
    $reviewers = Reviewer::getReviewers(FALSE, NULL, $owner_id);
    if (empty($reviewers) == FALSE) {
        $res = current($reviewers);
    }
    
    return $res;
  }

  public static function getReviewers($archived = NULL, $email_hash = NULL, $owner_id = NULL) {
    $properties = [];
    
    if ($archived !== NULL) {
        $properties['archived'] = $archived;
    }    
    
    if ($email_hash !== NULL) {
        $properties['email_hash'] = $email_hash;
    }        
    
    if ($owner_id !== NULL) {
        $properties['uid'] = $owner_id;
    }        
      
    if (empty($properties) == FALSE) {          
        $res = \Drupal::entityTypeManager()->getStorage('reviewer')->loadByProperties($properties);     
    }
    else {
        $res = Reviewer::loadMultiple();
    }
        
    if (empty($res) == TRUE) {
        $res = [];
    }        
    
    return $res;  
  }  
    
  public static function rebuildCachedFigures($reviewer_to_update = NULL) {
         
    $invitations = Invitation::getInvitations();
    $reviews = Review::getReviews();
    
    $recent_invitation_range_weeks = MPRCommon::getConfigValue('recent_invitation_range_weeks');
    if ($recent_invitation_range_weeks < 1) {
        $recent_invitation_range_weeks = 1; // Lower limit is 1
    }
    // 7 days * 24 hours * 60 minutes * 60 seconds
    $recent_invitation_range_time = (((($recent_invitation_range_weeks * 7) * 24) * 60) * 60);
    $recent_invitation_time = time() - $recent_invitation_range_time;
    
    if (empty($reviewer_to_update) == TRUE) {
        // Update all reviewers.
        $reviewers = Reviewer::getReviewers();
    }
    else {
        // Update a single reviewer.
        $reviewers = [$reviewer_to_update];
    }
    
    foreach ($reviewers as $reviewer) {
        
        $accepted_invitations = [];        
        $pending_invitations = 0;
        $pending_reviews = 0;
        $recently_accepted_invitations = 0;
        $recently_declined_invitations = 0;
        $recently_retracted_invitations = 0;
        foreach ($invitations as $invitation) {   
            
            if ($invitation->getReviewer() == $reviewer->id()) {                
                switch ($invitation->getStatus()) {
                    case Invitation::STATUS_PENDING:
                        // Count pending invitations for cached_pending_invitations.
                        $pending_invitations++;
                        break;
                    case Invitation::STATUS_ACCEPTED:
                        // Count only "recent" invitations.
                        $action_time = $invitation->get('accepted_timestamp')->getValue();
                        if ($action_time >= $recent_invitation_time) {
                            $recently_accepted_invitations++;
                        }
                        
                        // Store this Invitation for use in calculating Review times.
                        $accepted_invitations[$invitation->id()] = $invitation;
                        
                        // Count Accepted Invitations as Pending reviews
                        $pending_reviews++;
                        
                        break;                    
                    case Invitation::STATUS_DECLINED:
                        // Count only "recent" invitations.
                        $action_time = $invitation->get('declined_timestamp')->getValue();
                        if ($action_time >= $recent_invitation_time) {
                            $recently_declined_invitations++;
                        }
                        break;                        
                    case Invitation::STATUS_RETRACTED:
                        // Count only "recent" invitations.
                        $action_time = $invitation->get('retracted_timestamp')->getValue();
                        if ($action_time >= $recent_invitation_time) {
                            $recently_retracted_invitations++;
                        }
                        break;                         
                }                
            }
            
        }
        
        
        // Calculate recent Invitation acceptance rate (cached_recent_invitation_acceptance_rate).
        $all_non_pending_recent_invitations = $recently_accepted_invitations + $recently_declined_invitations + $recently_retracted_invitations;
        if ($all_non_pending_recent_invitations == 0) {
            // Indicate that there are no recent invitations as opposed to 0% acceptance.
            $cached_recent_invitation_acceptance_rate = -1;
        }
        else {
            $cached_recent_invitation_acceptance_rate = round(($recently_accepted_invitations / $all_non_pending_recent_invitations) * 100, 2);
        }
        
        // Calculate average review time (cached_average_review_time) and completed Reviews (cached_completed_reviews).
        $total_review_time_seconds = 0;        
        $completed_reviews = 0;
        foreach ($reviews as $review) {           
            // Only check submitted Reviews.
            if ($review->getStatus() == Review::STATUS_SUBMITTED) {
                if (array_key_exists($review->getInvitation(), $accepted_invitations) == TRUE) {
                    $invitation = $accepted_invitations[$review->getInvitation()];

                    // Get timestamps.                
                    $submitted_timestamp = intval($review->get('submitted_timestamp')->getValue());
                    $accepted_timestamp = intval($invitation->get('accepted_timestamp')->getValue());                    
                    $review_time_seconds = $submitted_timestamp - $accepted_timestamp;                

                    $total_review_time_seconds += $review_time_seconds;
                    $completed_reviews++;

                    // Since these are completed Reviews, we remove them from the pending review tally.
                    // This figure will be used to calculate pending reviews (cached_pending_reviews).
                    $pending_reviews--;
                }
            }
        }
        
        if ($completed_reviews == 0) {
            // No completed reviews, so no average review time is possible.
            $cached_average_review_time = 0; 
        }
        else {
            $cached_average_review_time = round($total_review_time_seconds / $completed_reviews);
        }
        
        
        
        // Set values if necessary.
        $save_required = FALSE;
        if ($reviewer->get('cached_average_review_time')->getValue() != $cached_average_review_time) {
            $reviewer->set('cached_average_review_time', $cached_average_review_time);            
            $save_required = TRUE;
        }
        if ($reviewer->get('cached_pending_invitations')->getValue() != $pending_invitations) {
            $reviewer->set('cached_pending_invitations', $pending_invitations);            
            $save_required = TRUE;
        }
        if ($reviewer->get('cached_pending_reviews')->getValue() != $pending_reviews) {
            $reviewer->set('cached_pending_reviews', $pending_reviews);            
            $save_required = TRUE;
        }
        if ($reviewer->get('cached_completed_reviews')->getValue() != $completed_reviews) {
            $reviewer->set('cached_completed_reviews', $completed_reviews);            
            $save_required = TRUE;
        }
        if ($reviewer->get('cached_recent_invitation_acceptance_rate')->getValue() != $cached_recent_invitation_acceptance_rate) {
            $reviewer->set('cached_recent_invitation_acceptance_rate', $cached_recent_invitation_acceptance_rate);            
            $save_required = TRUE;
        }
        
        // Commit to database if required.
        if ($save_required == TRUE) {
            $reviewer->save();
        }
        
    }
      
  }
  
  
  
  
  
  /**
   * Returns type and token definitions for this entity.
   * Called by multi_peer_review.token.inc when preparing tokens during hook_token_info.
   * 
   * @return array
   *   Associative array containing types and tokens keys.
   */  
  public static function getTokenInfo() {

    $types = [  
       'reviewer' => [
            'name' => t('Reviewer'),
            'description' => t('Tokens for Reviewer fields.'),
       ],   
    ];

    $tokens = [ 
       'reviewer' => [
           'title-and-full-name' => [
                'name' => t("Title and Full Name"),
                'dynamic' => FALSE,
                'description' => t('Reviewer’s Title and Full Name. e.g. Dr John Citizen.'),               
           ],
           'title' => [
                'name' => t("Title"),
                'dynamic' => FALSE,
                'description' => t('Reviewer’s Title. e.g. Mrs'),    
           ],
           'full-name' => [
                'name' => t("Full Name"),
                'dynamic' => FALSE,
                'description' => t('Reviewer’s Full Name.'),               
           ],           
           'first-name' => [
                'name' => t("First Name"),
                'dynamic' => FALSE,
                'description' => t('Reviewer’s First Name.'),               
           ],
           'last-name' => [
                'name' => t("Last Name"),
                'dynamic' => FALSE,
                'description' => t('Reviewer’s Last Name.'),    
           ],           
           'email' => [
                'name' => t("Email"),
                'dynamic' => FALSE,
                'description' => t('Reviewer’s Email Address.'),    
           ],             
           'owner-display-name' => [
                 'name' => t("Owner Display Name"),
                 'dynamic' => FALSE,
                 'description' => t('The display name of the user who owns the Reviewer. By default it will be their username.'),    
            ],          
           'owner-username' => [
                 'name' => t("Owner Username"),
                 'dynamic' => FALSE,
                 'description' => t('The username of the user who owns the Reviewer.'),    
            ],            
       ],                
    ];


    return ['types' => $types, 'tokens' => $tokens];
  }     
  
  
  /**
   * {@inheritdoc}
   */
  public function getTokenReplacementValue($name) {        
    $res = '';
    switch ($name) {
        case 'title-and-full-name':                   
            $res = $this->getTitleAndFullName();                   
            break;
        case 'title':                                         
            $res = $this->getTitle();
            break;       
        case 'full-name':                                         
            $res = $this->getFullName();
            break;            
        case 'first-name':                                         
            $res = $this->getFirstName();
            break;     
        case 'last-name':                                         
            $res = $this->getLastName();
            break;        
        case 'email':                                         
            $res = $this->getEmail();
            break;   
        case 'owner-display-name':
            $res = $this->getOwner()->getDisplayName();
            break;   
        case 'owner-username':
            $res = $this->getOwner()->getAccountName();
            break;          
    }   
    return ($res);
  }
  
    
  

}
