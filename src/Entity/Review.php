<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Url;
use Drupal\multi_peer_review\Entity\ReviewInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPREmail;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;


/**
 * The Review entity class.
 *
 * @ContentEntityType(
 *   id = "review",
 *   label = @Translation("Review"),
 *   label_collection = @Translation("Reviews"),
 *   label_singular = @Translation("review"),
 *   label_plural = @Translation("reviews"),
 *   label_count = @PluralTranslation(
 *     singular = "@count review",
 *     plural = "@count reviews"
 *   ),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\ReviewListBuilder",
 *     "access" = "Drupal\multi_peer_review\ReviewAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\multi_peer_review\ReviewHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\ReviewForm",
 *       "add" = "\Drupal\multi_peer_review\Form\ReviewForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\ReviewForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\ReviewDeleteForm",
 *       "activate" = "\Drupal\multi_peer_review\Form\ReviewActivateForm",
 *       "deploy" = "\Drupal\multi_peer_review\Form\ReviewDeployForm",
 *       "submit" = "\Drupal\multi_peer_review\Form\ReviewSubmitForm",
 *       "cancel" = "\Drupal\multi_peer_review\Form\ReviewCancelForm",
 *     },
 *   },
 *   admin_permission = "administer multi_peer_review_manage_all_reviews",
 *   base_table = "review",
 *   revision_table = "review_revision",
 *   data_table = "review_field_data",
 *   revision_data_table = "review_field_revision",
 *   field_ui_base_route = "entity.review.collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/multi-peer-review/reviews/add",
 *     "edit-form" = "/admin/multi-peer-review/reviews/manage/{review}/edit",
 *     "delete-form" = "/admin/multi-peer-review/reviews/manage/{review}/delete",
 *     "activate-form" = "/admin/multi-peer-review/reviews/manage/{review}/activate",
 *     "deploy-form" = "/admin/multi-peer-review/reviews/manage/{review}/deploy",
 *     "collection" = "/admin/multi-peer-review/reviews",
 *     "submit-form" = "/admin/multi-peer-review/reviews/manage/{review}/submit",
 *     "cancel-form" = "/admin/multi-peer-review/reviews/manage/{review}/cancel",
 *   },
 * )
 */
class Review extends EmailCapableEntity implements ReviewInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  
  
  const STATUS_PENDING = 'Pending';
  const STATUS_SUBMITTED = 'Submitted';
  const STATUS_CANCELLED = 'Cancelled';  
  

  protected $invitation; 
  protected $status;
  protected $start_date;
  protected $deadline;
  protected $target_date;
  protected $cancel_reason;
  protected $cancel_suggestion;
  protected $submit_message;  
  protected $cached_last_follow_up_timestamp;
  
  protected $submitted_timestamp;

  
  
  
  public function getInvitation() {
    return $this->invitation;
  }     

  
  public function getPaperTitle() {
      $res = '';
      $dat = $this->fabricateAndLoadInvitation();
      if (empty($dat) == FALSE) {             
        $res = $dat->getPaperTitle();
      }
      else {
        $res = new TranslatableMarkup(MPRCommon::ENTITY_ISSUE_BROKEN_RELATIONSHIP);
      }
      return $res;
  }
  
  public function getReviewerName() {
      $res = '';
      $dat = $this->fabricateAndLoadInvitation();
      if (empty($dat) == FALSE) {             
        $res = $dat->getReviewerName();
      }
      else {
        $res = new TranslatableMarkup(MPRCommon::ENTITY_ISSUE_BROKEN_RELATIONSHIP);
      }
      return $res;
  }    
  
  public function getStartDate() {
    return $this->start_date;
  }    
  
  public function getStartDateText() {
    $res = $this->getStartDate();
    if (empty($res) == TRUE) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = MPRCommon::getFormattedDateText($res, MPRCommon::NAMED_DATE_FORMAT_SHORT_DATE);
    }
    return $res;
  }      
  
  public function getDeadline() {
    return $this->deadline;
  }    
  
  public function getDeadlineText() {
    $res = $this->getDeadline();
    if (empty($res) == TRUE) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = MPRCommon::getFormattedDateText($res, MPRCommon::NAMED_DATE_FORMAT_SHORT_DATE);
    }
    return $res;
  }       
  
  public function getOverdue() {
      
    if (empty($this->deadline) == TRUE) {
        $res = FALSE;
    }
    else {
        if (time() > strtotime($this->deadline)) {
            $res = TRUE;
        }
        else {
            $res = FALSE;
        }
    }
    
    return $res;
  }
  
  public function getOverdueText() {
    return MPRCommon::getYesNoFromBool($this->getOverdue());
  }
  
  public function getDeadlineReminderTime($review_upcoming_deadline_reminder_days = NULL) {
      
    $deadline_time = strtotime($this->getDeadline());

    if ($review_upcoming_deadline_reminder_days == NULL) {
        $reminder_days = intval(MPRCommon::getConfigValue('review_upcoming_deadline_reminder_days'));
    }
    else {
        $reminder_days = $review_upcoming_deadline_reminder_days;
    }

    // Convert days to seconds.
    $reminder_seconds = MPRCommon::getSecondsFromDays($reminder_days);

    return $deadline_time - $reminder_seconds;
  }
  
  public function getOverdueReminderTime($review_overdue_reminder_days = NULL) {
          
    $deadline_time = strtotime($this->getDeadline());
    
    if ($review_overdue_reminder_days == NULL) {
        $reminder_days = intval(MPRCommon::getConfigValue('review_overdue_reminder_days'));
    }
    else {
        $reminder_days = $review_overdue_reminder_days;
    }

    // Convert days to seconds.
    $reminder_seconds = MPRCommon::getSecondsFromDays($reminder_days);

    return $deadline_time + $reminder_seconds;
  }  
  
  public function getCalculatedNextFollowUpDate() {
    $res = NULL;      
    $now = time();
    
    if ($this->getStatus() == Review::STATUS_PENDING) {
        // Only Review's that are on Pending status receive automated emails.
        
                
        // If the start date has elapsed, check the next relevant date.        
        $reminder_time = strtotime($this->getStartDate());
        if ($now > $reminder_time) {                
            
            // Obtain the next deadline reminder time.        
            $reminder_time = $this->getDeadlineReminderTime();        

            // If the time has passed, then the next reminder is sent after the deadline.
            if ($now > $reminder_time) {
                
                $reminder_time = $this->getOverdueReminderTime();                
                if ($now > $reminder_time) {

                    // No further reminders can be sent at this stage.
                    $res = NULL;

                }
                else {
                    $res = $reminder_time;
                }
            }   
            else {
                $res = $reminder_time;
            }
        }
        else {
            $res = $reminder_time;
        }
    }
        
    return $res;
  }    
  
  public function getCalculatedNextFollowUpDateText() {
    $res = $this->getCalculatedNextFollowUpDate();
    if ($res == NULL) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = MPRCommon::getFormattedDateText($res, MPRCommon::NAMED_DATE_FORMAT_SHORT_DATE);
    }
    return $res;
  }
  
  public function getCachedLastFollowUpTimestamp() {
    return $this->cached_last_follow_up_timestamp;
  }   
  
  public function getCachedLastFollowUpTimestampText() {
    $res = $this->getCachedLastFollowUpTimestamp();
    if (empty($res) == TRUE) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = MPRCommon::getFormattedDateText(intval($res));
    }
    return $res;
  }      
  

  public function getStatus() {
    $res = $this->status;
    if (empty($res) == TRUE) {
        $res = Review::STATUS_PENDING;
    }
    return $res;
  }     
  
  public static function getStatuses() {    
    return [
        Review::STATUS_PENDING, 
        Review::STATUS_SUBMITTED, 
        Review::STATUS_CANCELLED,
    ];
  }     
  
  public static function getTranslatedStatusListOptions() {
    $res = [];
    foreach (Review::getStatuses() as $status) {
        $res[$status] = new TranslatableMarkup($status);
    }        
    return $res;
  }     
  
  public static function getTranslatedStatuses() {
    $res = [];
    foreach (Review::getStatuses() as $status) {
        array_push($res, new TranslatableMarkup($status));
    }
    return $res;
  }      
  
  public function getSubmittedTimestamp() {
    return $this->submitted_timestamp;
  }
  
  
  public function getCancelReason() {
    return $this->cancel_reason['value'];
  }    

  public function getCancelSuggestion() {
    return $this->cancel_suggestion['value'];
  }    

  public function getSubmitMessage() {
    return $this->submit_message['value'];
  }      

  
  
  public function getSubmitEmailLink() {
    $home = Url::fromRoute('<front>');
    $home->setAbsolute();
    
    $res = $home->toString();
    if (substr($res, strlen($res) - 1) != '/') {
        $res .= '/';
    }            
    $res .= 'multi-peer-review/email-link/submit-review?eml=' . urlencode($this->getEmailHash());           
    
    return $res;
  }  
  
  public function getCancelEmailLink() {
    $home = Url::fromRoute('<front>');
    $home->setAbsolute();
    
    $res = $home->toString();
    if (substr($res, strlen($res) - 1) != '/') {
        $res .= '/';
    }         
    $res .= 'multi-peer-review/email-link/cancel-review?eml=' . urlencode($this->getEmailHash());           
    
    return $res;
  }     
  
  public function fabricateAndLoadInvitation() {
    $res = NULL;
    if (empty($this->invitation) == FALSE) {      
      $res = \Drupal\multi_peer_review\Entity\Invitation::load($this->invitation);                 
    }
    return $res;
  }    
  
  
  /**
   * {@inheritdoc}
   */
  public function getTranslatedDescriptiveLabel() {
    
    $res = new TranslatableMarkup('');
    $invitation = $this->fabricateAndLoadInvitation();
    if (empty($invitation) == FALSE) {
        $paper = $invitation->fabricateAndLoadPaper();  
        $reviewer = $invitation->fabricateAndLoadReviewer();
        if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE)) {
            $placeholder_replacements = [];
            MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $this, '@');        
            MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper, '@');
            MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer, '@');

            $res = new TranslatableMarkup('@paper-title by @reviewer-full-name', $placeholder_replacements);  
        }
    }
      
    return $res;
  }      
  
  
  /**
   * {@inheritdoc}
   */  
  public function updateCachedSearchMetaData() {
    // Set search cache.
    $cache_data = [];
    
    $invitation_id = $this->get('invitation')->value;
    $invitation = Invitation::load($invitation_id);
    if (empty($invitation) == FALSE) {
        $paper = $invitation->fabricateAndLoadPaper();        
        if (empty($paper) == FALSE) {
            array_push($cache_data, $paper->label());
            array_push($cache_data, $paper->getPaperType());
            array_push($cache_data, $paper->getAbstract());
            array_push($cache_data, $paper->getSubjectsCsv());
            
            $this->set('cached_paper_title', $paper->label());
        }
        
        $reviewer = $invitation->fabricateAndLoadReviewer();
        if (empty($reviewer) == FALSE) {
            $reviewer_name = $reviewer->getListItemText();
            array_push($cache_data, $reviewer_name);   
            $this->set('cached_reviewer_name', $reviewer_name);
        }

        $this->setCachedSearchMetaData(implode(' ', $cache_data));        
    }
  }
  

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    
    $fields['id']->setDescription(new TranslatableMarkup('The Review ID.'));

         
    $fields['invitation'] = MPRCommon::getDefaultDropDownDbField('Invitation', 
            'The Invitation that this Review relates to.', TRUE);     
                 

    $fields['start_date'] = MPRCommon::getDefaultDateDbField('Start Date',
            'The Review’s start date.', TRUE);       
    
    $fields['deadline'] = MPRCommon::getDefaultDateDbField('Deadline',
            'The Review deadline.', TRUE);      
    
    $fields['status'] = MPRCommon::getDefaultSingleLineTextDbField('Status', 
            'Status of the Review.', TRUE);   
    
    $fields['cancel_reason'] = MPRCommon::getDefaultMultiLineTextDbField('Cancellation Reason',
            'Reason that this Review was cancelled.', FALSE);       
    
    $fields['cancel_suggestion'] = MPRCommon::getDefaultMultiLineTextDbField('Suggested Reviewer',
            'An alternative Reviewer who was suggested by the Reviewer upon cancelling the Review.', FALSE);
        
    $fields['submit_message'] = MPRCommon::getDefaultMultiLineTextDbField('Message from Reviewer',
            'A message provided by the Reviewer upon submitting the Review.', FALSE);         
    
          
    
    // Fields for storing cached figures and figures used internally.
    $fields['cached_last_follow_up_timestamp'] = MPRCommon::getDefaultTimestampDbField('Last Follow-up Timestamp',
            'The last time a follow-up email was sent to the Reviewer about the Review.', FALSE);    
        
    
    $fields['submitted_timestamp'] = MPRCommon::getDefaultTimestampDbField('Submitted Timestamp',
            'Time when the Review was submitted.', FALSE);       
    
    $fields['cancelled_timestamp'] = MPRCommon::getDefaultTimestampDbField('Cancelled Timestamp',
            'Time when the Review was cancelled.', FALSE);       
      

    return $fields;
  }

  


  /**
   * {@inheritdoc}
   */
  function fabricateAndLoadEntity($id) {
    return Review::load($id);
  }    
  
  
  /**
   * {@inheritdoc}
   */
  function save() {
      
      
      // Set search cache.
      $this->updateCachedSearchMetaData();
      
      
      // Call standard save process.
      $status = parent::save();      
      
      return $status;
  }
  
  
  /**
   * {@inheritdoc}
   */
  function delete() {
    
    // Load related entities.
    $invitation = $this->fabricateAndLoadInvitation();
    if (empty($invitation) == FALSE) {
        $reviewer = $invitation->fabricateAndLoadReviewer();      
    }
    else {
        $reviewer = NULL;
    }
    
    // Process deletion.
    parent::delete();
    
    if (empty($reviewer) == FALSE) {
        // It is possible that the Review is being deleted to correct an error,
        // so we will not change the Invitation record and allow the user to take further action manually.    

        // Rebuild cached figures.
        Reviewer::rebuildCachedFigures($reviewer);
    }
    
  }
  
  public static function sortReviewsByPriority(&$reviews) {
    uasort($reviews, ['self', 'sortReviewsByPriorityComparison']);
  }
  
  private static function getSortPriorityForStatus($status) {    
    switch ($status) {
        case Review::STATUS_PENDING;
            $res = 1;
            break;
        case Review::STATUS_SUBMITTED;
            $res = 2;
            break;        
        default:
            $res = 3;
            break;
    }
    
    return $res;
  }
  
  private static function sortReviewsByPriorityComparison($a, $b) {
          
    $deadline_a = strtotime($a->getDeadline());
    $deadline_b = strtotime($b->getDeadline());    
           
    $priority_a = Review::getSortPriorityForStatus($a->getStatus());
    $priority_b = Review::getSortPriorityForStatus($b->getStatus());   
    
    if ($priority_a == $priority_b) {
        // Compare dates
        $res = 0;
        
        if ($deadline_a == $deadline_b) {
            $res = Review::sortReviewsByStartDateAscComparison($a, $b);
        }
        else {
            if ($deadline_a > $deadline_b) {
                $res = 1;
            }
            else {
                $res = -1;
            }
        }
        
    }
    else {
        if ($priority_a < $priority_b) {
            $res = -1;
        }
        else 
        {
            $res = 1;
        }
    }
        
    
    return $res;
  }  
  
  private static function sortReviewsByStartDateAscComparison($a, $b) {
    $date_a = strtotime($a->getStartDate());
    $date_b = strtotime($b->getStartDate());
    
    if ($date_a == $date_b) {
        $res = 0;
    }
    else {
        if ($date_a > $date_b) {
            $res = 1;
        }
        else {
            $res = -1;
        }
    }
    
    return $res;
  }   
  
  
  public static function getReviews($archived = NULL, $email_hash = NULL, $status = NULL, $invitation_id = NULL, $owner_id = NULL) {
    $properties = [];
    
    if ($archived !== NULL) {
        $properties['archived'] = $archived;
    }
    
    if ($email_hash !== NULL) {
        $properties['email_hash'] = $email_hash;
    }     
    
    if ($status !== NULL) {
        $properties['status'] = $status;
    }    
    
    if ($invitation_id !== NULL) {
        $properties['invitation'] = $invitation_id;
    }       
    
    if ($owner_id !== NULL) {
        $properties['uid'] = $owner_id;
    }          
         
    if (empty($properties) == FALSE) {     
        $res = \Drupal::entityTypeManager()->getStorage('review')->loadByProperties($properties);     
    }
    else {
        $res = Review::loadMultiple();
    }
        
    if (empty($res) == TRUE) {
        $res = [];
    }        
    
    return $res;  
  }   
  

  
  public static function rebuildCachedFigures($review_to_update = NULL) {  
    
    if ($review_to_update == NULL) {
        $reviews = Review::getReviews();
    }
    else {
        $reviews = [$review_to_update];
    }
    
    foreach ($reviews as $review) {
        $review->save();
    }
    
  }  
  
  
  public static function sendReviewReminders() {
    
    $now = time();  
    $review_upcoming_deadline_reminder_days = intval(MPRCommon::getConfigValue('review_upcoming_deadline_reminder_days'));
    $review_overdue_reminder_days = intval(MPRCommon::getConfigValue('review_overdue_reminder_days'));

    
    foreach (Review::getReviews(FALSE, NULL, Review::STATUS_PENDING) as $review) {
        
        // Only check Reviews that have already started.
        $start_timestamp = strtotime($review->getStartDate());
        if ($now >= $start_timestamp) {
            
            // Calculate other dates.
            $upcoming_deadline_reminder_timestamp = $review->getDeadlineReminderTime($review_upcoming_deadline_reminder_days);
            $overdue_reminder_timestamp = $review->getOverdueReminderTime($review_overdue_reminder_days);            
            
            
            // Set flags.
            $send_start_reminder = FALSE;
            $send_upcoming_deadline_reminder = FALSE;
            $send_overdue_reminder = FALSE;
        
            // Obtain the last follow up time.
            $last_follow_up = $review->getCachedLastFollowUpTimestamp();
            if (empty($last_follow_up) == TRUE) {
                // If a follow-up has never been sent, we can send one now
                // provided all other conditions are met.
                $last_follow_up = 0;
            }            
            
            // Send an email about the Review starting if one has not been sent before.
            if ($last_follow_up < $start_timestamp) {
                $send_start_reminder = TRUE;
            }
            
            // Send a reminder about an upcoming deadline if the time is
            // beyond the reminder timestamp and the last email to the Reviewer
            // was sent before the time elapsed.
            if (($now >= $upcoming_deadline_reminder_timestamp) && ($last_follow_up < $upcoming_deadline_reminder_timestamp)) {
                $send_upcoming_deadline_reminder = TRUE;
            }
            
            // Send a reminder about the Review being overdue if the time is
            // beyond the reminder timestamp and the last email to the Reviewer
            // was sent before the time elapsed.            
            if (($now >= $overdue_reminder_timestamp) && ($last_follow_up < $overdue_reminder_timestamp)) {
                $send_overdue_reminder = TRUE;
            }            
            
            if (($send_start_reminder == TRUE) || ($send_upcoming_deadline_reminder == TRUE) || ($send_overdue_reminder == TRUE)) {
                
                // Load related entities.
                $invitation = $review->fabricateAndLoadInvitation();
                if (empty($invitation) == FALSE) {
                    $paper = $invitation->fabricateAndLoadPaper();
                    $reviewer = $invitation->fabricateAndLoadReviewer();
                    if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE)) {
                        
                        $email_result = FALSE;
                        
                        // Start Date reminder.
                        if ($send_start_reminder == TRUE) {
                            $email = MPREmail::createFromEmailTemplate(
                                    'review_start', 
                                    [$invitation, $paper, $reviewer, $review], 
                                    $reviewer->getEmail(), 
                                    '', 
                                    '', 
                                    [$paper]
                            );
                            
                            if ($email->send() == TRUE) {
                                $email_result = TRUE;
                            }
                        }
                        
                        // Upcoming deadline reminder.
                        if ($send_upcoming_deadline_reminder == TRUE) {
                            $email = MPREmail::createFromEmailTemplate(
                                'review_reminder', 
                                [$invitation, $paper, $reviewer, $review], 
                                $reviewer->getEmail(), 
                                '', 
                                '', 
                                []
                            );
                            
                            if ($email->send() == TRUE) {
                                $email_result = TRUE;
                            }                           
                        }                        
                        
                        
                        // Overdue reminder.
                        if ($send_overdue_reminder == TRUE) {
                            $email = MPREmail::createFromEmailTemplate(
                                'review_overdue', 
                                [$invitation, $paper, $reviewer, $review], 
                                $reviewer->getEmail(), 
                                '', 
                                '', 
                                []
                            );
                            
                            if ($email->send() == TRUE) {
                                $email_result = TRUE;
                            }                            
                        }                            
                        
                        
                        // Update Review record if at least one email was sent successfully.
                        if ($email_result == TRUE) {
                            $review->set('cached_last_follow_up_timestamp', $now);
                            $review->setNewRevision(TRUE);
                            $review->save();
                        }
                        
                        
                    }                    
                }
                
            }
            
                        
        }
        
    }
      
  }
  
  

  /**
   * Returns type and token definitions for this entity.
   * Called by multi_peer_review.token.inc when preparing tokens during hook_token_info.
   * 
   * @return array
   *   Associative array containing types and tokens keys.
   */  
  public static function getTokenInfo() {

    $types = [  
        'review' => [
             'name' => t('Review'),
             'description' => t('Tokens for Review fields.'),
        ], 
    ];

    $tokens = [ 
        'review' => [
            'start-date' => [
                 'name' => t("Start Date"),
                 'dynamic' => FALSE,
                 'description' => t('The effective Review start date.'),    
            ],
            'deadline' => [
                 'name' => t("Deadline"),
                 'dynamic' => FALSE,
                 'description' => t('The date when the Review is due.'),    
            ],                 
            'cancel-reason' => [
                 'name' => t("Cancellation Reason"),
                 'dynamic' => FALSE,
                 'description' => t('The reason the Invitation was declined by the Reviewer.'),    
            ],    
            'cancel-suggestion' => [
                 'name' => t("Reviewer Suggestion"),
                 'dynamic' => FALSE,
                 'description' => t('An alternative Reviewer who was suggested by the Reviewer upon cancelling the Review.'),    
            ],            
            'submit-message' => [
                 'name' => t("Message from Reviewer"),
                 'dynamic' => FALSE,
                 'description' => t('Message provided by the Reviewer at the time that they submitted the Review.'),    
            ],                    
            'submit-email-link' => [
                 'name' => t("Submit Email Link"),
                 'dynamic' => FALSE,
                 'description' => t('Link to the Review submission form.'),    
            ],    
            'cancel-email-link' => [
                 'name' => t("Cancel Email Link"),
                 'dynamic' => FALSE,
                 'description' => t('Link to the Review cancellation form.'),    
            ],                 
            'owner-display-name' => [
                 'name' => t("Owner Display Name"),
                 'dynamic' => FALSE,
                 'description' => t('The display name of the user who owns the Invitation. By default it will be their username.'),    
            ],           
        ],               
    ];


    return ['types' => $types, 'tokens' => $tokens];
  }     
  
  
  /**
   * {@inheritdoc}
   */  
  public function getTokenReplacementValue($name) {
    $date_format = MPRCommon::NAMED_DATE_FORMAT_VERBOSE_DATE; // Email dates are typically short.
    
    $res = '';
    switch ($name) {
        case 'start-date':                                         
            $res = MPRCommon::getFormattedDateText($this->getStartDate(), $date_format);                
            break;                                   
        case 'deadline':                                         
            $res = MPRCommon::getFormattedDateText($this->getDeadline(), $date_format);                
            break;             
        case 'cancel-reason':
            $res = $this->getCancelReason();
            break;            
        case 'cancel-suggestion':
            $res = $this->getCancelSuggestion();
            break;                    
        case 'submit-message':
            $res = $this->getSubmitMessage();
            break;                     
        case 'submit-email-link':
            $res = $this->getSubmitEmailLink();
            break;      
        case 'cancel-email-link':
            $res = $this->getCancelEmailLink();
            break;           
        case 'owner-display-name':
            $res = $this->getOwner()->getDisplayName();
            break;     
    }   
    return ($res);
  }
    
  
  
  
}
