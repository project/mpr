<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\multi_peer_review\Entity\SearchResultItemInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRDropDownListItem;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * The Search Result Item entity class.
 *
 * @ContentEntityType(
 *   id = "search_result_item",
 *   label = @Translation("Search Result"),
 *   label_collection = @Translation("Search Results"),
 *   label_singular = @Translation("Search Result"),
 *   label_plural = @Translation("Search Results"),
 *   label_count = @PluralTranslation(
 *     singular = "@count search result item",
 *     plural = "@count search result items"
 *   ),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\SearchResultItemListBuilder",
 *     "access" = "Drupal\multi_peer_review\SearchResultItemAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\SearchResultItemForm",
 *       "add" = "\Drupal\multi_peer_review\Form\SearchResultItemForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\SearchResultItemForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\SearchResultItemDeleteForm",
 *       "activate" = "\Drupal\multi_peer_review\Form\SearchResultItemActivateForm",
 *       "deploy" = "\Drupal\multi_peer_review\Form\SearchResultItemDeployForm",
 *     },
 *   },
 *   admin_permission = "administer multi_peer_review_manage_all_reviewers",
 *   base_table = "search_result_item",
 *   revision_table = "search_result_item_revision",
 *   data_table = "search_result_item_field_data",
 *   revision_data_table = "search_result_item_field_revision",
 *   field_ui_base_route = "entity.search_result_item.collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/multi-peer-review/search-result-items/add",
 *     "edit-form" = "/admin/multi-peer-review/search-result-items/manage/{search_result_item}/edit",
 *     "delete-form" = "/admin/multi-peer-review/search-result-items/manage/{search_result_item}/delete",
 *     "activate-form" = "/admin/multi-peer-review/search-result-items/manage/{search_result_item}/activate",
 *     "deploy-form" = "/admin/multi-peer-review/search-result-items/manage/{search_result_item}/deploy",
 *     "collection" = "/admin/multi-peer-review/reviewers/search",
 *   },
 * )
 */
class SearchResultItem extends CommonContentEntity implements SearchResultItemInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  

  protected $title;
  protected $full_name;
  protected $first_name; 
  protected $last_name;
  protected $email;
  
  protected $source_url;
  protected $external_papers;

  protected $subjects; 
  protected $data_source;
  protected $id_orcid; 
  protected $id_isni; 
  protected $id_researcherid;     
  
  protected $remote_id;
  protected $remote_data;
  
  

  public function getTitle() {
    return $this->title;
  }   
  
  public function getFirstName() {
    return $this->first_name;
  }    

  public function getLastName() {
    return $this->last_name;
  }   
  
  public function getFullName() {
    return $this->full_name;
  }   
 
  
  public function getEmail() {
    return $this->email;
  }      

  
  public function getSourceUrl() {
    return $this->source_url;
  }    
  

  
  public function getDataSource() {
    return $this->data_source;
  }    
  
  public function getSubjects() {
    return $this->subjects['value'];
  }     
  
  public function setSubjects($value) {
    $this->subjects['value'] = $value;
  }     
  
  public function getSubjectsCsv() {
    return MPRCommon::getCsvFromMultiLineValues($this->getSubjects()); 
  }     
    
  
  public function getExternalPapers() {
    return $this->external_papers['value'];
  }    
  
  public function setExternalPapers($value) {
    $this->external_papers['value'] = $value;
  }   

  public function getORCID() {
    return $this->id_orcid;
  }    
    
  public function getISNI() {
    return $this->id_isni;
  }     
  
  public function getResearcherID() {
    return $this->id_researcherid;
  }          
  
  public function getRemoteId() {
    return $this->remote_id;
  }    

  public function getRemoteData() {
    return $this->remote_data['value'];
  }     
  
  public function setRemoteData($value) {
      $this->remote_data['value'] = $value;
  }

  
  /**
   * {@inheritdoc}
   */    
  public function updateCachedSearchMetaData() {    
    // Search Result Items do not have any search meta data.
  }    

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    
    $fields['id']->setDescription(new TranslatableMarkup('The Search Result Item ID.'));

    $fields['title'] = MPRCommon::getDefaultSingleLineTextDbField('Title', 
            'Reviewer’s title.', FALSE);        
    
    $fields['full_name'] = MPRCommon::getDefaultSingleLineTextDbField('First Name', 
            'Reviewer’s first name.', FALSE);    
    
    $fields['first_name'] = MPRCommon::getDefaultSingleLineTextDbField('First Name', 
            'Reviewer’s first name.', FALSE);
    
    $fields['last_name'] = MPRCommon::getDefaultSingleLineTextDbField('Last Name', 
            'Reviewer’s last name.', FALSE);    
       
    $fields['email'] = MPRCommon::getDefaultSingleEmailAddressDbField('Email', 
            'Reviewer’s email.', FALSE);             
    
    $fields['source_url'] = MPRCommon::getDefaultSingleLineTextDbField('Source URL', 
            '', FALSE, 2048);        
    
    $fields['data_source'] = MPRCommon::getDefaultSingleLineTextDbField('Data Source',
            'Name of the search engine script that found the Reviewer.', FALSE);                    

    $fields['external_papers'] = MPRCommon::getDefaultMultiLineTextDbField('External Papers',
            'The external papers this Reviewer has contributed to.', FALSE);    
    
    $fields['subjects'] = MPRCommon::getDefaultMultiLineTextDbField('Subjects',
            'The subjects that the Reviewer specialises in. Each subject must be entered on a single line.', FALSE);    
       
    
    $fields['id_orcid'] = MPRCommon::getDefaultSingleLineTextDbField('ORCID',
            'The Reviewer’s unique identifier on the ORCID registry (https://orcid.org/).', FALSE);       

    $fields['id_isni'] = MPRCommon::getDefaultSingleLineTextDbField('ISNI',
            'The Reviewer’s unique identifier on the ISNI platform (http://www.isni.org/).', FALSE);   

    $fields['id_researcherid'] = MPRCommon::getDefaultSingleLineTextDbField('ResearcherID',
            'The Reviewer’s unique identifier on the Publons platform (https://publons.com).', FALSE);       
    
    $fields['remote_id'] = MPRCommon::getDefaultSingleLineTextDbField('Remote ID', 
            'Reviewer’s ID in the data source.', FALSE);     
    
    $fields['remote_data'] = MPRCommon::getDefaultMultiLineTextDbField('Remote Data',
            'Data stored by the Search Engine Script that found this Reviewer.', FALSE);        
    


    return $fields;
  }

  
  
  /**
   * {@inheritdoc}
   */
  public function save() {
      
      // Archived is false by default.
      if ($this->isNew() == TRUE) {      
          $this->set('archived', FALSE);
      }          
           
      // Call standard save process.
      $status = parent::save();
      
      return $status;
  }    
  
  
  public static function loadByRemoteId($remote_id, $data_source = NULL) {
      $res = NULL;
      
      $items = SearchResultItem::getSearchResultItems($data_source, $remote_id);
      if (empty($items) == FALSE) {
          $res = current($items);
      }
      
      return $res;
  }

  
  public static function getSearchResultItems($data_source = NULL, $remote_id = NULL) {
    $properties = [];
    
    if ($data_source != NULL) {
        $properties['data_source'] = $data_source;
    }
    
    if ($remote_id != NULL) {
        $properties['remote_id'] = $remote_id;
    }    
      
         
    if (empty($properties) == FALSE) {     
        $res = \Drupal::entityTypeManager()->getStorage('search_result_item')->loadByProperties($properties);     
    }
    else {
        $res = Review::loadMultiple();
    }
        
    if (empty($res) == TRUE) {
        $res = [];
    }        
    
    return $res;  
  }  
  
  /**
   * Returns type and token definitions for this entity.
   * Called by multi_peer_review.token.inc when preparing tokens during hook_token_info.
   * 
   * @return array
   *   Associative array containing types and tokens keys.
   */  
  public static function getTokenInfo() {

    // Blank.
    $types = [];
    $tokens = [];

    return ['types' => $types, 'tokens' => $tokens];
  }     
  
  
  /**
   * {@inheritdoc}
   */ 
  public function getTokenReplacementValue($name) {        
    // Blank.
    return '';
  }
  
    
  

}
