<?php

namespace Drupal\multi_peer_review\Entity;


use Drupal\multi_peer_review\Entity\EmailTemplateInterface;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * The EmailTemplate entity class.
 * Delete functionality has been removed from this class. To reinstate it, add the following lines under "form" and "links"
 * respectively:
 * "delete" = "\Drupal\multi_peer_review\Form\EmailTemplateDeleteForm",
 * "delete-form" = "/admin/config/multi-peer-review/email-template/manage/{email_template}/delete",
 *
 * @ConfigEntityType(
 *   id = "email_template",
 *   label = @Translation("Email Template"),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\EmailTemplateListBuilder",
 *     "access" = "Drupal\multi_peer_review\EmailTemplateAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\EmailTemplateForm",
 *       "add" = "\Drupal\multi_peer_review\Form\EmailTemplateForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\EmailTemplateForm",
 *     },
 *   },
 *   config_prefix = "email_template",
 *   admin_permission = "administer multi_peer_review_settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "staff_notes",
 *     "subject",
 *     "body"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/multi-peer-review/email-template/{email_template}",
 *     "add-form" = "/admin/config/multi-peer-review/email-template/add",
 *     "edit-form" = "/admin/config/multi-peer-review/email-template/manage/{email_template}/edit",
 *     "collection" = "/admin/config/multi-peer-review/email-template",
 *   },
 * )
 */
class EmailTemplate extends ConfigEntityBase implements EmailTemplateInterface {

    
  /**
   * The Email Template ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Email Template label.
   *
   * @var string
   */
  protected $label;

  
  /**
   * Email Template staff notes.
   *
   * @var string
   */
  protected $staff_notes;  
  
  
  /**
   * Email Template subject.
   *
   * @var string
   */
  protected $subject;
  
  /**
   * Email Template body.
   *
   * @var mixed
   */
  protected $body;  
  
    
  /**
   * {@inheritdoc}
   */
  public function getStaffNotes() {
    return $this->staff_notes;
  }    
  
  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->subject;
  }  
  
  /**
   * {@inheritdoc}
   */
  public function getBody() {     
    
    if (is_array($this->body) == false){
        $res = $this->body;
    }
    else{
        $res = $this->body["value"];
    }
      
    return $res;
  }  
  
  public static function getEmailTemplateIds() {
    $res = [];
    
    $email_templates = EmailTemplate::loadMultiple();      
    foreach ($email_templates as $email_template_id => $email_template) {        
        array_push($res, $email_template_id);
    }      
    
    return $res;
  }

}
