<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\multi_peer_review\Entity\ReviewerInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRDropDownListItem;


/**
 * Defines common fields and methods used by Multi Peer Review entities.
 */
abstract class CommonContentEntity extends ContentEntityBase {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  
  /**
   * Archive flag.
   *
   * @var array
   */    
  protected $archived;    
  
  /**
   * Email hash.
   *
   * @var string
   */        
  protected $email_hash;    
  
  
  /**
   * Stores meta data used for search purposes.
   *
   * @var string
   */  
  protected $cached_search_meta_data;
  
  
  /**
   * {@inheritdoc}
   */  
  public function getArchived() {
    return $this->archived;
  }     
  
      
  /**
   * {@inheritdoc}
   */
  public function getEmailHash() {
    return $this->email_hash;
  }
 
  
  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    return $this->set('created', (int) $created);
  }
  
  
  /**
   * {@inheritdoc}
   */  
  public function getCachedSearchMetaData() {
    return $this->cached_search_meta_data['value'];
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function setCachedSearchMetaData($cache_data) {
    $this->set('cached_search_meta_data', $cache_data);
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function getTranslatedDescriptiveLabel() {
    // Default is the built-in label.  
    return new TranslatableMarkup($this->label());
  }  
    
  
  /**
   * {@inheritdoc}
   */   
  public function getTranslatedMessage($message_type) {
    
    $placeholder_replacements = [];
    $placeholder_replacements['@entity-type'] = $this->getEntityType()->getLabel();
    $placeholder_replacements['%entity-description'] = $this->getTranslatedDescriptiveLabel();
    $placeholder_replacements['%entity-id'] = $this->id();
    $placeholder_replacements['%entity-bundle'] = $this->bundle();
      
          
    switch ($message_type) {
      case CommonContentEntityInterface::MESSAGE_ADMIN_EDIT_TITLE:
        $res = new TranslatableMarkup('Edit @entity-type %entity-description', $placeholder_replacements);
        break;
      case CommonContentEntityInterface::MESSAGE_ADMIN_INSERTED:            
        $res = new TranslatableMarkup('@entity-type %entity-description has been created.', $placeholder_replacements);
        break;  
      case CommonContentEntityInterface::MESSAGE_ADMIN_UPDATED:            
        $res = new TranslatableMarkup('@entity-type %entity-description has been updated.', $placeholder_replacements);
        break;                 
      default:
        $res = '';
        break;
    }
    
    return $res;
  }
  
  
  /**
   * Logs and displays message related to a given form status. 
   *
   * @param object $logger
   *   Instance of logger that should receive log requests.
   * @param object $messenger
   *   Instance of messenger that should process the message.
   * @param int $status
   *   Form status after saving its entity.
   */    
  public function processAdminFormSaveMessages($logger, $messenger, $status) {
    if (($status == SAVED_UPDATED) || ($status == SAVED_NEW)) {        
      $entity = self::load($this->id());
    }
    else {
      $entity = $this;
    }
    
    
    if ($status == SAVED_UPDATED) {
      $this->logActionForInstance($logger, $entity, 'updated');
      $messenger->addMessage($entity->getTranslatedMessage(CommonContentEntityInterface::MESSAGE_ADMIN_UPDATED));
    }
    else {
      $this->logActionForInstance($logger, $entity, 'added');
      $messenger->addMessage($entity->getTranslatedMessage(CommonContentEntityInterface::MESSAGE_ADMIN_INSERTED));
    }              
  }
  
  
  /**
   * Logs an action for the current entity instance. 
   *
   * @param object $logger
   *   Instance of logger that should receive log requests.
   * @param string $action
   *   Action to log.
   */    
  public function logAction($logger, $action) {
    $this->logActionForInstance($logger, $this, $action);
  }
  
  
  /**
   * Logs an action for a given entity instance. 
   *
   * @param object $logger
   *   Instance of logger that should receive log requests.
   * @param object $entity
   *   Entity related to the action.
   * @param string $action
   *   Action to log.
   */   
  public function logActionForInstance($logger, $entity, $action) {
        
    $descriptive_label = $entity->getTranslatedDescriptiveLabel()->render();

    $info = ['%info' => $descriptive_label];
    $context = ['@type' => $entity->bundle(), '%info' => $descriptive_label];
    
    // Action is not added as a context variable since
    // it conventionally is output into the notice.
    $logger->notice('@type: ' . $action . ' %info.', $context);
  }  
  
  
  /**
   * {@inheritdoc}
   */   
  abstract function updateCachedSearchMetaData();
    

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {                  
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    
    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setDescription(new TranslatableMarkup('The Entity ID.'))
      ->setSetting('max_length', 128)
      ->setRequired(TRUE)
      ->addConstraint('UniqueField')
      ->addPropertyConstraints('value', ['Regex' => ['pattern' => '/^[a-z0-9_]+$/']]);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Unique ID'))
      ->setDescription(new TranslatableMarkup('Enter a unique custom identifier for this entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 255)
      ->setRequired(TRUE);
    
    
    
    $fields['archived'] = MPRCommon::getDefaultCheckBoxDbField('Archived',
            'Denotes the archive status of this record. Archived records are typically hidden.', TRUE);        
    
    $fields['email_hash'] = MPRCommon::getDefaultSingleLineTextDbField('Email Hash', 
            'Unique identifier used for generating email links to this record.', FALSE);       
    
    // Cache fields.
    $fields['cached_search_meta_data'] = MPRCommon::getDefaultMultiLineTextDbField('Search Meta Data', 
            'Cached search meta data.', FALSE);         
        
    
    
    // Owner and date fields.
    $fields['uid']
      ->setLabel(new TranslatableMarkup('Owner'))
      ->setDescription(new TranslatableMarkup('The record owner.'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);


    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the record was last edited.'))
      ->setRevisionable(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the record was created.'));    
    
    
    return $fields;
  }
      
  
  /**
   * {@inheritdoc}
   */
  abstract function getTokenReplacementValue($name);
      
  

}
