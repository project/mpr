<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\multi_peer_review\Entity\PaperInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * The Paper entity class.
 *
 * @ContentEntityType(
 *   id = "paper",
 *   label = @Translation("Paper"),
 *   label_collection = @Translation("Papers"),
 *   label_singular = @Translation("paper"),
 *   label_plural = @Translation("papers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count paper",
 *     plural = "@count papers"
 *   ),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\PaperListBuilder",
 *     "access" = "Drupal\multi_peer_review\PaperAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\PaperForm",
 *       "add" = "\Drupal\multi_peer_review\Form\PaperForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\PaperForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\PaperDeleteForm",
 *       "activate" = "\Drupal\multi_peer_review\Form\PaperActivateForm",
 *       "deploy" = "\Drupal\multi_peer_review\Form\PaperDeployForm",
 *     },
 *   },
 *   admin_permission = "administer multi_peer_review_manage_all_papers",
 *   base_table = "paper",
 *   revision_table = "paper_revision",
 *   data_table = "paper_field_data",
 *   revision_data_table = "paper_field_revision",
 *   field_ui_base_route = "entity.paper.collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/multi-peer-review/papers/add",
 *     "edit-form" = "/admin/multi-peer-review/papers/manage/{paper}/edit",
 *     "delete-form" = "/admin/multi-peer-review/papers/manage/{paper}/delete",
 *     "activate-form" = "/admin/multi-peer-review/papers/manage/{paper}/activate",
 *     "deploy-form" = "/admin/multi-peer-review/papers/manage/{paper}/deploy",
 *     "collection" = "/admin/multi-peer-review/papers",
 *   },
 * )
 */
class Paper extends CommonContentEntity implements PaperInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  
  const STATUS_IDLE = 'Idle';
  const STATUS_SEEKING_REVIEWERS = 'Seeking Reviewers';
  const STATUS_REVIEW_PENDING = 'Review Pending';
  const STATUS_REVIEWED = 'Reviewed';
  

  /**
   * Paper's abstract.
   *
   * @var string
   */
  protected $abstract;
  
  /**
   * Paper's type.
   *
   * @var string
   */
  protected $paper_type;  
  
  
  /**
   * File of the actual Paper.
   *
   * @var array
   *   Contains keys: target_id, display, description
   */
  protected $file;    
  
  
  /**
   * Email addresses of people who should receive updates about the Paper.
   *
   * @var string
   */
  protected $alert_recipients; 


  /**
   * The subjects that the Paper relates to.
   *
   * @var string
   */
  protected $subjects; 
  
  
  /**
   * Each of the Paper’s individual (human) authors/contributors, stored in XML format.
   *
   * @var string
   */
  protected $individual_contributors;   
  
  /**
   * Each of the Paper’s non-individual (organisations, teams etc) authors/contributors, stored in XML format.
   *
   * @var string
   */
  protected $non_individual_contributors;     
  
  
  /**
   * Paper's checklist.
   *
   * @var array
   */  
  protected $checklist;
  
  
  /**
   * Denotes the status of the Paper.
   *
   * @var string
   */  
  protected $status;    
  
  
  /**
   * Marks the Paper as peer reviewed.
   *
   * @var bool
   */    
  protected $peer_review_complete;
  

  

  
  
  /**
   * Review completion percentage based on Accepted Invitations and Submitted Reviews.
   *
   * @var float
   */    
  protected $cached_review_completion_percent;
  
  
  /**
   * Number of Reviews submitted for this Paper.
   *
   * @var int
   */  
  protected $cached_reviews_submitted;
  
    
  /**
   * {@inheritdoc}
   */
  public function getAbstract() {
    return $this->abstract['value'];
  }    
  
  /**
   * {@inheritdoc}
   */
  public function getPaperType() {
    return $this->paper_type;
  }   
  
  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->file;
  }     
  
  public function getFileDownloadMarkup() {
    $res = '';
    
    $file = $this->getFile();
    if (empty($file) == FALSE) {
        $fid = $file['target_id'];
        $file = \Drupal\file\Entity\File::load($fid);
        if (empty($file) == FALSE) {

            $res = '<a target="_blank" href="' . 
                    file_create_url($file->getFileUri()) . '">' . 
                    htmlentities($file->getFilename()) . ' (' . format_size($file->getSize()) . ')</a>';

        }
    }
    
    return $res;
  }       
  
  
  /**
   * {@inheritdoc}
   */
  public function getAlertRecipients() {
    return $this->alert_recipients['value'];
  }   

  /**
   * {@inheritdoc}
   */
  public function getSubjects() {
    return $this->subjects['value'];
  }     
  
  
  public function getSubjectsCsv() {
    return MPRCommon::getCsvFromMultiLineValues($this->getSubjects()); 
  }    
  
  
  /**
   * {@inheritdoc}
   */
  public function getIndividualContributors() {
    return $this->individual_contributors['value'];
  }      
  
  /**
   * {@inheritdoc}
   */
  public function getNonIndividualContributors() {
    return $this->non_individual_contributors['value'];
  }   
  
  
  public function getChecklist() {
    
    $res = [];
    
    $raw = $this->checklist['value'];    
    if (empty($raw) == FALSE) {
        $raw = explode("\n", $raw);
        if (count($raw) != 0) {            
            foreach ($raw as $item) {
                $res[$item] = new TranslatableMarkup($item);
            }
        }
    }
      
    return $res;
  }
  
  public function getChecklistCompletionPercentText() {
        
    $total_items = count(MPRCommon::getTranslatedListOptionsFromConfig('paper_checklist'));
    if ($total_items == 0) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {        
        $completed_items = count($this->getChecklist());

        $percentage = round(($completed_items / $total_items) * 100, 0);
        $res = strval($percentage) . '%';
    }
      
    return $res;
  }  
  
  
  public function getStatus() {
    $res = $this->status;
    if (empty($res) == TRUE) {
        $res = Paper::STATUS_IDLE;
    }
    return $res;
  }     
  
  public function getTranslatedVerboseStatusText() {
      
    // Display percentage if the Paper has reviews pending.            
    $status_text = new TranslatableMarkup($this->getStatus());
    if ($status_text == Paper::STATUS_REVIEW_PENDING) {
        $status_text .= ' (' . $this->getCachedReviewCompletionPercentText() . ')';
    }     
    
    return $status_text;
  }
  
  public static function getStatuses() {    
    return [
        Paper::STATUS_IDLE, 
        Paper::STATUS_SEEKING_REVIEWERS, 
        Paper::STATUS_REVIEW_PENDING, 
        Paper::STATUS_REVIEWED
        ];
  }   
  
  public static function getTranslatedStatusListOptions() {
    $res = [];
    foreach (Paper::getStatuses() as $status) {
        $res[$status] = new TranslatableMarkup($status);
    }        
    return $res;
  }     
  
  public static function getTranslatedStatuses() {
    $res = [];
    foreach (Paper::getStatuses() as $status) {
        array_push($res, new TranslatableMarkup($status));
    }
    return $res;
  }     
  
  public function getPeerReviewComplete() {
    return $this->peer_review_complete;
  }
  

  
  /**
   * {@inheritdoc}
   */
  public function getCachedReviewCompletionPercent() {
    return $this->cached_review_completion_percent;
  }      
  
  public function getCachedReviewCompletionPercentText() {
    $res = $this->getCachedReviewCompletionPercent();
    if (empty($res) == TRUE) {
        $res = '0%';
    }
    else {
        $res = strval($res) . '%';
    }
    return $res;
  }    
  
  
  public function getCachedReviewsSubmitted() {
    return $this->cached_reviews_submitted;
  }
  
  
  /**
   * Adds email attachment data to parameters for MailManager processing.
   *
   * @param array &$params
   *   MailManager associative array.
   */       
  public function addEmailAttachmentsToMailParams(&$params) {      
      $fids = $this->getFileIdsFromFile($this);
      if (empty($fids) == FALSE) {
          
          $attachments = [];
          foreach ($fids as $fid) {
            $file = \Drupal\file\Entity\File::load($fid);

            $file_info = [
                'filecontent' => file_get_contents($file->getFileUri()),
                'filename' => $file->getFilename(),
                'filemime' => $file->getMimeType(),
                'filesize' => $file->getSize(),
            ];
             
            array_push($attachments, $file_info);
          }
          
          // Avoids conflicts with other attachment modules
          if (array_key_exists('multi_peer_review_attachments', $params) == FALSE) {
            $params['multi_peer_review_attachments'] = $attachments;          
          }
          else {
              foreach ($attachments as $attachment) {
                  array_push($params['multi_peer_review_attachments'], $attachment);
              }
          }       
      }
  }  
  
  
  /**
   * {@inheritdoc}
   */  
  public function updateCachedSearchMetaData() {        
    $this->setCachedSearchMetaData($this->getOwner()->getDisplayName());
  }  
  

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);


    $fields['id']->setDescription(new TranslatableMarkup('The Paper ID.'));


    $fields['label']->setLabel(new TranslatableMarkup('Title'));
    $fields['label']->setDescription(new TranslatableMarkup('Title of the Paper'));
        
    
    $fields['abstract'] = MPRCommon::getDefaultHtmlTextDbField('Abstract', 
            'Paper’s Abstract.', TRUE);    
    
    $fields['paper_type'] = MPRCommon::getDefaultSingleLineTextDbField('Type',             
            'The type of Paper.', TRUE);
    
    $fields['file'] = MPRCommon::getDefaultFileDbField('File', 
            'The actual Paper in Word Document or PDF format.', TRUE);    
    
    $fields['alert_recipients'] = MPRCommon::getDefaultMultiLineTextDbField('Alert Recipients',
            'Email addresses of people who should receive updates about the Paper. Each email address should be separated by a comma.', FALSE);
    
    $fields['subjects'] = MPRCommon::getDefaultMultiLineTextDbField('Subjects',
            'The subjects that the Paper relates to. Each subject must be entered on a single line.', FALSE);
    
    $fields['checklist'] = MPRCommon::getDefaultMultiLineTextDbField('Checklist',
            'Indicates administrative statuses that apply to the Paper.', FALSE);    
    
    $fields['individual_contributors'] = MPRCommon::getDefaultMultiLineTextDbField('Individual Contributors',
                'Each of the Paper’s individual (human) authors/contributors.', FALSE);
    
    $fields['non_individual_contributors'] = MPRCommon::getDefaultMultiLineTextDbField('Non-Individual Contributors',
                'Each of the Paper’s non-individual (organisations, teams etc) authors/contributors.', FALSE);    
    
    $fields['status'] = MPRCommon::getDefaultSingleLineTextDbField('Status',             
            'Denotes the status of the Paper.', FALSE);    
    
    $fields['peer_review_complete'] = MPRCommon::getDefaultCheckBoxDbField('Peer Review Complete',
            'Confirms that the Paper has been Peer Reviewed.', FALSE);    
    

    
    // Fields for storing cached figures and figures used internally.
    
    $fields['cached_owner_display_name'] = MPRCommon::getDefaultSingleLineTextDbField('Cached Owner Display Name', 
            'Cached owner display name for sorting purposes.', FALSE);           
    
    $fields['cached_review_completion_percent'] = MPRCommon::getDefaultDecimalDbField('Review Completion Percent', 
            'Review completion percentage based on Accepted Invitations and Submitted Reviews.', FALSE);       
    
    $fields['cached_reviews_submitted'] = MPRCommon::getDefaultIntegerDbField('Reviews Submitted', 
            'Number of Reviews submitted for this Paper.', FALSE);       
    
    

    return $fields;
  }

  


  /**
   * Gets an array of file ids from the file field.
   * 
   * @param Paper $paper
   *   An instance of a Paper entity.
   *
   * @return array
   *   An array of integers. If there is no file, an empty array is returned.
   */    
  public function getFileIdsFromFile($paper) {
    $res = $paper->getFile();
    if (empty($res) == FALSE) {
        if (empty($res['target_id']) == FALSE) {
            $res = [$res['target_id']];
        }
        else {
            $res = [];
        }
    }
    else {
        $res = [];
    }      
    
    return $res;
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function save() {
      
      // Prepare unique email hash if this is a new record.
      if ($this->isNew() == TRUE) {
          $this->set('email_hash', MPRCommon::getNewEmailHash($this, $this->label()));
          $this->set('status', Paper::STATUS_IDLE);
          $this->set('peer_review_complete', FALSE);
      }          
      
      // Update search cache.     
      $this->updateCachedSearchMetaData();
      
      
      // Get a list of currently linked files.
      if ($this->isNew() == FALSE) {
        $old_attachment_fids = $this->getFileIdsFromFile(Paper::load($this->id()));
      }
      else {
        $old_attachment_fids = [];
      }
      
      
      // Call standard save process.
      $status = parent::save();
      
      
      // Update caches of related entities.
      if ($status == SAVED_UPDATED) {          
          // Update Invitations.
          $invitation_ids = [];
          foreach (Invitation::getInvitations(NULL, NULL, NULL, $this->id()) as $invitation) {              
            Invitation::rebuildCachedFigures($invitation);                        
            array_push($invitation_ids, $invitation->id());
          }
          
          // Update Reviews.
          foreach (Review::getReviews() as $review) {
              if (in_array($review->getInvitation(), $invitation_ids)) {
                Review::rebuildCachedFigures($review);
              }
          }               
      }
      
      
      // Get current attachments.
      $dat = Paper::load($this->id());
      $attachment_fids = $this->getFileIdsFromFile($dat);
            
      
      $this_type_name = $this->getEntityTypeId();
      $file_usage = \Drupal::service('file.usage');
      

      $remove_fids = [];
      if (count($attachment_fids) == 0) {
          if (count($old_attachment_fids) != 0) {
              // Need to unlink all attachments
              $remove_fids = $old_attachment_fids;
          }          
      }
      else {          
          
          // Process removed attachments
          foreach ($old_attachment_fids as $fid) {
              if (in_array($fid, $attachment_fids) == FALSE) {
                  // Queue attachment for removal
                  array_push($remove_fids, $fid);
              }
          }
                    
      }
      
      
      // Process file link removal queue
      foreach ($remove_fids as $fid) {
        $file = \Drupal\file\Entity\File::load($fid);
        if ($file != NULL) {
            $list = $file_usage->listUsage($file);

            if ($list != NULL) { 
                
                // Check that the file is not linked to another entity
                // $list value example: array(1) { ["multi_peer_review"]=> array(1) { ["review"]=> array(1) { ["1596717083004071300"]=> string(1) "1" } } }
                $list_count = 0;
                foreach ($list as $module_type_key => $module_type_value) {
                    foreach ($module_type_value as $entity_type_key => $entity_type_value) {
                        $list_count += intval($entity_type_value);
                    }
                }

                $delete_file_usage = FALSE;  
                $delete_file = FALSE;                  

                if ($list_count > 0) {
                    $delete_file_usage = TRUE;  
                }

                if ($list_count == 1) {
                    $delete_file = TRUE;
                }              

                if ($delete_file_usage == TRUE) {
                    // @see Drupal\file\FileUsage\FileUsageInterface
                    $file_usage->delete($file, 'multi_peer_review', $this_type_name, $this->id(), 0);                   

                    if ($delete_file == TRUE) {
                        $file->delete();                    
                    }
                }                            
            }
        }
      }
      
      
      return $status;
  }  
  

  /**
   * {@inheritdoc}
   */
  public function delete() {

    // Prepare to delete related usage record and file.
    $delete_file_usage = FALSE;    
    $delete_file = FALSE;
    $list_count = 0;
    $entity_id = $this->id();
    $this_type_name = $this->getEntityTypeId();
    
    $fid = $this->file['target_id'];
    if ($fid != NULL) {
        $file = \Drupal\file\Entity\File::load($fid);
        $file_usage = \Drupal::service('file.usage');
        $list = $file_usage->listUsage($file);

        // Check that the file linked to this entity is not used elsewhere.
        if ($list != NULL) {            
            $list_count = 0;
            foreach ($list as $module_type_key => $module_type_value) {
                foreach ($module_type_value as $entity_type_key => $entity_type_value) {
                    $list_count += intval($entity_type_value);
                }
            }            
            
            if ($list_count > 0) {
                $delete_file_usage = TRUE;  
            }
            
            if ($list_count == 1) {
                $delete_file = TRUE;
            }
        }        
    }

    // Run standard deletion process.
    parent::delete();    
    
    // Delete related usage record and file.
    if ($delete_file_usage == TRUE) {
        // @see Drupal\file\FileUsage\FileUsageInterface
        $file_usage->delete($file, 'multi_peer_review', $this_type_name, $entity_id, 0);    
                
        if ($delete_file == TRUE) {
            $file->delete();
        }
    }
    
  }  
  
  
  public static function getPapers($archived = NULL, $email_hash = NULL, $status = NULL, $peer_review_complete = NULL, $owner_id = NULL) {
    $properties = [];
    
    if ($archived !== NULL) {
        $properties['archived'] = $archived;
    }
    
    if ($email_hash !== NULL) {
        $properties['email_hash'] = $email_hash;
    }     
    
    if ($status !== NULL) {
        $properties['status'] = $status;
    }         
        
    if ($peer_review_complete !== NULL) {
        $properties['peer_review_complete'] = $peer_review_complete;
    }       
    
    if ($owner_id !== NULL) {
        $properties['uid'] = $owner_id;
    }          
      
    if (empty($properties) == FALSE) {      
        $res = \Drupal::entityTypeManager()->getStorage('paper')->loadByProperties($properties);     
    }
    else {
        $res = Paper::loadMultiple();
    }
        
    if (empty($res) == TRUE) {
        $res = [];
    }        
    
    return $res;      
  }     
  
  public static function sortPapersByStatusAndTitle(&$papers) {
    uasort($papers, ['self', 'sortPapersByStatusAndTitleComparison']);
  }
  
  private static function getSortWeightForStatus($status) {    
    switch ($status) {
        case Paper::STATUS_IDLE;
            $res = 1;
            break;
        case Paper::STATUS_SEEKING_REVIEWERS;
            $res = 2;
            break;        
        case Paper::STATUS_REVIEWED;
            $res = 3;
            break;        
        default:
            $res = 4;
            break;
    }
    
    return $res;
  }  

  private static function sortPapersByStatusAndTitleComparison($a, $b) {
    
    $status_weight_a = Paper::getSortWeightForStatus($a->getStatus());
    $status_weight_b = Paper::getSortWeightForStatus($b->getStatus());
    
    if ($status_weight_a == $status_weight_b) {      
        if ($a->label() == $b->label()) {
            $res = 0;
        }
        else {
            if ($a->label() > $b->label()) {
                $res = 1;
            }
            else {
                $res = -1;
            }
        }
    }
    else {
        if ($status_weight_a > $status_weight_b) {
            $res = -1;
        }
        else {
            $res = 1;
        }
    }
    
    return $res;
  }
  

  public static function rebuildCachedFigures($paper_to_update = NULL) {
      
    $invitations = Invitation::getInvitations(FALSE);      
    $reviews = Review::getReviews(FALSE);
      
    if ($paper_to_update == NULL) {
        $papers = Paper::getPapers(FALSE);
    }
    else {
        $papers = [$paper_to_update];
    }

    foreach ($papers as $paper) {
        
        $invitations_pending = 0;
        $invitations_accepted = 0;
        $invitations_declined = 0;
        $invitations_retracted = 0;
        
        $reviews_pending = 0;
        $reviews_submitted = 0;
        $reviews_cancelled = 0;

        // Detect Invitations for this Paper.
        foreach ($invitations as $invitation_id => $invitation) {
            if ($invitation->getPaper() == $paper->id()) {
                switch ($invitation->getStatus()) {
                    case Invitation::STATUS_PENDING:
                        $invitations_pending++;
                        break;
                    case Invitation::STATUS_ACCEPTED:
                        $invitations_accepted++;
                        break;
                    case Invitation::STATUS_DECLINED:
                        $invitations_declined++;
                        break;
                    case Invitation::STATUS_RETRACTED:
                        $invitations_retracted++;
                        break;
                }
            }
        }
        
        // Detect Reviews for this Paper.
        foreach ($reviews as $review_id => $review) {
            if (array_key_exists($review->getInvitation(), $invitations) == TRUE) {
                $review_invitation = $invitations[$review->getInvitation()];

                if ($review_invitation->getPaper() == $paper->id()) {
                    switch ($review->getStatus()) {
                        case Review::STATUS_PENDING:
                            $reviews_pending++;
                            break;
                        case Review::STATUS_SUBMITTED:
                            $reviews_submitted++;
                            break;
                        case Review::STATUS_CANCELLED:
                            $reviews_cancelled++;
                            break;
                    }
                }     
                
            }
        }
        
        // Default status.
        $status = Paper::STATUS_IDLE;
        $review_completion_percent = 0;
        $update_required = FALSE;
        
        if ($invitations_pending != 0) {
            $status = Paper::STATUS_SEEKING_REVIEWERS;
        }
        
        if (($reviews_pending != 0) || ($reviews_submitted != 0)) {
            $status = Paper::STATUS_REVIEW_PENDING;
        }
        
        if ($invitations_accepted != 0) {
            
            $review_completion_percent = round(($reviews_submitted / $invitations_accepted) * 100, 2);
            if ($review_completion_percent > 100) {
                $review_completion_percent = 100;
            }
            
            // The Paper must be manually marked as complete before the peer reviewed status can apply.
            // Note that there is no expectation here that the minimum reviews are met since that can
            // change over time, so the best indicator is the staff's judgement.
            if ($paper->getPeerReviewComplete() == TRUE) { // && ($reviews_submitted >= $invitations_accepted)) {
                
                // The number of Reviews submitted must be equal to or greater
                // than the number of Invitations that were accepted for the Paper
                // to be considered reviewed.
                
                $status = Paper::STATUS_REVIEWED;
            }
        }
        
        
        if ($paper->getStatus() != $status) {
            $paper->set('status', $status);
            $update_required = TRUE;
        }
        
        if ($paper->getCachedReviewCompletionPercent() != $review_completion_percent) {
            $paper->set('cached_review_completion_percent', $review_completion_percent);
            $update_required = TRUE;
        }
        
        if ($paper->getCachedReviewsSubmitted() != $reviews_submitted) {
            $paper->set('cached_reviews_submitted', $reviews_submitted);
            $update_required = TRUE;
        }
        
        if ($update_required == TRUE) {
            $paper->save();
        }
        

    }
      
  }
  

  /**
   * Returns type and token definitions for this entity.
   * Called by multi_peer_review.token.inc when preparing tokens during hook_token_info.
   * 
   * @return array
   *   Associative array containing types and tokens keys.
   */  
  public static function getTokenInfo() {

    $types = [  
       'paper' => [
            'name' => t('Paper'),
            'description' => t('Tokens for Paper fields.'),
       ],  
    ];

    $tokens = [ 
       'paper' => [
           'title' => [
                'name' => t("Title"),
                'dynamic' => FALSE,
                'description' => t('Title of the Paper.'),               
           ],        
           'owner-display-name' => [
                'name' => t("Owner Display Name"),
                'dynamic' => FALSE,
                'description' => t('The display name of the user who owns the Paper. By default it will be their username.'),    
           ],       
           'abstract' => [
                'name' => t("Abstract"),
                'dynamic' => FALSE,
                'description' => t('Paper abstract.'),    
           ],              
       ],                        
    ];


    return ['types' => $types, 'tokens' => $tokens];
  }     
  
  
  /**
   * {@inheritdoc}
   */
  public function getTokenReplacementValue($name) {        
    $res = '';
    switch ($name) {
        case 'title':                                         
            $res = $this->label();
            break;             
        case 'owner-display-name':
            $res = $this->getOwner()->getDisplayName();
            break;   
        case 'abstract':                                         
            $res = \Drupal\Core\Field\FieldFilteredMarkup::create($this->getAbstract());
            break;         
    }   
    return ($res);
  }
  
    
  
  
}
