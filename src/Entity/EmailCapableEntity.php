<?php
namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\multi_peer_review\Entity\ReviewInterface;
use Drupal\multi_peer_review\MPRCommon;

/* 
 * Abstract class for content entities that are capable of being used to send emails.
 */
abstract class EmailCapableEntity extends CommonContentEntity implements EmailCapableEntityInterface {
    
    
    
  /**
   * Email subject line.
   *
   * @var string
   */        
  protected $email_subject;
  
  
  /**
   * HTML content of email.
   *
   * @var array
   */    
  protected $email_body;
  
  
  /**
   * Email addresses of carbon-copy recipients.
   *
   * @var array
   */  
  protected $email_cc;
  
  
  /**
   * Email addresses of blind carbon-copy recipients.
   *
   * @var array
   */    
  protected $email_bcc;
  
  
  /**
   * Comma-delimited file ids (fid) of any email attachments.
   *
   * @var array
   */    
  protected $email_attachments;    
    
  

  /**
   * {@inheritdoc}
   */    
  public function getEmailSubject() {
    return $this->email_subject;
  }   

  /**
   * {@inheritdoc}
   */  
  public function getEmailBody() {
    return $this->email_body['value'];
  }      
  
  /**
   * {@inheritdoc}
   */  
  public function getEmailCC() {
    return $this->email_cc['value'];
  }     
  
  /**
   * {@inheritdoc}
   */  
  public function getEmailBCC() {
    return $this->email_bcc['value'];
  }         
  
  /**
   * {@inheritdoc}
   */  
  public function getEmailAttachments() {
    
    $res = NULL;
    
    if (($this->email_attachments != NULL) && (array_key_exists('value', $this->email_attachments) == TRUE)) {
        $res = $this->email_attachments['value'];
        if ($res != NULL) {
            if (strpos($res, ',') === FALSE) {
                $res = [$res];
            }
            else {
                $res = explode(',', $res);
            }
        }
    }
    
    return $res;
  }        
  


  /**
   * Adds email attachment data to parameters for MailManager processing.
   *
   * @param array &$params
   *   MailManager associative array.
   */       
  public function addEmailAttachmentsToMailParams(&$params) {      
      $fids = $this->getEmailAttachments();
      if (empty($fids) == FALSE) {
          
          $attachments = [];
          foreach ($fids as $fid) {
            $file = \Drupal\file\Entity\File::load($fid);

            $file_info = [
                'filecontent' => file_get_contents($file->getFileUri()),
                'filename' => $file->getFilename(),
                'filemime' => $file->getMimeType(),
                'filesize' => $file->getSize(),
            ];
             
            array_push($attachments, $file_info);
          }
          
          // Avoids conflicts with other attachment modules
          if (array_key_exists('multi_peer_review_attachments', $params) == FALSE) {
            $params['multi_peer_review_attachments'] = $attachments;          
          }
          else {
              foreach ($attachments as $attachment) {
                  array_push($params['multi_peer_review_attachments'], $attachment);
              }
          }
      }
  }

  /**
   * Returns a new instance of the subclass, loaded with data from the database.
   */
  abstract function fabricateAndLoadEntity($id);
  
  
  
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    
    $fields['email_attachments'] = MPRCommon::getDefaultMultiFileDbField('Attachment',
            'Files attached to the email represented by this entity.', FALSE);         
    
    $fields['email_subject'] = MPRCommon::getDefaultSingleLineTextDbField('Email Subject', 
            'Subject line of the email represented by this entity.', TRUE);    
    
    $fields['email_body'] = MPRCommon::getDefaultHtmlTextDbField('Email Body', 
            'Body of the email.', TRUE);    
    
    $fields['email_cc'] = MPRCommon::getDefaultMultiLineTextDbField('Email CC',
            'Email addresses of other recipients who should receive a copy of the email represented by this entity.', FALSE);      
    
    $fields['email_bcc'] = MPRCommon::getDefaultMultiLineTextDbField('Email BCC',
            'Email addresses of other recipients who should receive a copy of the email represented by this entity without the knowledge of other recipients.', FALSE);       
    
    
    // Cache fields.
    $fields['cached_paper_title'] = MPRCommon::getDefaultSingleLineTextDbField('Cached Paper Title', 
            'Cached Paper Title for sorting purposes.', FALSE);        
    
    $fields['cached_reviewer_name'] = MPRCommon::getDefaultSingleLineTextDbField('Cached Reviewer Name', 
            'Cached Reviewer Name for sorting purposes.', FALSE);     
    
    
    return $fields;
  }  
  
  
  /**
   * {@inheritdoc}
   */
  public function save() {
      
      // Prepare unique email hash if this is a new record.
      if ($this->isNew() == TRUE) {
          $this->set('email_hash', MPRCommon::getNewEmailHash($this, $this->label()));
      }      
      
      
      // Get a list of currently linked files
      if ($this->isNew() == FALSE) {
        $dat = $this->fabricateAndLoadEntity($this->id());
        $old_attachment_fids = $dat->getEmailAttachments();
        if ($old_attachment_fids == NULL) {
            $old_attachment_fids = [];
        }
      }
      else {
        $old_attachment_fids = [];
      }
      
      
      // Call standard save process
      $status = parent::save();
      
      
      // Get current attachments
      $dat = $this->fabricateAndLoadEntity($this->id());
      $attachment_fids = $dat->getEmailAttachments();
      if ($attachment_fids == NULL) {
          $attachment_fids = [];
      }      
      
      
      $this_type_name = $this->getEntityTypeId();
      $file_usage = \Drupal::service('file.usage');
      

      $remove_fids = [];
      if (count($attachment_fids) == 0) {
          if (count($old_attachment_fids) != 0) {
              // Need to unlink all attachments
              $remove_fids = $old_attachment_fids;
          }          
      }
      else {          
          
          // Process removed attachments
          foreach ($old_attachment_fids as $fid) {
              if (in_array($fid, $attachment_fids) == FALSE) {
                  // Queue attachment for removal
                  array_push($remove_fids, $fid);
              }
          }
          
          
          // Process new and existing attachments
          foreach ($attachment_fids as $fid) {
              
            $add_to_usage = FALSE;
            $file = \Drupal\file\Entity\File::load($fid);
            if ($file != NULL) {                
                $list = $file_usage->listUsage($file);

                // Check that the file is linked to this entity
                if ($list != NULL) {        

                    $add_to_usage = TRUE;

                    // Example value: array(1) { ["multi_peer_review"]=> array(1) { ["review"]=> array(1) { ["1596717083004071300"]=> string(1) "1" } } } 
                    if (array_key_exists('multi_peer_review', $list) == TRUE) {
                        if (array_key_exists($this_type_name, $list['multi_peer_review']) == TRUE) {
                            if (array_key_exists($this->id(), $list['multi_peer_review'][$this_type_name]) == TRUE) {
                                $add_to_usage = FALSE; // Proves that this file is already linked
                            }
                        }
                    }                

                }
                else {                
                    // The file is not linked to this entity.
                    // Therefore, we add a reference between the file and the entity.
                    $add_to_usage = TRUE;
                }

                if ($add_to_usage == TRUE) {
                    // @see Drupal\file\FileUsage\FileUsageInterface
                    $file_usage->add($file, 'multi_peer_review', $this_type_name, $this->id(), 1);                                          
                }

              }
          }
                    
      }
      
      
      // Process file link removal queue
      foreach ($remove_fids as $fid) {
        $file = \Drupal\file\Entity\File::load($fid);
        if ($file != NULL) {
            $list = $file_usage->listUsage($file);

            if ($list != NULL) { 
                
                // Check that the file is not linked to another entity
                // $list value example: array(1) { ["multi_peer_review"]=> array(1) { ["review"]=> array(1) { ["1596717083004071300"]=> string(1) "1" } } }
                $list_count = 0;
                foreach ($list as $module_type_key => $module_type_value) {
                    foreach ($module_type_value as $entity_type_key => $entity_type_value) {
                        $list_count += intval($entity_type_value);
                    }
                }

                $delete_file_usage = FALSE;  
                $delete_file = FALSE;                  

                if ($list_count > 0) {
                    $delete_file_usage = TRUE;  
                }

                if ($list_count == 1) {
                    $delete_file = TRUE;
                }              

                if ($delete_file_usage == TRUE) {
                    // @see Drupal\file\FileUsage\FileUsageInterface
                    $file_usage->delete($file, 'multi_peer_review', $this_type_name, $this->id(), 0);                   

                    if ($delete_file == TRUE) {
                        $file->delete();                    
                    }
                }                            
            }
        }
      }
      
      
      return $status;
  }    
    
    
  
  
  /**
   * {@inheritdoc}
   */
  public function delete() {

    // We override the delete method so that we can update managed file usage
    // records when the entity is deleted.
      
    // Prepare to delete related usage record and file
    $delete_queue = [];
    $this_type_name = $this->getEntityTypeId();
    $entity_id = $this->id();
    $attachment_fids = $this->getEmailAttachments();
    if ($attachment_fids != NULL) {
        $file_usage = \Drupal::service('file.usage');
        
        // Process existing attachments
        foreach ($attachment_fids as $fid) {
         
          $file = \Drupal\file\Entity\File::load($fid);
          if ($file != NULL) {              
              $list = $file_usage->listUsage($file);

              // Check that the file is not linked to another entity
              // $list value example: array(1) { ["multi_peer_review"]=> array(1) { ["review"]=> array(1) { ["1596717083004071300"]=> string(1) "1" } } }
              if ($list != NULL) {               
                    
                    $list_count = 0;
                    foreach ($list as $module_type_key => $module_type_value) {
                        foreach ($module_type_value as $entity_type_key => $entity_type_value) {
                            $list_count += intval($entity_type_value);
                        }
                    }
                                      
                    $delete_file_usage = FALSE;  
                    $delete_file = FALSE;                  
                                      
                    if ($list_count > 0) {
                        $delete_file_usage = TRUE;  
                    }

                    if ($list_count == 1) {
                        $delete_file = TRUE;
                    }              
                    
                    $delete_queue[$fid] = ['delete_file_usage' => $delete_file_usage, 'delete_file' => $delete_file];
              }             

           }
        }
    }

    // Run standard deletion process
    parent::delete();    
    
    // Delete related usage records and files
    foreach ($delete_queue as $key => $value) {
        $fid = $key;
        $delete_file_usage = $value['delete_file_usage'];
        $delete_file = $value['delete_file'];
        
        $file = \Drupal\file\Entity\File::load($fid);
        if ($file != NULL) {           
            if ($delete_file_usage == TRUE) {
                // @see Drupal\file\FileUsage\FileUsageInterface
                $file_usage->delete($file, 'multi_peer_review', $this_type_name, $entity_id, 0);                   
                
                if ($delete_file == TRUE) {
                    $file->delete();                    
                }
            }
        }
    }
    
  }  
  

  
  /**
   * Additional form validation processing to be used within forms containing an email attachment control.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */    
  public function validateFormProcess(&$form, $form_state) {

    // Converts the email attachment form field into a comma-delimited string.
    // Note: Control property structure is $form['email_attachments_control']["#value"]['fids']
    if (empty($form['email_attachments_control']) == FALSE) {
        $fileIds = $form['email_attachments_control']['#value'];
        if ($fileIds != NULL) {
            if (array_key_exists('fids', $fileIds) == TRUE) {
                $fileIds = $fileIds['fids'];            
                $fileIds = implode(',', $fileIds);

                $form_state->setValue('email_attachments', $fileIds);     
            }
        }    
    }
    
  }  
  
  
    
}