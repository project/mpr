<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Url;
use Drupal\multi_peer_review\Entity\WebRequestCacheItemInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * The WebRequestCacheItem entity class.
 *
 * @ContentEntityType(
 *   id = "web_request_cache_item",
 *   label = @Translation("Web Request Cache Item"),
 *   label_collection = @Translation("Web Request Cache"),
 *   label_singular = @Translation("Web Request Cache Item"),
 *   label_plural = @Translation("Web Request Cache Items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count web request cache item",
 *     plural = "@count web request cache items"
 *   ),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\WebRequestCacheItemListBuilder",
 *     "access" = "Drupal\multi_peer_review\WebRequestCacheItemAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\WebRequestCacheItemForm",
 *       "add" = "\Drupal\multi_peer_review\Form\WebRequestCacheItemForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\WebRequestCacheItemForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\WebRequestCacheItemDeleteForm",
 *       "activate" = "\Drupal\multi_peer_review\Form\WebRequestCacheItemActivateForm",
 *       "deploy" = "\Drupal\multi_peer_review\Form\WebRequestCacheItemDeployForm",
 *     },
 *   },
 *   admin_permission = "administer multi_peer_review_settings",
 *   base_table = "web_request_cache_item",
 *   revision_table = "web_request_cache_item_revision",
 *   data_table = "web_request_cache_item_field_data",
 *   revision_data_table = "web_request_cache_item_field_revision",
 *   field_ui_base_route = "entity.web_request_cache_item.collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/multi-peer-review/web-request-cache-items/add",
 *     "edit-form" = "/admin/multi-peer-review/web-request-cache-items/manage/{web_request_cache_item}/edit",
 *     "delete-form" = "/admin/multi-peer-review/web-request-cache-items/manage/{web_request_cache_item}/delete",
 *     "activate-form" = "/admin/multi-peer-review/web-request-cache-items/manage/{web_request_cache_item}/activate",
 *     "deploy-form" = "/admin/multi-peer-review/web-request-cache-items/manage/{web_request_cache_item}/deploy",
 *     "collection" = "/admin/multi-peer-review/web-request-cache-items",
 *   },
 * )
 */
class WebRequestCacheItem extends CommonContentEntity implements WebRequestCacheItemInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  
  
  protected $external_url;
  protected $page_source;



  public function getExternalUrl() {
    return $this->extenal_url;
  }     

  public function getPageSource() {
    return $this->page_source['value'];
  }       


  /**
   * {@inheritdoc}
   */    
  public function updateCachedSearchMetaData() {    
    // Web Request Cache Items do not have any search meta data.
  }      
  
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    

    $fields['id']->setDescription(new TranslatableMarkup('The Web Request Cache Item ID.'));
    
        
    
    $fields['external_url'] = MPRCommon::getDefaultSingleLineTextDbField('External URL',
            'The URL that this cache item represents.', FALSE, 2048);   

    $fields['page_source'] = MPRCommon::getDefaultMultiLineTextDbField('Page Source',
            'HTML downloaded from the external URL.', FALSE);       
    
   

    return $fields;
  }

  
  
  
  public static function deleteExpiredWebRequestCacheItems() {
    
    // Calculate expiry time. Today minus 3 months (approximately).
    $expiry_time = time() - MPRCommon::getSecondsFromDays(90);
      
    $items = WebRequestCacheItem::loadMultiple();
    if (empty($items) == FALSE) {
        
        foreach ($items as $item_id => $item) {
            if ($item->getCreatedTime() <= $expiry_time) {
                $item->delete();
            }
        }
        
    }
      
  }
  

  /**
   * Returns type and token definitions for this entity.
   * Called by multi_peer_review.token.inc when preparing tokens during hook_token_info.
   * 
   * @return array
   *   Associative array containing types and tokens keys.
   */  
  public static function getTokenInfo() {
   
    // Blank.
    $types = [];
    $tokens = [];


    return ['types' => $types, 'tokens' => $tokens];
  }     
  
  
  /**
   * {@inheritdoc}
   */   
  public function getTokenReplacementValue($name) {
    // Blank.
    return '';
  }
  
  


  
  
}
