<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Url;
use Drupal\multi_peer_review\Entity\InvitationInterface;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\multi_peer_review\MPRDropDownListItem;
use Drupal\multi_peer_review\MPREmail;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Review;


/**
 * The Invitation entity class.
 *
 * @ContentEntityType(
 *   id = "invitation",
 *   label = @Translation("Invitation"),
 *   label_collection = @Translation("Invitations"),
 *   label_singular = @Translation("invitation"),
 *   label_plural = @Translation("invitations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count invitation",
 *     plural = "@count invitations"
 *   ),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\InvitationListBuilder",
 *     "access" = "Drupal\multi_peer_review\InvitationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\multi_peer_review\InvitationHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\InvitationForm",
 *       "add" = "\Drupal\multi_peer_review\Form\InvitationForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\InvitationForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\InvitationDeleteForm",
 *       "activate" = "\Drupal\multi_peer_review\Form\InvitationActivateForm",
 *       "deploy" = "\Drupal\multi_peer_review\Form\InvitationDeployForm",
 *       "send" = "\Drupal\multi_peer_review\Form\InvitationSendForm",
 *       "retract" = "\Drupal\multi_peer_review\Form\InvitationRetractForm",
 *       "accept" = "\Drupal\multi_peer_review\Form\InvitationAcceptForm",
 *       "decline" = "\Drupal\multi_peer_review\Form\InvitationDeclineForm",
 *     },
 *   },
 *   admin_permission = "administer multi_peer_review_manage_all_invitations",
 *   base_table = "invitation",
 *   revision_table = "invitation_revision",
 *   data_table = "invitation_field_data",
 *   revision_data_table = "invitation_field_revision",
 *   field_ui_base_route = "entity.invitation.collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/multi-peer-review/invitations/add",
 *     "edit-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/edit",
 *     "delete-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/delete",
 *     "activate-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/activate",
 *     "deploy-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/deploy",
 *     "collection" = "/admin/multi-peer-review/invitations",
 *     "send-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/send",
 *     "retract-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/retract",
 *     "accept-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/accept",
 *     "decline-form" = "/admin/multi-peer-review/invitations/manage/{invitation}/decline",
 *   },
 * )
 */
class Invitation extends EmailCapableEntity implements InvitationInterface, MPRDropDownListItem {

  use EntityChangedTrait;
  use EntityOwnerTrait;


  const STATUS_DRAFT = 'Draft';
  const STATUS_PENDING = 'Pending';
  const STATUS_ACCEPTED = 'Accepted';
  const STATUS_DECLINED = 'Declined';
  const STATUS_RETRACTED = 'Retracted';  

  
  protected $paper;
  protected $reviewer;
  protected $target_start_date;
  protected $preferred_start_date;
  protected $deadline;
  protected $status;
  protected $decline_reason;
  protected $decline_message;
  protected $decline_suggestion;


  protected $cached_follow_ups;
  protected $cached_last_follow_up_timestamp;
  


  public function getPaper() {
    return $this->paper;
  }     

  public function getReviewer() {
    return $this->reviewer;
  }       

  public function getTargetStartDate() {
    return $this->target_start_date;
  }     
  
  public function getPreferredStartDate() {
    return $this->preferred_start_date;
  }  
  
  public function getDeadline() {
    return $this->deadline;
  }   
  
  public function getDeclineReason() {
    return $this->decline_reason;
  }     
  
  public function getDeclineReasonText() {
    $res = $this->getDeclineReason();
    if ((empty($res) == FALSE) && ($res == '_none')) {
        $res = 'None';
    }
    return ($res);
  }     
  
  public function getDeclineMessage() {
    return $this->decline_message['value'];
  }    

  public function getDeclineSuggestion() {
    return $this->decline_suggestion['value'];
  }        
  
  public function getStatus() {
    $res = $this->status;
    if (empty($res) == TRUE) {
        $res = Invitation::STATUS_DRAFT;
    }
    return $res;
  }     
  
  public static function getStatuses() {    
    return [
        Invitation::STATUS_DRAFT, 
        Invitation::STATUS_PENDING, 
        Invitation::STATUS_ACCEPTED, 
        Invitation::STATUS_DECLINED, 
        Invitation::STATUS_RETRACTED,
    ];
  }     
  
  public static function getTranslatedStatusListOptions() {
    $res = [];
    foreach (Invitation::getStatuses() as $status) {
        $res[$status] = new TranslatableMarkup($status);
    }        
    return $res;
  }     
  
  public static function getTranslatedStatuses() {
    $res = [];
    foreach (Invitation::getStatuses() as $status) {
        array_push($res, new TranslatableMarkup($status));
    }
    return $res;
  }     
  
   
  
  public function fabricateAndLoadPaper() {
    $res = NULL;
    if (empty($this->paper) == FALSE) {      
      $res = \Drupal\multi_peer_review\Entity\Paper::load($this->paper);                 
    }
    return $res;
  }  
  
  public function fabricateAndLoadReviewer() {
    $res = NULL;
    if (empty($this->reviewer) == FALSE) {      
      $res = \Drupal\multi_peer_review\Entity\Reviewer::load($this->reviewer);                 
    }
    return $res;
  }  
  
  public function getPaperTitle() {
    $dat = $this->fabricateAndLoadPaper();
    if ($dat == NULL) {
        $res = new TranslatableMarkup(MPRCommon::ENTITY_ISSUE_BROKEN_RELATIONSHIP);
    }
    else {
        $res = $dat->label();
    }                 
    return $res;
  }
  
  public function getReviewerName() {
    $dat = $this->fabricateAndLoadReviewer();
    if ($dat == NULL) {
        $res = new TranslatableMarkup(MPRCommon::ENTITY_ISSUE_BROKEN_RELATIONSHIP);
    }
    else {
        $res = $dat->getListItemText();
    }   
    return $res;
  }  
    
  
  public function getCachedFollowUps() {
    $res = $this->cached_follow_ups;
    if (empty($res) == TRUE) {
        $res = 0;
    }
    return $res;
  }   
  
  public function getCalculatedRemainingFollowUps($maximum_invitation_follow_ups = NULL) {
    if ($maximum_invitation_follow_ups == NULL) {
        $max = intval(MPRCommon::getConfigValue('maximum_invitation_follow_ups'));
    }
    else {
        $max = $maximum_invitation_follow_ups;
    }
    return ($max - $this->getCachedFollowUps());
  }   
  
  public function getCachedLastFollowUpTimestamp() {
    return $this->cached_last_follow_up_timestamp;
  }   
  
  public function getCachedLastFollowUpTimestampText() {
    $res = $this->getCachedLastFollowUpTimestamp();
    if (empty($res) == TRUE) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = MPRCommon::getFormattedDateText(intval($res));
    }
    return $res;
  }     
  
  public function getCalculatedNextFollowUpDate($invitation_follow_up_frequency_days = NULL) {
    $res = NULL;      
    
    // Follow-ups only occur when the Invitation is pending.
    if ($this->getStatus() == Invitation::STATUS_PENDING) {
        $last = $this->getCachedLastFollowUpTimestamp();
        if (empty($last) == TRUE) {
            // If there has not been a follow-up before, the next date is based on the created date.
            $last = $this->getCreatedTime();
        }

        if (empty($last) == FALSE) {
            if ($invitation_follow_up_frequency_days == NULL) {
                $frequency_days = intval(MPRCommon::getConfigValue('invitation_follow_up_frequency_days'));
            }
            else {
                $frequency_days = $invitation_follow_up_frequency_days;
            }

            // Convert days to seconds.
            $frequency_seconds = MPRCommon::getSecondsFromDays($frequency_days);

            $res = intval($last) + $frequency_seconds;
        }
    }
    return $res;
  }     
  
  public function getCalculatedNextFollowUpDateText() {
    $res = $this->getCalculatedNextFollowUpDate();
    if (empty($res) == TRUE) {
        $res = new TranslatableMarkup(MPRCommon::VALUE_STATE_NON_APPLICABLE);
    }
    else {
        $res = MPRCommon::getFormattedDateText($res);
    }
    return $res;
  }      
  
  
  public function getAcceptEmailLink() {
    $home = Url::fromRoute('<front>');
    $home->setAbsolute();
    
    $res = $home->toString();
    if (substr($res, strlen($res) - 1) != '/') {
        $res .= '/';
    }            
    $res .= 'multi-peer-review/email-link/accept-invitation?eml=' . urlencode($this->getEmailHash());           
    
    return $res;
  }  
  
  public function getDeclineEmailLink() {
    $home = Url::fromRoute('<front>');
    $home->setAbsolute();
    
    $res = $home->toString();
    if (substr($res, strlen($res) - 1) != '/') {
        $res .= '/';
    }         
    $res .= 'multi-peer-review/email-link/decline-invitation?eml=' . urlencode($this->getEmailHash());           
    
    return $res;
  }    
  
  
  /**
   * {@inheritdoc}
   */
  public function getListItemText() {
      return trim($this->getPaperTitle() . ' - ' . $this->getReviewerName());
  }
  
  /**
   * {@inheritdoc}
   */
  public function getListItemValue() {
      return $this->id();
  }  

  
  /**
   * {@inheritdoc}
   */
  public function getTranslatedDescriptiveLabel() {
    
    $paper = $this->fabricateAndLoadPaper();  
    $reviewer = $this->fabricateAndLoadReviewer();
    $res = new TranslatableMarkup('');
    
    if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE)) {
        $placeholder_replacements = [];
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $this, '@');
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $paper, '@');
        MPRCommon::buildEntityPlaceholderReplacements($placeholder_replacements, $reviewer, '@');
        
        $res = new TranslatableMarkup('@paper-title for @reviewer-full-name', $placeholder_replacements);  
    }
      
    return $res;
  }    
  

  /**
   * {@inheritdoc}
   */    
  public function updateCachedSearchMetaData() {
    // Set search cache.
    $cache_data = [];
    $paper_id = $this->get('paper')->value;
    $paper = Paper::load($paper_id);
    if (empty($paper) == FALSE) {
        array_push($cache_data, $paper->label());
        
        $this->set('cached_paper_title', $paper->label());
    }

    $reviewer_id = $this->get('reviewer')->value;
    $reviewer = Reviewer::load($reviewer_id);
    if (empty($reviewer) == FALSE) {
        array_push($cache_data, $reviewer->getFullName());
        
        $this->set('cached_reviewer_name', $reviewer->getFullName());
    }      
    
    $this->setCachedSearchMetaData(implode(' ', $cache_data));
  }
  

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    
    $fields['id']->setDescription(new TranslatableMarkup('The Invitation ID.'));
        
    
    $fields['paper'] = MPRCommon::getDefaultDropDownDbField('Paper', 
            'The Paper that the Invitation relates to.', TRUE);
    
    $fields['reviewer'] = MPRCommon::getDefaultDropDownDbField('Reviewer',
            'The Reviewer that is being invited to review the Paper.', TRUE);    
                    
    
    $fields['target_start_date'] = MPRCommon::getDefaultDateDbField('Target Start Date',
            'The team’s target start date.', TRUE);    
    
    $fields['preferred_start_date'] = MPRCommon::getDefaultDateDbField('Preferred Start Date',
            'The reviewer’s Preferred start date.', FALSE);        
    
    $fields['deadline'] = MPRCommon::getDefaultDateDbField('Deadline',
            'The Review deadline.', TRUE);              
    

    $fields['decline_reason'] = MPRCommon::getDefaultDropDownDbField('Reason', 
            'Reason that the Invitation was declined by the Reviewer. The drop-down options are configurable in Multi Peer Review Settings.', FALSE);          
    
    $fields['decline_message'] = MPRCommon::getDefaultHtmlTextDbField('Message', 
            'Message provided by the Reviewer at the time they declined the Invitation.', FALSE);        
    
    $fields['decline_suggestion'] = MPRCommon::getDefaultHtmlTextDbField('Suggested Reviewer', 
            'An alternative Reviewer who was suggested by the declining Reviewer.', FALSE);
    
    
    $fields['status'] = MPRCommon::getDefaultSingleLineTextDbField('Status', 
            'Indicates the status of the Invitation.', TRUE);     
                 
    
    // Fields for storing cached figures and figures used internally.
    $fields['cached_follow_ups'] = MPRCommon::getDefaultIntegerDbField('Follow-ups', 
            'Number of follow-up emails sent to Reviewer.', FALSE);    
    
    $fields['cached_last_follow_up_timestamp'] = MPRCommon::getDefaultTimestampDbField('Last Follow-up Timestamp',
            'The last time a follow-up email was sent to the Reviewer about the Invitation.', FALSE);     
         
    $fields['pending_timestamp'] = MPRCommon::getDefaultTimestampDbField('Pending Timestamp',
            'Time when the Invitation changed to Pending status.', FALSE);        
    
    $fields['accepted_timestamp'] = MPRCommon::getDefaultTimestampDbField('Accepted Timestamp',
            'Time when the Invitation was Accepted.', FALSE);        

    $fields['declined_timestamp'] = MPRCommon::getDefaultTimestampDbField('Declined Timestamp',
            'Time when the Invitation was Declined.', FALSE);                
    
    $fields['retracted_timestamp'] = MPRCommon::getDefaultTimestampDbField('Retracted Timestamp',
            'Time when the Invitation was Retracted.', FALSE);                
        
    

    return $fields;
  }

  
  
  /**
   * {@inheritdoc}
   */
  function fabricateAndLoadEntity($id) {
    return Invitation::load($id);
  }    
  
  
  /**
   * {@inheritdoc}
   */
  function save() {
            
      // Update search cache.      
      $this->updateCachedSearchMetaData();      
      
      
      // Call standard save process.
      $status = parent::save();      
      
      return $status;
  }  
  
  
  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    
    //Rebuild cached figures.
    Reviewer::rebuildCachedFigures();
    Paper::rebuildCachedFigures();
  }
  
  
  public static function getInvitations($archived = NULL, $email_hash = NULL, $status = NULL, $paper_id = NULL, $reviewer_id = NULL) {
    $properties = [];
    
    if ($archived !== NULL) {
        $properties['archived'] = $archived;
    }
    
    if ($email_hash !== NULL) {
        $properties['email_hash'] = $email_hash;
    }     
    
    if ($status !== NULL) {
        $properties['status'] = $status;
    }
    
    if ($paper_id !== NULL) {
        $properties['paper'] = $paper_id;
    }

    if ($reviewer_id !== NULL) {
        $properties['reviewer'] = $reviewer_id;
    }    
      
    if (empty($properties) == FALSE) {        
        $res = \Drupal::entityTypeManager()->getStorage('invitation')->loadByProperties($properties);     
    }
    else {
        $res = Invitation::loadMultiple();
    }
        
    if (empty($res) == TRUE) {
        $res = [];
    }        
    
    return $res;      
  }              
  
  
  
  public static function sortInvitationsByDeadlineDate(&$invitations) {
    uasort($invitations, ['self', 'sortInvitationsByDeadlineDateComparison']);
  }
    
  private static function sortInvitationsByDeadlineDateComparison($a, $b) {
          
    $deadline_a = strtotime($a->getDeadline());
    $deadline_b = strtotime($b->getDeadline());    
    $created_a = intval($a->getCreatedTime());
    $created_b = intval($b->getCreatedTime());    
               
    if ($deadline_a == $deadline_b) {
        if ($created_a == $created_b) {
            $res = 0;
        }
        else {
            if ($created_a > $created_b) {
                $res = 1;
            }
            else {
                $res = -1;
            }
        }
    }
    else {
        if ($deadline_a > $deadline_b) {
            $res = 1;
        }
        else {
            $res = -1;
        }
    }
    
    return $res;
  }    
  
  
  
  public static function rebuildCachedFigures($invitation_to_update = NULL) {  
    
    if ($invitation_to_update == NULL) {
        $invitations = Invitation::getInvitations();
    }
    else {
        $invitations = [$invitation_to_update];
    }
    
    foreach ($invitations as $invitation) {
        $invitation->save();
    }
    
  }
  
  
  public static function autoRetractExpiredInvitations() {
   
    $entities_updated = FALSE;
    $now = time();
    $maximum_invitation_follow_ups = intval(MPRCommon::getConfigValue('maximum_invitation_follow_ups'));
    $invitation_follow_up_frequency_days = intval(MPRCommon::getConfigValue('invitation_follow_up_frequency_days'));
    
      
    // Check pending Invitations.
    foreach (Invitation::getInvitations(FALSE, NULL, Invitation::STATUS_PENDING) as $invitation) {
        
        // Check if any follow-ups remain.
        if ($invitation->getCalculatedRemainingFollowUps($maximum_invitation_follow_ups) <= 0) {
        
            // Check the next follow-up date.
            $next_follow_up_date = $invitation->getCalculatedNextFollowUpDate($invitation_follow_up_frequency_days);
            if ((empty($next_follow_up_date) == FALSE) && ($now >= $next_follow_up_date)) {
                
                // Retract Invitation.
                $paper = $invitation->fabricateAndLoadPaper();
                $reviewer = $invitation->fabricateAndLoadReviewer();
                if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE)) {
                
                    $email = MPREmail::createFromEmailTemplate('invitation_auto_retract', 
                            [$invitation, $paper, $reviewer], 
                            $reviewer->getEmail(), 
                            '', 
                            '', 
                            []
                    );
                    
                    if ($email->send() == TRUE) {
                        
                        // Update Invitation status.
                        $invitation->set('status', Invitation::STATUS_RETRACTED);
                        $invitation->set('retracted_timestamp', $now);
                        $invitation->setNewRevision(TRUE);
                        if ($invitation->save() == SAVED_UPDATED) {
                                                        
                            // Rebuild caches.
                            Invitation::rebuildCachedFigures($invitation);
                            Reviewer::rebuildCachedFigures($reviewer);
                            Paper::rebuildCachedFigures($paper);
                            
                            
                            // Send a notification to the Invitation owner.                  
                            $email = MPREmail::createFromEmailTemplate('invitation_auto_retract_notify', 
                                    [$invitation, $paper, $reviewer], 
                                    $invitation->getOwner()->getEmail(),
                                    '', 
                                    '', 
                                    []
                            );         
                            $email->send();
                            

                         
                            $entities_updated = TRUE;
                        }
                    }
                }
                
            }
        }
        
    }
    
    if ($entities_updated == TRUE) {
        Review::rebuildCachedFigures();
    }
      
      
  }
  

  public static function sendInvitationFollowUps() {
   
    $entities_updated = FALSE;
    $now = time();
    $maximum_invitation_follow_ups = intval(MPRCommon::getConfigValue('maximum_invitation_follow_ups'));
    $invitation_follow_up_frequency_days = intval(MPRCommon::getConfigValue('invitation_follow_up_frequency_days'));
    
      
    // Check pending Invitations.
    foreach (Invitation::getInvitations(FALSE, NULL, Invitation::STATUS_PENDING) as $invitation) {
        
        // Check if any follow-ups remain.
        if ($invitation->getCalculatedRemainingFollowUps($maximum_invitation_follow_ups) > 0) {
        
            // Check the next follow-up date.
            $next_follow_up_date = $invitation->getCalculatedNextFollowUpDate($invitation_follow_up_frequency_days);
            if ((empty($next_follow_up_date) == FALSE) && ($now >= $next_follow_up_date)) {
                
                // Send follow-up.
                $paper = $invitation->fabricateAndLoadPaper();
                $reviewer = $invitation->fabricateAndLoadReviewer();
                if ((empty($paper) == FALSE) && (empty($reviewer) == FALSE)) {
                
                    // The follow-up email does not contain the attachment since
                    // typically, Reviewers should only see the Paper once they accept to Review it.
                    $email = MPREmail::createFromEmailTemplate('invitation_follow_up', 
                            [$invitation, $paper, $reviewer], 
                            $reviewer->getEmail(), 
                            '', 
                            '', 
                            []
                    );
                    
                    if ($email->send() == TRUE) {
                        
                        // Update figures.
                        $follow_ups = $invitation->getCachedFollowUps();
                        $follow_ups++;
                        $invitation->set('cached_follow_ups', $follow_ups);
                        $invitation->set('cached_last_follow_up_timestamp', $now);                          
                        $invitation->setNewRevision(TRUE);
                        if ($invitation->save() == SAVED_UPDATED) {
                                                        
                            // Rebuild caches.
                            Invitation::rebuildCachedFigures($invitation);
                            Reviewer::rebuildCachedFigures($reviewer);
                            Paper::rebuildCachedFigures($paper);
                            
                         
                            $entities_updated = TRUE;
                        }
                    }
                }
                
            }
        }
        
    }
    
    if ($entities_updated == TRUE) {
        Review::rebuildCachedFigures();
    }
      
      
  }
  
  
  

  /**
   * Returns type and token definitions for this entity.
   * Called by multi_peer_review.token.inc when preparing tokens during hook_token_info.
   * 
   * @return array
   *   Associative array containing types and tokens keys.
   */  
  public static function getTokenInfo() {

    $types = [  
        'invitation' => [
             'name' => t('Invitation'),
             'description' => t('Tokens for Invitation fields.'),
        ], 
    ];

    $tokens = [ 
        'invitation' => [
            'target-start-date' => [
                 'name' => t("Target Start Date"),
                 'dynamic' => FALSE,
                 'description' => t('The team’s target start date.'),    
            ],
            'deadline' => [
                 'name' => t("Deadline"),
                 'dynamic' => FALSE,
                 'description' => t('The date when the Review is due.'),    
            ],      
            'preferred-start-date' => [
                 'name' => t("Preferred Start Date"),
                 'dynamic' => FALSE,
                 'description' => t('The date when the Reviewer prefers to start reviewing.'),    
            ],               
            'decline-reason' => [
                 'name' => t("Reason"),
                 'dynamic' => FALSE,
                 'description' => t('The reason the Invitation was declined by the Reviewer.'),    
            ],    
            'decline-message' => [
                 'name' => t("Message"),
                 'dynamic' => FALSE,
                 'description' => t('Message provided by the Reviewer at the time that they declined the Invitation.'),    
            ],    
            'decline-suggestion' => [
                 'name' => t("Reviewer Suggestion"),
                 'dynamic' => FALSE,
                 'description' => t('An alternative Reviewer who was suggested by the declining Reviewer.'),    
            ],            
            'accept-email-link' => [
                 'name' => t("Accept Email Link"),
                 'dynamic' => FALSE,
                 'description' => t('Link to the Invitation acceptance form.'),    
            ],    
            'decline-email-link' => [
                 'name' => t("Decline Email Link"),
                 'dynamic' => FALSE,
                 'description' => t('Link to the Invitation declination form.'),    
            ],              
            'owner-display-name' => [
                 'name' => t("Owner Display Name"),
                 'dynamic' => FALSE,
                 'description' => t('The display name of the user who owns the Invitation. By default it will be their username.'),    
            ],           
        ],               
    ];


    return ['types' => $types, 'tokens' => $tokens];    
  }     
  
  
  /**
   * {@inheritdoc}
   */
  public function getTokenReplacementValue($name) {
    $date_format = MPRCommon::NAMED_DATE_FORMAT_VERBOSE_DATE; // Email dates are typically short.
    
    $res = '';
    switch ($name) {
        case 'target-start-date':                                         
            $res = MPRCommon::getFormattedDateText($this->getTargetStartDate(), $date_format);                
            break;                                   
        case 'deadline':                                         
            $res = MPRCommon::getFormattedDateText($this->getDeadline(), $date_format);                
            break;         
        case 'preferred-start-date':                                         
            $res = MPRCommon::getFormattedDateText($this->getPreferredStartDate(), $date_format);                
            break;      
        case 'decline-reason':
            $res = $this->getDeclineReasonText();
            break;            
        case 'decline-message':
            $res = $this->getDeclineMessage();
            break;                    
        case 'decline-suggestion':
            $res = $this->getDeclineSuggestion();
            break;            
        case 'accept-email-link':
            $res = $this->getAcceptEmailLink();
            break;      
        case 'decline-email-link':
            $res = $this->getDeclineEmailLink();
            break;              
        case 'owner-display-name':
            $res = $this->getOwner()->getDisplayName();
            break;     
    }   
    return ($res);
  }
  
  


  
  
}
