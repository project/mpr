<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines an interface for the Email Capable abstract class.
 */
interface EmailCapableEntityInterface extends CommonContentEntityInterface {
    

    
  /**
   * Gets the email subject line.
   *
   * @return string
   *   Single line of text.
   */    
  public function getEmailSubject();

  
  /**
   * Gets the email body content.
   *
   * @return string
   *   HTML formatted text.
   */  
  public function getEmailBody();
  
  
  /**
   * Gets the email carbon-copy recipients.
   *
   * @return string
   *   A comma-delimited or semicolon-delimited string of email addresses.
   */  
  public function getEmailCC();
  
  
  /**
   * Gets the email blind carbon-copy recipients.
   *
   * @return string
   *   A comma-delimited or semicolon-delimited string of email addresses.
   */  
  public function getEmailBCC();
  
  
  /**
   * Gets the email attachment file ids.
   *
   * @return array
   *   An array containing the file ids (fid) of any email attachments.
   */  
  public function getEmailAttachments();
   
    
}