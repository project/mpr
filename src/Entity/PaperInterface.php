<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines an interface for the Paper entity type.
 */
interface PaperInterface extends CommonContentEntityInterface {

    
  /**
   * Gets the Paper's abstract.
   *
   * @return string
   *   Abstract HTML.
   */
  public function getAbstract();       
  
  /**
   * Gets the Paper's type.
   *
   * @return string
   *   Paper type.
   */
  public function getPaperType();         
  
  
  /**
   * Gets the file of the actual paper.
   *
   * @return array
   *   Associative array containing keys: target_id, display, description
   */
  public function getFile();     
    
  
  /**
   * Gets the email addresses of people who should receive updates about the Paper.
   *
   * @return string
   *   Multi-line string of email addresses.
   */
  public function getAlertRecipients();

  /**
   * Gets the subjects that the Paper relates to.
   *
   * @return string
   *   Multi-line string of subject names.
   */
  public function getSubjects();
  
  
  /**
   * Gets the Paper’s individual (human) authors/contributors.
   *
   * @return string
   *   XML containing contributor information.
   */
  public function getIndividualContributors();
  
  /**
   * Gets the Paper’s non-individual (organisations, teams etc) authors/contributors.
   *
   * @return string
   *   XML containing contributor information.
   */
  public function getNonIndividualContributors();  


}
