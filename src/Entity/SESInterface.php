<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\multi_peer_review\Entity\Reviewer;
use Drupal\multi_peer_review\Entity\Paper;
use Drupal\multi_peer_review\Entity\Invitation;
use Drupal\multi_peer_review\Entity\Review;

/**
 * Defines an interface for the SES entity type.
 */
interface SESInterface extends ConfigEntityInterface {

    
  /**
   * Gets the SES script.
   *
   * @return string
   *   Script content of the SES.
   */
  public function getEngineScript();    

  
  
  /**
   * Gets string data stored by the SES. Each SES will store data uniquely or not at all.
   *
   * @return string
   *   Data in serialised string, XML, JSON or other format.
   */
  public function getInternalStorage();
  
  
  /**
   * Stores string data on the SES record.
   * 
   * @param string $value
   *   A string that should be stored on the SES record.
   * 
   */
  public function setInternalStorage($value);  
  
  /**
   * Gets the online status of the SES. The SES will only scan external websites when it is marked as online.
   *
   * @return bool
   *   True if the SES has been marked as online.
   */
  public function getOnline();
  
}
