<?php

namespace Drupal\multi_peer_review\Entity;


use Drupal\multi_peer_review\Entity\SESInterface;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\multi_peer_review\MPRCommon;
use Drupal\Core\StringTranslation\TranslatableMarkup;


/**
 * The SES entity class.
 *
 * @ConfigEntityType(
 *   id = "ses",
 *   label = @Translation("Search Engine Script"),
 *   handlers = {
 *     "list_builder" = "\Drupal\multi_peer_review\SESListBuilder",
 *     "access" = "Drupal\multi_peer_review\SESAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "\Drupal\multi_peer_review\Form\SESForm",
 *       "add" = "\Drupal\multi_peer_review\Form\SESForm",
 *       "edit" = "\Drupal\multi_peer_review\Form\SESForm",
 *       "delete" = "\Drupal\multi_peer_review\Form\SESDeleteForm",
 *     },
 *   },
 *   config_prefix = "ses",
 *   admin_permission = "administer multi_peer_review_settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "online",
 *     "internal_storage",
 *     "engine_script"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/multi-peer-review/ses/{ses}",
 *     "add-form" = "/admin/config/multi-peer-review/ses/add",
 *     "edit-form" = "/admin/config/multi-peer-review/ses/manage/{ses}/edit",
 *     "delete-form" = "/admin/config/multi-peer-review/ses/manage/{ses}/delete",
 *     "collection" = "/admin/config/multi-peer-review/ses",
 *   },
 * )
 */
class SES extends ConfigEntityBase implements SESInterface {

    
  /**
   * The SES ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The SES label.
   *
   * @var string
   */
  protected $label;

  
  /**
   * Search Engine Script code.
   *
   * @var string
   */
  protected $engine_script;
  
  /**
   * Search Engine Script internal storage.
   *
   * @var string
   */
  protected $internal_storage;  
  
  
  /**
   * Search Engine Script online status.
   *
   * @var bool
   */
  protected $online;  
  
    
  /**
   * {@inheritdoc}
   */
  public function getEngineScript() {
    return $this->engine_script;
  }  
  

  /**
   * {@inheritdoc}
   */
  public function getInternalStorage() {
    return $this->internal_storage;
  }    
  
  /**
   * {@inheritdoc}
   */  
  public function setInternalStorage($value) {
    $this->internal_storage = $value;
  }      
  
  /**
   * {@inheritdoc}
   */
  public function getOnline() {
    return $this->online;
  }    
  
  
  public function fabricateSearchEngineScriptObject() {
    
    $res = NULL;
    
    $engine_script = $this->getEngineScript();
    $engine_script_test = $engine_script;
    $engine_script_test = str_replace("\n", '', $engine_script_test);
    $engine_script_test = str_replace("\r", '', $engine_script_test);
    $engine_script_test = str_replace(' ', '', $engine_script_test);
    $engine_script_test = str_replace("\t", '', $engine_script_test);
    $engine_script_test = str_replace("\\", '', $engine_script_test);
    
    if ((strpos($engine_script, 'class') !== FALSE) && (strpos($engine_script_test, 'extendsDrupalmulti_peer_reviewMPRSearchEngineScript') !== FALSE)) {
        $script_class_name = trim(MPRCommon::getTextBetweenText($engine_script, 'class', 'extends'));
        if ($script_class_name != '') {
            
            // Load script class, unless it has already been loaded.
            if (class_exists($script_class_name) == FALSE) {
                eval($engine_script);
            }

            if (class_exists($script_class_name) == TRUE) {
                // Detect required methods.
                $required_methods = [
                    'getName', 
                    'getVersion', 
                    'getDescription', 
                    'getDeveloperName', 
                    'getDeveloperUrl',
                    'buildSearchCache',
                ];
                
                $required_methods_check = '|' . implode('||', $required_methods) . '|';
                $defined_methods = get_class_methods($script_class_name);
                foreach ($defined_methods as $defined_method) {
                    $required_methods_check = str_replace($defined_method, '', $required_methods_check);
                }
                
                $required_methods_check = trim(str_replace('|', '', $required_methods_check));
                if ($required_methods_check == '') {
                    // All required methods are defined, it is ok to proceed.
                    
                    // Initialise script and set storage reference.
                    $res = new $script_class_name();
                    $res->setStorageHandler($this);                    
                }
                
            }
        }
    }
    
    return $res;
  }
  
  
  public static function getOnlineSearchEngineScriptObjects() {    
    $res = [];
    $properties = [];    
    $properties['online'] = TRUE;
         
    $search_engine_scripts = \Drupal::entityTypeManager()->getStorage('ses')->loadByProperties($properties);
    foreach ($search_engine_scripts as $ses_id => $ses_item) {
        array_push($res, $ses_item->fabricateSearchEngineScriptObject());
    }
    
    return $res;
  }
  
  public static function buildSearchCaches() {
      
    // Get a list of all online Search Engine Scripts and build their caches.
    foreach (SES::getOnlineSearchEngineScriptObjects() as $ses_object) {
        // Find X new results within Y second(s).
        $ses_object->buildSearchCache(20, time() + 60);
    }
    
  }
  
  
  public static function runSelfTests() {
    
    $alert_lines = [];  
    $res = TRUE;
      
    // Get a list of all online Search Engine Scripts and run their self test methods.
    foreach (SES::getOnlineSearchEngineScriptObjects() as $ses_object) {        
        if ($ses_object->selfTest() == FALSE) {
            
            // Report on failure.
            // Note that this method does nothing unless the script developer
            // has overridden the method and added logic to report to an endpoint, whether it be
            // via email or a ping back.
            $ses_object->notifyScriptDeveloperOfSelfTestFailure();

            
            $placeholder_replacements = ['%name' => $ses_object->getName()];
            
            // Prepare notification to Multi Peer Review staff.
            array_push($alert_lines, '<li>' . (new TranslatableMarkup('Search Engine Script %name failed it\'s self test.', $placeholder_replacements)) . '</li>');        
            
            // Indicate that at least one test failed.
            $res = FALSE;
        }
    }
    
    
    // Send notification to Multi Peer Review staff.
    if (count($alert_lines) != 0) {
        
        $message_html = '<ul>' . implode("\n", $alert_lines) . '</ul>';
        
        MPRCommon::sendEmailToTechnicalContact($message_html);        
    }
    
    return $res;
  }  
  
    

}
