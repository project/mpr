<?php

namespace Drupal\multi_peer_review\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines an interface for the CommonContentEntity abstract class.
 */
interface CommonContentEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Defines the message types that may be requested for this entity.
   *
   */    
  const MESSAGE_ADMIN_EDIT_TITLE = 1;
  const MESSAGE_ADMIN_INSERTED = 2;
  const MESSAGE_ADMIN_UPDATED = 3;

    
    
    
  /**
   * Gets the archived status of this entity.
   *
   * @return bool
   *   If TRUE, the entity is considered archived.
   */  
  public function getArchived();
    
 
  /**
   * Gets the entity's unique email hash.
   *
   * @return string
   *   An HTML friendly hash value that uniquely identifies this entity in emails.
   */  
  public function getEmailHash();      
    

  /**
   * Gets the entity's creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Reviewer.
   */
  public function getCreatedTime();

  /**
   * Sets the entity's creation timestamp.
   *
   * @param int $timestamp
   *   The Reviewer creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);
  
  
  /**
   * Gets the entity's cached search meta data.
   *
   * @return string
   *   Search meta data.
   */  
  public function getCachedSearchMetaData();
  
  
  /**
   * Set's the search meta data cache.
   *
   * @param string $cache_data
   *   The search meta data to cache. The value is stored in this entity.
   */  
  public function setCachedSearchMetaData($cache_data);

  
  
  /**
   * Gets a translated label that is descriptive of this entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A label that describes this entity properly.
   */  
  public function getTranslatedDescriptiveLabel();
  

  /**
   * Gets a translated message describing the entity or an action taken on this entity.
   * 
   * @param int $message_type
   *   A message type such as MESSAGE_ADMIN_EDIT_TITLE.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A label that describes this entity properly.
   */    
  public function getTranslatedMessage($message_type);
   
  
  
  /**
   * Generates search meta data based on the data in this entity and stores it within the entity.
   *
   */    
  public function updateCachedSearchMetaData();  
  
  
  /**
   * Returns a value to replace a given token placeholder using the current object.
   * Called by multi_peer_review.token.inc when processing tokens during hook_tokens.
   * 
   * @param string $name
   *   Token name.
   * 
   * @return string
   *   A value that corresponds to the token name.
   */    
  public function getTokenReplacementValue($name);      
  
  
  
  /**
   * Adds standard log entries and UI messages for an entity that has been saved.
   * 
   * @param string $name
   *   Token name.
   * @param string $name
   *   Token name.
   * @param string $name
   *   Token name.
   * 
   */     
  public function processAdminFormSaveMessages($logger, $messenger, $status);
  

}
