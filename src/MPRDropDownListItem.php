<?php
namespace Drupal\multi_peer_review;

/**
 * Represents an object that can be parsed as an option in an HTML select element.
 * 
 */
interface MPRDropDownListItem {
    

  /**
   * Gets the option item text.
   *
   * @return string
   *   Text that represents this item.
   */
  public function getListItemText();    
  
  /**
   * Gets the option item value.
   *
   * @return string
   *   The value of this item.
   */
  public function getListItemValue();     
    
}