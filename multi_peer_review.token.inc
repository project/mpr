<?php

use Drupal\multi_peer_review\MPRCommon;

/* 
 * Contains token hooks used by the Multi Peer Review module.
 */


/**
* Implements hook_token_info().
*/
function multi_peer_review_token_info() {
   
   $types = [
       'multi-peer-review' => [
            'name' => t('Multi Peer Review'),
            'description' => t('Module-wide tokens.'),
       ],            
   ];
   
   
   
   $tokens = [
       'multi-peer-review' => [
           'technical-contact-email' => [
                'name' => t("Technical Contact Email"),
                'dynamic' => FALSE,
                'description' => t('Email address of the Multi Peer Review module technical contact.'),               
           ],
           'last-error' => [
                'name' => t("Last Error"),
                'dynamic' => FALSE,
                'description' => t('The last error message (if any) set by a process in the Multi Peer Review.'),               
           ],           
       ],                    
   ];
   
   
   // Add token info from entities in this module.
   $entity_token_info = [];
   array_push($entity_token_info, Drupal\multi_peer_review\Entity\Invitation::getTokenInfo());
   array_push($entity_token_info, Drupal\multi_peer_review\Entity\Reviewer::getTokenInfo());
   array_push($entity_token_info, Drupal\multi_peer_review\Entity\Paper::getTokenInfo());   
   

   // Merge types and tokens.
   foreach ($entity_token_info as $entity_token_types => $entity_tokens) {       
       foreach ($entity_token_types['types'] as $key => $value) {
           $types[$key] = $value;
       }
       
       foreach ($entity_tokens['tokens'] as $key => $value) {
           $tokens[$key] = $value;
       }              
   }
   
   
   return ['types' => $types, 'tokens' => $tokens];
}


/**
* Implements hook_tokens().
*/
function multi_peer_review_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
   $replacements = [];
      
   if ($type == 'multi-peer-review') {
       
        // Module scope tokens (no data is required).
        foreach ($tokens as $name => $original) {
            switch ($name) {
                 case 'technical-contact-email':                   
                     $replacements[$original] = MPRCommon::getConfigValue('technical_contact_email');                  
                     break;          
                 case 'last-error':                                        
                     $html = MPRCommon::$last_error;
                     
                     // If the error contains a bullet point list, encapsulate it into an appropriate object
                     // that implements MarkupInterface.
                     // @see \Drupal\Component\Render\MarkupInterface
                     if (strpos($html, '<ul>') !== FALSE) {                         
                         $html = \Drupal\Core\Field\FieldFilteredMarkup::create($html);
                     }
                     
                     $replacements[$original] = $html;                 
                     break;     
            }
        }           
        
   }
   else {   
        if (empty($data[$type]) == FALSE) {       
             
             $dat = $data[$type];
             switch ($type) {
                 case 'invitation';
                 case 'reviewer';
                 case 'paper';    
                 case 'review';        
                     foreach ($tokens as $name => $original) {
                         $replacements[$original] = $dat->getTokenReplacementValue($name);
                     }                    
                     break;                              
             }       
        }
   }
   
   return $replacements;
}