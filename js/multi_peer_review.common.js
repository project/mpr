/**
 * @file
 * Contains multi_peer_review.common.js.
 */

function renameAllIds(element, newId) {
    var propSwap;
    var attributesToCheck = new Array("data-drupal-selector", "aria-controls", "aria-describedby");
    var index;
    var max;    
    var newIdText;
    
    newIdText = "inxid" + newId;
    
    element.id = element.id.replace(/inxid0/g, newIdText);
    if (element.name !== undefined) {
        element.name = element.name.replace(/inxid0/g, newIdText);
    }
    element.className = element.className.replace(/inxid0/g, newIdText);

    if (element.htmlFor !== undefined) {
        element.htmlFor = element.htmlFor.replace(/inxid0/g, newIdText);
    }

    max = attributesToCheck.length;
    index = 0;
    while (index < max) {
        propSwap = element.getAttribute(attributesToCheck[index]);
        if (propSwap != null) {                        
            propSwap = propSwap.replace(/inxid0/g, newIdText);
            element.setAttribute(attributesToCheck[index], propSwap);
        }                    
        index++;
    }    
}

function removeContainerFromParent(targetId) {
    
    var ctl;
    
    ctl = document.getElementById(targetId);
    if (ctl != null) {
        ctl.parentNode.removeChild(ctl);
    }
}

function setupCloneAndAppendContainerJQueryMethod() {
    
    jQuery.fn.cloneAndAppendContainer = function(destinationSelector, destinationChildIndex, templateContainerId) {
        
        var elements;
        var destCtl;      
        var clonedCtl;        
        var newId;
        var index;
        var max;
        
        newId = 999999; //unlikely to ever be used
        
        destCtl = null;
        if (destinationSelector.indexOf("#") == 0) {
            destCtl = document.getElementById(destinationSelector.substr(1));
        }
        else {
            elements = document.getElementsByClassName(destinationSelector.substr(1));
            if (elements.length != 0) {
                destCtl = elements[0];
            }
        }
                        
        if (destCtl != null) {        
            if (destinationChildIndex != -1) {
                destCtl = destCtl.childNodes[destinationChildIndex];
            }
            
            max = 1000; //unlikely to have more than this many containers
            index = 0;
            while (index < max) {
                if (document.getElementById(templateContainerId + index) == null) {
                    // Element not found, so we should use this ID.
                    // This method will also fill in gaps if a container was removed.
                    newId = index;
                    index = max;
                }
                index++;
            }                   
            
            
            // Clone without any data or events
            clonedCtl = this.clone(true);
            
            // Clears input fields
            clonedCtl.find("input:text").val("");
            clonedCtl.find("textarea").val("");
            clonedCtl.find("input:checkbox").prop("checked", false);
                                    
            // Rename IDs on parent element           
            renameAllIds(clonedCtl[0], newId);
            
            // Rename IDs on child elements
            clonedCtl.find("*").each(function(){       
                renameAllIds(this, newId);
            });       
            
            // Override submit button
            clonedCtl.find("input:submit").each(function(){
                var buttonRef;
                
                buttonRef = jQuery(this);
                this.setAttribute("type", "button");
                buttonRef.unbind();
                buttonRef.bind("click", this, function(){removeContainerFromParent(templateContainerId + newId);});
            });

            
            clonedCtl.prependTo( destCtl );
        }
        
    };    
    
}

setupCloneAndAppendContainerJQueryMethod();